<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.109
Name: Anamnese - Labor und Mikrobiologie - codiert
Description: Diese Section dient zur Dokumentation der Anamnese. In erster Linie soll oberflächlich erfasst werden, ob für den Patienten gewisse Aspekte aus seiner Krankengeschichte zutreffen oder nicht.  Darstellung der Ergebnisse  "section/text" enthält den narrativen Text, der der CDA Level 2 Darstellung der medizinischen Inhalte entspricht. Die
                Darstellung der Anamnese in "section/text" erfolgt idealerweise tabellarisch, wobei die Tabelle wie folgt aufgebaut sein sollte.  Spaltenname Beschreibung Aspekt Angabe des Aspekts der Krankengeschichte, der erhoben worden ist. Ergebnis Binäres Ergebnis der Bewertung des Aspekts in der Form "trifft
                            zu" oder "trifft nicht zu".
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709">
   <title>Anamnese - Labor und Mikrobiologie - codiert</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.109
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]
Item: (atlab_section_AnamneseLaborUndMikrobiologieCodiert)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.109
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]
Item: (atlab_section_AnamneseLaborUndMikrobiologieCodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]"
         id="d42e16382-false-d1062013e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="string(@classCode) = ('DOCSECT') or not(@classCode)">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Der Wert von classCode MUSS 'DOCSECT' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="string(@moodCode) = ('EVN') or not(@moodCode)">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']) &gt;= 1">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']) &lt;= 1">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="count(hl7:id[not(@nullFlavor)]) &lt;= 1">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Element hl7:id[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="count(hl7:code[(@code = '10164-2' and @codeSystem = '2.16.840.1.113883.6.1')]) &gt;= 1">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Element hl7:code[(@code = '10164-2' and @codeSystem = '2.16.840.1.113883.6.1')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="count(hl7:code[(@code = '10164-2' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt;= 1">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Element hl7:code[(@code = '10164-2' and @codeSystem = '2.16.840.1.113883.6.1')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="count(hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.12']]]) &gt;= 1">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Element hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.12']]] ist mandatory [min 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.109
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']
Item: (atlab_section_AnamneseLaborUndMikrobiologieCodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']"
         id="d42e16388-false-d1062195e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.2.109')">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Der Wert von root MUSS '1.2.40.0.34.6.0.11.2.109' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.109
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]/hl7:id[not(@nullFlavor)]
Item: (atlab_section_AnamneseLaborUndMikrobiologieCodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]/hl7:id[not(@nullFlavor)]"
         id="d42e16397-false-d1062207e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.109
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]/hl7:code[(@code = '10164-2' and @codeSystem = '2.16.840.1.113883.6.1')]
Item: (atlab_section_AnamneseLaborUndMikrobiologieCodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]/hl7:code[(@code = '10164-2' and @codeSystem = '2.16.840.1.113883.6.1')]"
         id="d42e16403-false-d1062218e0">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="@nullFlavor or (@code='10164-2' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='History of Present illness Narrative')">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Der Elementinhalt MUSS einer von 'code '10164-2' codeSystem '2.16.840.1.113883.6.1' displayName='History of Present illness Narrative'' sein.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="string(@codeSystemName) = ('LOINC') or not(@codeSystemName)">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Der Wert von codeSystemName MUSS 'LOINC' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.109
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]/hl7:title[not(@nullFlavor)]
Item: (atlab_section_AnamneseLaborUndMikrobiologieCodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]/hl7:title[not(@nullFlavor)]"
         id="d42e16411-false-d1062240e0">
      <extends rule="ST"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="text()='Anamnese'">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Anamnese'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.109
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]/hl7:text[not(@nullFlavor)]
Item: (atlab_section_AnamneseLaborUndMikrobiologieCodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]/hl7:text[not(@nullFlavor)]"
         id="d42e16419-false-d1062254e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.109
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.12']]]
Item: (atlab_section_AnamneseLaborUndMikrobiologieCodiert)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.12']]]">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="string(@typeCode) = ('DRIV')">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Der Wert von typeCode MUSS 'DRIV' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.109
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]
Item: (atlab_section_AnamneseLaborUndMikrobiologieCodiert)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.109']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atlab_section_AnamneseLaborUndMikrobiologieCodiert): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>
</pattern>
