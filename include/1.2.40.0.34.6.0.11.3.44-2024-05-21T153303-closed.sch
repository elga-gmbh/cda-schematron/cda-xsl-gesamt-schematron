<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.44
Name: Aktuelle Schwangerschaft Entry
Description: Informationen über eine aktuelle Schwangerschaft.  
                  Angegeben wird ein Kennzeichen (ja/nein/unbekannt).  Liegt eine aktuelle Schwangerschaft vor, kann auch der voraussichtliche Geburtstermin angegeben werden (Schätzung oder Berechnung nach letzter Regelblutung oder Eisprung) sowie das Datum der Schätzung/Berechnung.  
            
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303-closed">
   <title>Aktuelle Schwangerschaft Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']])]"
         id="d42e47544-true-d2031189e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d42e47544-true-d2031189e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']] (rule-reference: d42e47544-true-d2031189e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27'] | self::hl7:code[(@code = '82810-3' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.52-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]] | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d42e47603-true-d2031591e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d42e47603-true-d2031591e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27'] | hl7:code[(@code = '82810-3' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.52-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]] | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d42e47603-true-d2031591e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:code[(@code = '82810-3' and @codeSystem = '2.16.840.1.113883.6.1')]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation | self::ips:designation)]"
         id="d42e47612-true-d2031630e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d42e47612-true-d2031630e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation | ips:designation (rule-reference: d42e47612-true-d2031630e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:code[(@code = '82810-3' and @codeSystem = '2.16.840.1.113883.6.1')]/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d2031634e41-true-d2031646e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d2031634e41-true-d2031646e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d2031634e41-true-d2031646e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d2031665e54-true-d2031677e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d2031665e54-true-d2031677e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d2031665e54-true-d2031677e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low[not(@nullFlavor)] | self::hl7:high)]"
         id="d42e47632-true-d2031707e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d42e47632-true-d2031707e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low[not(@nullFlavor)] | hl7:high (rule-reference: d42e47632-true-d2031707e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.52-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/*[not(@xsi:nil = 'true')][not(self::hl7:translation | self::ips:designation)]"
         id="d42e47642-true-d2031739e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d42e47642-true-d2031739e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:translation | ips:designation (rule-reference: d42e47642-true-d2031739e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e47666-true-d2031876e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d42e47666-true-d2031876e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e47666-true-d2031876e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d2031754e44-true-d2031984e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2031754e44-true-d2031984e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d2031754e44-true-d2031984e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d2031754e63-true-d2032043e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2031754e63-true-d2032043e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d2031754e63-true-d2032043e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d2031754e109-true-d2032100e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2031754e109-true-d2032100e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d2031754e109-true-d2032100e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d2032104e92-true-d2032134e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d2032104e92-true-d2032134e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d2032104e92-true-d2032134e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d2031754e132-true-d2032182e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2031754e132-true-d2032182e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d2031754e132-true-d2032182e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d2031754e148-true-d2032227e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2031754e148-true-d2032227e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d2031754e148-true-d2032227e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d2032197e67-true-d2032290e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2032197e67-true-d2032290e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d2032197e67-true-d2032290e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e47670-true-d2032458e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d42e47670-true-d2032458e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e47670-true-d2032458e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d2032335e10-true-d2032590e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2032335e10-true-d2032590e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d2032335e10-true-d2032590e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d2032463e50-true-d2032656e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2032463e50-true-d2032656e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d2032463e50-true-d2032656e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d2032463e120-true-d2032718e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2032463e120-true-d2032718e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d2032463e120-true-d2032718e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d2032463e132-true-d2032740e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2032463e132-true-d2032740e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d2032463e132-true-d2032740e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d2032728e12-true-d2032769e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2032728e12-true-d2032769e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d2032728e12-true-d2032769e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d2032463e143-true-d2032820e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2032463e143-true-d2032820e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d2032463e143-true-d2032820e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d2032794e58-true-d2032881e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2032794e58-true-d2032881e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d2032794e58-true-d2032881e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d2032335e12-true-d2032958e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2032335e12-true-d2032958e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d2032335e12-true-d2032958e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d2032335e21-true-d2033011e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2032335e21-true-d2033011e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d2032335e21-true-d2033011e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d2032335e25-true-d2033066e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2032335e25-true-d2033066e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d2032335e25-true-d2033066e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d2033059e10-true-d2033093e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2033059e10-true-d2033093e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d2033059e10-true-d2033093e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']])]"
         id="d42e47677-true-d2033168e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d42e47677-true-d2033168e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']] (rule-reference: d42e47677-true-d2033168e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29'] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.202-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text | self::hl7:statusCode[@code = 'completed' or @nullFlavor] | self::hl7:effectiveTime | self::hl7:value | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d2033124e4-true-d2033255e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2033124e4-true-d2033255e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29'] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.202-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text | hl7:statusCode[@code = 'completed' or @nullFlavor] | hl7:effectiveTime | hl7:value | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d2033124e4-true-d2033255e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.202-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation | self::ips:designation)]"
         id="d2033124e24-true-d2033296e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2033124e24-true-d2033296e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation | ips:designation (rule-reference: d2033124e24-true-d2033296e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.202-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d2033300e41-true-d2033312e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d2033300e41-true-d2033312e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d2033300e41-true-d2033312e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d2033331e54-true-d2033343e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d2033331e54-true-d2033343e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d2033331e54-true-d2033343e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d2033124e94-true-d2033392e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2033124e94-true-d2033392e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d2033124e94-true-d2033392e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d2033369e6-true-d2033425e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2033369e6-true-d2033425e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d2033369e6-true-d2033425e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d2033445e54-true-d2033457e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d2033445e54-true-d2033457e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d2033445e54-true-d2033457e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d42e47681-true-d2033496e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d42e47681-true-d2033496e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d42e47681-true-d2033496e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d2033473e4-true-d2033529e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.html"
              test="not(.)">(atcdabbr_entry_AktuelleSchwangerschaft)/d2033473e4-true-d2033529e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d2033473e4-true-d2033529e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d2033549e54-true-d2033561e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d2033549e54-true-d2033561e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d2033549e54-true-d2033561e0)</assert>
   </rule>
</pattern>
