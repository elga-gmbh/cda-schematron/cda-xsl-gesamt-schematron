<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.51
Name: Beeinträchtigungen - kodiert
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717-closed">
   <title>Beeinträchtigungen - kodiert</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']])]"
         id="d42e22645-true-d1123781e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d42e22645-true-d1123781e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']] (rule-reference: d42e22645-true-d1123781e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[(@code = '47420-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]])]"
         id="d42e22686-true-d1124462e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d42e22686-true-d1124462e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8'] | hl7:id[not(@nullFlavor)] | hl7:code[(@code = '47420-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]] (rule-reference: d42e22686-true-d1124462e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e22712-true-d1124620e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d42e22712-true-d1124620e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e22712-true-d1124620e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1124498e44-true-d1124728e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1124498e44-true-d1124728e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1124498e44-true-d1124728e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1124498e63-true-d1124787e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1124498e63-true-d1124787e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1124498e63-true-d1124787e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1124498e109-true-d1124844e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1124498e109-true-d1124844e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1124498e109-true-d1124844e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1124848e92-true-d1124878e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1124848e92-true-d1124878e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1124848e92-true-d1124878e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1124498e132-true-d1124926e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1124498e132-true-d1124926e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1124498e132-true-d1124926e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1124498e148-true-d1124971e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1124498e148-true-d1124971e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1124498e148-true-d1124971e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1124941e67-true-d1125034e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1124941e67-true-d1125034e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1124941e67-true-d1125034e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e22716-true-d1125202e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d42e22716-true-d1125202e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e22716-true-d1125202e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1125079e12-true-d1125334e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125079e12-true-d1125334e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1125079e12-true-d1125334e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1125207e50-true-d1125400e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125207e50-true-d1125400e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1125207e50-true-d1125400e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1125207e120-true-d1125462e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125207e120-true-d1125462e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1125207e120-true-d1125462e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1125207e132-true-d1125484e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125207e132-true-d1125484e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1125207e132-true-d1125484e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1125472e12-true-d1125513e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125472e12-true-d1125513e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1125472e12-true-d1125513e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1125207e143-true-d1125564e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125207e143-true-d1125564e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1125207e143-true-d1125564e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1125538e58-true-d1125625e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125538e58-true-d1125625e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1125538e58-true-d1125625e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1125079e14-true-d1125702e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125079e14-true-d1125702e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1125079e14-true-d1125702e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1125079e23-true-d1125755e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125079e23-true-d1125755e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1125079e23-true-d1125755e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1125079e27-true-d1125810e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125079e27-true-d1125810e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1125079e27-true-d1125810e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1125803e10-true-d1125837e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125803e10-true-d1125837e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1125803e10-true-d1125837e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']])]"
         id="d42e22724-true-d1126164e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d42e22724-true-d1126164e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']] (rule-reference: d42e22724-true-d1126164e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43'] | self::hl7:id | self::hl7:code[(@code = '284773001' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime | self::hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')][hl7:originalText] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d1125868e8-true-d1126512e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125868e8-true-d1126512e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43'] | hl7:id | hl7:code[(@code = '284773001' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime | hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')][hl7:originalText] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d1125868e8-true-d1126512e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')][hl7:originalText]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText)]"
         id="d1125868e55-true-d1126560e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125868e55-true-d1126560e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText (rule-reference: d1125868e55-true-d1126560e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')][hl7:originalText]/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference)]"
         id="d1125868e70-true-d1126574e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125868e70-true-d1126574e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference (rule-reference: d1125868e70-true-d1126574e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1125868e94-true-d1126706e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125868e94-true-d1126706e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1125868e94-true-d1126706e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1126584e41-true-d1126814e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1126584e41-true-d1126814e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1126584e41-true-d1126814e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1126584e60-true-d1126873e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1126584e60-true-d1126873e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1126584e60-true-d1126873e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1126584e106-true-d1126930e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1126584e106-true-d1126930e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1126584e106-true-d1126930e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1126934e92-true-d1126964e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1126934e92-true-d1126964e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1126934e92-true-d1126964e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1126584e129-true-d1127012e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1126584e129-true-d1127012e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1126584e129-true-d1127012e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1126584e145-true-d1127057e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1126584e145-true-d1127057e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1126584e145-true-d1127057e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1127027e67-true-d1127120e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1127027e67-true-d1127120e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1127027e67-true-d1127120e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1125868e97-true-d1127288e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125868e97-true-d1127288e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1125868e97-true-d1127288e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1127165e5-true-d1127420e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1127165e5-true-d1127420e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1127165e5-true-d1127420e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1127293e50-true-d1127486e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1127293e50-true-d1127486e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1127293e50-true-d1127486e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1127293e120-true-d1127548e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1127293e120-true-d1127548e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1127293e120-true-d1127548e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1127293e132-true-d1127570e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1127293e132-true-d1127570e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1127293e132-true-d1127570e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1127558e12-true-d1127599e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1127558e12-true-d1127599e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1127558e12-true-d1127599e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1127293e143-true-d1127650e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1127293e143-true-d1127650e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1127293e143-true-d1127650e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1127624e58-true-d1127711e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1127624e58-true-d1127711e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1127624e58-true-d1127711e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1127165e7-true-d1127788e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1127165e7-true-d1127788e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1127165e7-true-d1127788e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1127165e16-true-d1127841e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1127165e16-true-d1127841e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1127165e16-true-d1127841e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1127165e20-true-d1127896e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1127165e20-true-d1127896e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1127165e20-true-d1127896e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1127889e10-true-d1127923e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1127889e10-true-d1127923e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1127889e10-true-d1127923e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d1125868e99-true-d1127977e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1125868e99-true-d1127977e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d1125868e99-true-d1127977e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d1127961e1-true-d1128010e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1127961e1-true-d1128010e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d1127961e1-true-d1128010e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1128030e54-true-d1128042e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1128030e54-true-d1128042e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1128030e54-true-d1128042e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']])]"
         id="d42e22733-true-d1128337e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d42e22733-true-d1128337e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']] (rule-reference: d42e22733-true-d1128337e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | self::hl7:id[not(@nullFlavor)] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant)]"
         id="d1128058e6-true-d1128650e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1128058e6-true-d1128650e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | hl7:id[not(@nullFlavor)] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | hl7:author[hl7:assignedAuthor] | hl7:informant (rule-reference: d1128058e6-true-d1128650e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1128058e57-true-d1128804e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1128058e57-true-d1128804e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1128058e57-true-d1128804e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1128682e45-true-d1128912e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1128682e45-true-d1128912e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1128682e45-true-d1128912e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1128682e64-true-d1128971e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1128682e64-true-d1128971e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1128682e64-true-d1128971e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1128682e110-true-d1129028e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1128682e110-true-d1129028e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1128682e110-true-d1129028e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1129032e92-true-d1129062e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1129032e92-true-d1129062e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1129032e92-true-d1129062e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1128682e133-true-d1129110e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1128682e133-true-d1129110e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1128682e133-true-d1129110e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1128682e149-true-d1129155e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1128682e149-true-d1129155e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1128682e149-true-d1129155e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1129125e67-true-d1129218e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1129125e67-true-d1129218e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1129125e67-true-d1129218e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1128058e63-true-d1129386e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1128058e63-true-d1129386e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1128058e63-true-d1129386e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1129263e5-true-d1129518e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1129263e5-true-d1129518e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1129263e5-true-d1129518e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1129391e50-true-d1129584e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1129391e50-true-d1129584e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1129391e50-true-d1129584e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1129391e120-true-d1129646e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1129391e120-true-d1129646e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1129391e120-true-d1129646e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1129391e132-true-d1129668e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1129391e132-true-d1129668e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1129391e132-true-d1129668e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1129656e12-true-d1129697e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1129656e12-true-d1129697e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1129656e12-true-d1129697e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1129391e143-true-d1129748e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1129391e143-true-d1129748e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1129391e143-true-d1129748e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1129722e58-true-d1129809e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1129722e58-true-d1129809e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1129722e58-true-d1129809e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1129263e7-true-d1129886e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1129263e7-true-d1129886e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1129263e7-true-d1129886e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1129263e16-true-d1129939e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1129263e16-true-d1129939e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1129263e16-true-d1129939e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1129263e20-true-d1129994e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1129263e20-true-d1129994e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1129263e20-true-d1129994e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1129987e10-true-d1130021e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d1129987e10-true-d1130021e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1129987e10-true-d1130021e0)</assert>
   </rule>
</pattern>
