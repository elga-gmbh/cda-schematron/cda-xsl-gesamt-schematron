<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.10009
Name: ELGA CDA Dokument Pflegesituationsberichts
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.10009-2015-09-26T000000-closed">
   <title>ELGA CDA Dokument Pflegesituationsberichts</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/*[not(@xsi:nil = 'true')][not(self::hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']])]"
         id="d42e5306-true-d129050e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.10009-2015-09-26T000000.html"
              test="not(.)">(Pflegesituationsberichtdocument)/d42e5306-true-d129050e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']] (rule-reference: d42e5306-true-d129050e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/*[not(@xsi:nil = 'true')][not(self::hl7:realmCode[not(@nullFlavor)] | self::hl7:typeId[@root = '2.16.840.1.113883.1.3'][@extension = 'POCD_HD000040'] | self::hl7:templateId[@root = '1.2.40.0.34.11.1'] | self::hl7:templateId[@root = '1.2.40.0.34.11.12'] | self::hl7:templateId[@root = '1.2.40.0.34.11.12.0.1'] | self::hl7:templateId[@root = '1.2.40.0.34.11.12.0.2'] | self::hl7:templateId[@root = '1.2.40.0.34.11.12.0.3'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[(@code = '28651-8' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title[not(@nullFlavor)] | self::hl7:effectiveTime[not(@nullFlavor)] | self::hl7:confidentialityCode[(@code = 'N' and @codeSystem = '2.16.840.1.113883.5.25')] | self::hl7:languageCode[@code = 'de-AT'] | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)] | self::hl7:recordTarget[hl7:patientRole] | self::hl7:author[hl7:assignedAuthor] | self::hl7:dataEnterer[hl7:assignedEntity] | self::hl7:custodian[hl7:assignedCustodian] | self::hl7:informationRecipient[hl7:intendedRecipient] | self::hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']] | self::hl7:authenticator[hl7:signatureCode[@code = 'S']] | self::hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.1']] | self::hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.2']] | self::hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.3']] | self::hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.4']] | self::hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.5']] | self::hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.6']] | self::hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.7']] | self::hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.8']] | self::hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = 'GDLPUB' and @codeSystem = '1.2.40.0.34.5.21') or @nullFlavor]]] | self::hl7:relatedDocument[@typeCode = 'RPLC'][hl7:parentDocument] | self::hl7:authorization | self::hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]] | self::hl7:component)]"
         id="d42e5310-true-d129613e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.10009-2015-09-26T000000.html"
              test="not(.)">(Pflegesituationsberichtdocument)/d42e5310-true-d129613e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:realmCode[not(@nullFlavor)] | hl7:typeId[@root = '2.16.840.1.113883.1.3'][@extension = 'POCD_HD000040'] | hl7:templateId[@root = '1.2.40.0.34.11.1'] | hl7:templateId[@root = '1.2.40.0.34.11.12'] | hl7:templateId[@root = '1.2.40.0.34.11.12.0.1'] | hl7:templateId[@root = '1.2.40.0.34.11.12.0.2'] | hl7:templateId[@root = '1.2.40.0.34.11.12.0.3'] | hl7:id[not(@nullFlavor)] | hl7:code[(@code = '28651-8' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title[not(@nullFlavor)] | hl7:effectiveTime[not(@nullFlavor)] | hl7:confidentialityCode[(@code = 'N' and @codeSystem = '2.16.840.1.113883.5.25')] | hl7:languageCode[@code = 'de-AT'] | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] | hl7:recordTarget[hl7:patientRole] | hl7:author[hl7:assignedAuthor] | hl7:dataEnterer[hl7:assignedEntity] | hl7:custodian[hl7:assignedCustodian] | hl7:informationRecipient[hl7:intendedRecipient] | hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']] | hl7:authenticator[hl7:signatureCode[@code = 'S']] | hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.1']] | hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.2']] | hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.3']] | hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.4']] | hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.5']] | hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.6']] | hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.7']] | hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.8']] | hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = 'GDLPUB' and @codeSystem = '1.2.40.0.34.5.21') or @nullFlavor]]] | hl7:relatedDocument[@typeCode = 'RPLC'][hl7:parentDocument] | hl7:authorization | hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]] | hl7:component (rule-reference: d42e5310-true-d129613e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:recordTarget[hl7:patientRole]/*[not(@xsi:nil = 'true')][not(self::hl7:patientRole)]"
         id="d129737e4-true-d129757e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20014-2017-03-27T000000.html"
              test="not(.)">(HeaderRecordTargetPflege)/d129737e4-true-d129757e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:patientRole (rule-reference: d129737e4-true-d129757e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:recordTarget[hl7:patientRole]/hl7:patientRole/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:addr | self::hl7:telecom | self::hl7:patient[hl7:administrativeGenderCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code]])]"
         id="d129737e18-true-d129800e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20014-2017-03-27T000000.html"
              test="not(.)">(HeaderRecordTargetPflege)/d129737e18-true-d129800e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:addr | hl7:telecom | hl7:patient[hl7:administrativeGenderCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code]] (rule-reference: d129737e18-true-d129800e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:recordTarget[hl7:patientRole]/hl7:patientRole/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d129737e55-true-d129854e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20014-2017-03-27T000000.html"
              test="not(.)">(HeaderRecordTargetPflege)/d129737e55-true-d129854e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d129737e55-true-d129854e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:recordTarget[hl7:patientRole]/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code]]/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:administrativeGenderCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code] | self::hl7:birthTime | self::hl7:maritalStatusCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.11-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.11-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code] | self::hl7:religiousAffiliationCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.18-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.18-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code] | self::hl7:raceCode | self::hl7:ethnicGroupCode | self::hl7:guardian | self::hl7:birthplace[hl7:place] | self::hl7:languageCommunication)]"
         id="d129737e121-true-d129981e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20014-2017-03-27T000000.html"
              test="not(.)">(HeaderRecordTargetPflege)/d129737e121-true-d129981e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:administrativeGenderCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code] | hl7:birthTime | hl7:maritalStatusCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.11-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.11-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code] | hl7:religiousAffiliationCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.18-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.18-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code] | hl7:raceCode | hl7:ethnicGroupCode | hl7:guardian | hl7:birthplace[hl7:place] | hl7:languageCommunication (rule-reference: d129737e121-true-d129981e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:recordTarget[hl7:patientRole]/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code]]/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:given[not(@nullFlavor)] | self::hl7:family[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d129737e137-true-d130010e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20014-2017-03-27T000000.html"
              test="not(.)">(HeaderRecordTargetPflege)/d129737e137-true-d130010e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:given[not(@nullFlavor)] | hl7:family[not(@nullFlavor)] | hl7:suffix (rule-reference: d129737e137-true-d130010e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:recordTarget[hl7:patientRole]/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code]]/hl7:guardian/*[not(@xsi:nil = 'true')][not(self::hl7:addr | self::hl7:telecom | self::hl7:guardianPerson | self::hl7:guardianOrganization)]"
         id="d129737e214-true-d130092e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20014-2017-03-27T000000.html"
              test="not(.)">(HeaderRecordTargetPflege)/d129737e214-true-d130092e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:addr | hl7:telecom | hl7:guardianPerson | hl7:guardianOrganization (rule-reference: d129737e214-true-d130092e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:recordTarget[hl7:patientRole]/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code]]/hl7:guardian/hl7:guardianPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d129737e251-true-d130116e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20014-2017-03-27T000000.html"
              test="not(.)">(HeaderRecordTargetPflege)/d129737e251-true-d130116e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d129737e251-true-d130116e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:recordTarget[hl7:patientRole]/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code]]/hl7:guardian/hl7:guardianOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d129737e256-true-d130135e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20014-2017-03-27T000000.html"
              test="not(.)">(HeaderRecordTargetPflege)/d129737e256-true-d130135e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d129737e256-true-d130135e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:recordTarget[hl7:patientRole]/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code]]/hl7:birthplace[hl7:place]/*[not(@xsi:nil = 'true')][not(self::hl7:place[not(@nullFlavor)])]"
         id="d129737e265-true-d130154e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20014-2017-03-27T000000.html"
              test="not(.)">(HeaderRecordTargetPflege)/d129737e265-true-d130154e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:place[not(@nullFlavor)] (rule-reference: d129737e265-true-d130154e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:recordTarget[hl7:patientRole]/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code]]/hl7:birthplace[hl7:place]/hl7:place[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:addr[not(@nullFlavor)])]"
         id="d129737e275-true-d130168e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20014-2017-03-27T000000.html"
              test="not(.)">(HeaderRecordTargetPflege)/d129737e275-true-d130168e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:addr[not(@nullFlavor)] (rule-reference: d129737e275-true-d130168e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:recordTarget[hl7:patientRole]/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code]]/hl7:languageCommunication/*[not(@xsi:nil = 'true')][not(self::hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code or @nullFlavor] | self::hl7:modeCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.175-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:proficiencyLevelCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.174-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:preferenceInd)]"
         id="d130177e22-true-d130222e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90017-2017-03-27T000000.html"
              test="not(.)">(LanguageCommunication)/d130177e22-true-d130222e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code or @nullFlavor] | hl7:modeCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.175-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:proficiencyLevelCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.174-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:preferenceInd (rule-reference: d130177e22-true-d130222e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time | self::hl7:assignedAuthor[hl7:representedOrganization])]"
         id="d130257e131-true-d130288e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20002-2015-05-09T000000.html"
              test="not(.)">(HeaderAuthor)/d130257e131-true-d130288e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time | hl7:assignedAuthor[hl7:representedOrganization] (rule-reference: d130257e131-true-d130288e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization[not(@nullFlavor)])]"
         id="d130257e178-true-d130342e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20002-2015-05-09T000000.html"
              test="not(.)">(HeaderAuthor)/d130257e178-true-d130342e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:telecom | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization[not(@nullFlavor)] (rule-reference: d130257e178-true-d130342e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d130257e268-true-d130375e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20002-2015-05-09T000000.html"
              test="not(.)">(HeaderAuthor)/d130257e268-true-d130375e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d130257e268-true-d130375e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName | self::hl7:softwareName)]"
         id="d130257e279-true-d130401e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20002-2015-05-09T000000.html"
              test="not(.)">(HeaderAuthor)/d130257e279-true-d130401e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName | hl7:softwareName (rule-reference: d130257e279-true-d130401e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d130257e301-true-d130440e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20002-2015-05-09T000000.html"
              test="not(.)">(HeaderAuthor)/d130257e301-true-d130440e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d130257e301-true-d130440e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:dataEnterer[hl7:assignedEntity]/*[not(@xsi:nil = 'true')][not(self::hl7:time | self::hl7:assignedEntity[hl7:assignedPerson])]"
         id="d130466e32-true-d130504e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20003-2013-02-10T000000.html"
              test="not(.)">(HeaderDataEnterer)/d130466e32-true-d130504e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time | hl7:assignedEntity[hl7:assignedPerson] (rule-reference: d130466e32-true-d130504e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[hl7:assignedPerson]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson[not(@nullFlavor)] | self::hl7:representedOrganization)]"
         id="d130466e47-true-d130542e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20003-2013-02-10T000000.html"
              test="not(.)">(HeaderDataEnterer)/d130466e47-true-d130542e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:addr | hl7:telecom | hl7:assignedPerson[not(@nullFlavor)] | hl7:representedOrganization (rule-reference: d130466e47-true-d130542e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d130546e42-true-d130574e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="not(.)">(AssignedEntityElements)/d130546e42-true-d130574e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d130546e42-true-d130574e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[hl7:assignedPerson]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d130546e50-true-d130602e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="not(.)">(AssignedEntityElements)/d130546e50-true-d130602e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d130546e50-true-d130602e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:custodian[hl7:assignedCustodian]/*[not(@xsi:nil = 'true')][not(self::hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization])]"
         id="d130630e60-true-d130642e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20004-2015-05-28T000000.html"
              test="not(.)">(HeaderCustodian)/d130630e60-true-d130642e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization] (rule-reference: d130630e60-true-d130642e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/*[not(@xsi:nil = 'true')][not(self::hl7:representedCustodianOrganization[not(@nullFlavor)][hl7:name])]"
         id="d130630e68-true-d130656e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20004-2015-05-28T000000.html"
              test="not(.)">(HeaderCustodian)/d130630e68-true-d130656e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:representedCustodianOrganization[not(@nullFlavor)][hl7:name] (rule-reference: d130630e68-true-d130656e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)][hl7:name]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr[not(@nullFlavor)])]"
         id="d130630e72-true-d130685e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20004-2015-05-28T000000.html"
              test="not(.)">(HeaderCustodian)/d130630e72-true-d130685e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr[not(@nullFlavor)] (rule-reference: d130630e72-true-d130685e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:informationRecipient[hl7:intendedRecipient]/*[not(@xsi:nil = 'true')][not(self::hl7:intendedRecipient[not(@nullFlavor)][hl7:informationRecipient])]"
         id="d130711e82-true-d130738e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20005-2011-12-19T000000.html"
              test="not(.)">(HeaderInformationRecipient)/d130711e82-true-d130738e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:intendedRecipient[not(@nullFlavor)][hl7:informationRecipient] (rule-reference: d130711e82-true-d130738e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:informationRecipient[hl7:intendedRecipient]/hl7:intendedRecipient[not(@nullFlavor)][hl7:informationRecipient]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:informationRecipient[not(@nullFlavor)] | self::hl7:receivedOrganization)]"
         id="d130711e103-true-d130776e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20005-2011-12-19T000000.html"
              test="not(.)">(HeaderInformationRecipient)/d130711e103-true-d130776e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:informationRecipient[not(@nullFlavor)] | hl7:receivedOrganization (rule-reference: d130711e103-true-d130776e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:informationRecipient[hl7:intendedRecipient]/hl7:intendedRecipient[not(@nullFlavor)][hl7:informationRecipient]/hl7:informationRecipient[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d130711e141-true-d130796e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20005-2011-12-19T000000.html"
              test="not(.)">(HeaderInformationRecipient)/d130711e141-true-d130796e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d130711e141-true-d130796e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:informationRecipient[hl7:intendedRecipient]/hl7:intendedRecipient[not(@nullFlavor)][hl7:informationRecipient]/hl7:receivedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d130711e155-true-d130824e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20005-2011-12-19T000000.html"
              test="not(.)">(HeaderInformationRecipient)/d130711e155-true-d130824e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d130711e155-true-d130824e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/*[not(@xsi:nil = 'true')][not(self::hl7:time | self::hl7:signatureCode[@code = 'S'] | self::hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson])]"
         id="d130852e74-true-d130898e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20006-2011-12-19T000000.html"
              test="not(.)">(HeaderLegalAuthenticator)/d130852e74-true-d130898e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time | hl7:signatureCode[@code = 'S'] | hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson] (rule-reference: d130852e74-true-d130898e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson[not(@nullFlavor)] | self::hl7:representedOrganization)]"
         id="d130852e104-true-d130942e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20006-2011-12-19T000000.html"
              test="not(.)">(HeaderLegalAuthenticator)/d130852e104-true-d130942e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:addr | hl7:telecom | hl7:assignedPerson[not(@nullFlavor)] | hl7:representedOrganization (rule-reference: d130852e104-true-d130942e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d130946e42-true-d130974e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="not(.)">(AssignedEntityElements)/d130946e42-true-d130974e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d130946e42-true-d130974e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d130946e50-true-d131002e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="not(.)">(AssignedEntityElements)/d130946e50-true-d131002e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d130946e50-true-d131002e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:authenticator[hl7:signatureCode[@code = 'S']]/*[not(@xsi:nil = 'true')][not(self::hl7:time | self::hl7:signatureCode[@code = 'S'] | self::hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson])]"
         id="d131030e90-true-d131076e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20007-2011-12-19T000000.html"
              test="not(.)">(HeaderAuthenticator)/d131030e90-true-d131076e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time | hl7:signatureCode[@code = 'S'] | hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson] (rule-reference: d131030e90-true-d131076e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:authenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson[not(@nullFlavor)] | self::hl7:representedOrganization)]"
         id="d131030e117-true-d131120e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20007-2011-12-19T000000.html"
              test="not(.)">(HeaderAuthenticator)/d131030e117-true-d131120e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:addr | hl7:telecom | hl7:assignedPerson[not(@nullFlavor)] | hl7:representedOrganization (rule-reference: d131030e117-true-d131120e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:authenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d131124e42-true-d131152e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="not(.)">(AssignedEntityElements)/d131124e42-true-d131152e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d131124e42-true-d131152e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:authenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d131124e50-true-d131180e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="not(.)">(AssignedEntityElements)/d131124e50-true-d131180e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d131124e50-true-d131180e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.1.1'] | self::hl7:time | self::hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson])]"
         id="d131208e64-true-d131240e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.1-2014-03-25T000000.html"
              test="not(.)">(HeaderParticipantAnsprechpartner)/d131208e64-true-d131240e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.1.1'] | hl7:time | hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson] (rule-reference: d131208e64-true-d131240e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.1']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson]/*[not(@xsi:nil = 'true')][not(self::hl7:addr | self::hl7:telecom[not(@nullFlavor)] | self::hl7:associatedPerson[not(@nullFlavor)] | self::hl7:scopingOrganization)]"
         id="d131208e79-true-d131294e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.1-2014-03-25T000000.html"
              test="not(.)">(HeaderParticipantAnsprechpartner)/d131208e79-true-d131294e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:addr | hl7:telecom[not(@nullFlavor)] | hl7:associatedPerson[not(@nullFlavor)] | hl7:scopingOrganization (rule-reference: d131208e79-true-d131294e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.1']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson]/hl7:associatedPerson[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d131208e110-true-d131319e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.1-2014-03-25T000000.html"
              test="not(.)">(HeaderParticipantAnsprechpartner)/d131208e110-true-d131319e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d131208e110-true-d131319e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.1']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson]/hl7:scopingOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d131208e120-true-d131347e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.1-2014-03-25T000000.html"
              test="not(.)">(HeaderParticipantAnsprechpartner)/d131208e120-true-d131347e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d131208e120-true-d131347e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.1.2'] | self::hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson])]"
         id="d131375e49-true-d131402e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.2-2013-10-16T000000.html"
              test="not(.)">(HeaderParticipantRefArzt)/d131375e49-true-d131402e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.1.2'] | hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson] (rule-reference: d131375e49-true-d131402e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.2']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:addr | self::hl7:telecom | self::hl7:associatedPerson | self::hl7:scopingOrganization)]"
         id="d131375e74-true-d131456e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.2-2013-10-16T000000.html"
              test="not(.)">(HeaderParticipantRefArzt)/d131375e74-true-d131456e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:addr | hl7:telecom | hl7:associatedPerson | hl7:scopingOrganization (rule-reference: d131375e74-true-d131456e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.2']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson]/hl7:associatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d131375e128-true-d131486e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.2-2013-10-16T000000.html"
              test="not(.)">(HeaderParticipantRefArzt)/d131375e128-true-d131486e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d131375e128-true-d131486e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.2']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson]/hl7:scopingOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d131375e139-true-d131514e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.2-2013-10-16T000000.html"
              test="not(.)">(HeaderParticipantRefArzt)/d131375e139-true-d131514e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d131375e139-true-d131514e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.3']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.1.3'] | self::hl7:functionCode[(@code = 'PCP' and @codeSystem = '2.16.840.1.113883.5.88')] | self::hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson])]"
         id="d131542e52-true-d131576e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.3-2013-10-16T000000.html"
              test="not(.)">(HeaderParticipantHausarzt)/d131542e52-true-d131576e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.1.3'] | hl7:functionCode[(@code = 'PCP' and @codeSystem = '2.16.840.1.113883.5.88')] | hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson] (rule-reference: d131542e52-true-d131576e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.3']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:addr | self::hl7:telecom | self::hl7:associatedPerson[not(@nullFlavor)] | self::hl7:scopingOrganization)]"
         id="d131542e80-true-d131636e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.3-2013-10-16T000000.html"
              test="not(.)">(HeaderParticipantHausarzt)/d131542e80-true-d131636e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:addr | hl7:telecom | hl7:associatedPerson[not(@nullFlavor)] | hl7:scopingOrganization (rule-reference: d131542e80-true-d131636e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.3']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson]/hl7:associatedPerson[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d131542e137-true-d131666e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.3-2013-10-16T000000.html"
              test="not(.)">(HeaderParticipantHausarzt)/d131542e137-true-d131666e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d131542e137-true-d131666e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.3']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson]/hl7:scopingOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d131542e150-true-d131694e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.3-2013-10-16T000000.html"
              test="not(.)">(HeaderParticipantHausarzt)/d131542e150-true-d131694e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d131542e150-true-d131694e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.4']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.1.4'] | self::hl7:time | self::hl7:associatedEntity[not(@nullFlavor)][@classCode = 'ECON'][hl7:associatedPerson])]"
         id="d131722e37-true-d131754e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.4-2013-11-13T000000.html"
              test="not(.)">(HeaderParticipantNotfallkontakt)/d131722e37-true-d131754e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.1.4'] | hl7:time | hl7:associatedEntity[not(@nullFlavor)][@classCode = 'ECON'][hl7:associatedPerson] (rule-reference: d131722e37-true-d131754e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.4']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'ECON'][hl7:associatedPerson]/*[not(@xsi:nil = 'true')][not(self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:addr | self::hl7:telecom | self::hl7:associatedPerson[not(@nullFlavor)] | self::hl7:scopingOrganization)]"
         id="d131722e67-true-d131819e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.4-2013-11-13T000000.html"
              test="not(.)">(HeaderParticipantNotfallkontakt)/d131722e67-true-d131819e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:addr | hl7:telecom | hl7:associatedPerson[not(@nullFlavor)] | hl7:scopingOrganization (rule-reference: d131722e67-true-d131819e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.4']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'ECON'][hl7:associatedPerson]/hl7:associatedPerson[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d131722e118-true-d131852e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.4-2013-11-13T000000.html"
              test="not(.)">(HeaderParticipantNotfallkontakt)/d131722e118-true-d131852e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d131722e118-true-d131852e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.4']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'ECON'][hl7:associatedPerson]/hl7:scopingOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d131722e131-true-d131880e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.4-2013-11-13T000000.html"
              test="not(.)">(HeaderParticipantNotfallkontakt)/d131722e131-true-d131880e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d131722e131-true-d131880e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.5']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.1.5'] | self::hl7:associatedEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]])]"
         id="d131908e25-true-d131941e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.5-2016-07-21T000000.html"
              test="not(.)">(HeaderParticipantAngehoerige)/d131908e25-true-d131941e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.1.5'] | hl7:associatedEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]] (rule-reference: d131908e25-true-d131941e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.5']]/hl7:associatedEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/*[not(@xsi:nil = 'true')][not(self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:addr | self::hl7:telecom | self::hl7:associatedPerson[not(@nullFlavor)] | self::hl7:scopingOrganization)]"
         id="d131908e38-true-d132004e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.5-2016-07-21T000000.html"
              test="not(.)">(HeaderParticipantAngehoerige)/d131908e38-true-d132004e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:addr | hl7:telecom | hl7:associatedPerson[not(@nullFlavor)] | hl7:scopingOrganization (rule-reference: d131908e38-true-d132004e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.5']]/hl7:associatedEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:associatedPerson[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d131908e82-true-d132037e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.5-2016-07-21T000000.html"
              test="not(.)">(HeaderParticipantAngehoerige)/d131908e82-true-d132037e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d131908e82-true-d132037e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.5']]/hl7:associatedEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:scopingOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d131908e92-true-d132065e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.5-2016-07-21T000000.html"
              test="not(.)">(HeaderParticipantAngehoerige)/d131908e92-true-d132065e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d131908e92-true-d132065e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.6']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.1.6'] | self::hl7:time | self::hl7:associatedEntity[not(@nullFlavor)][@classCode = 'POLHOLD'][hl7:scopingOrganization])]"
         id="d132093e103-true-d132125e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.6-2015-11-25T000000.html"
              test="not(.)">(HeaderParticipantVersicherung)/d132093e103-true-d132125e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.1.6'] | hl7:time | hl7:associatedEntity[not(@nullFlavor)][@classCode = 'POLHOLD'][hl7:scopingOrganization] (rule-reference: d132093e103-true-d132125e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.6']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'POLHOLD'][hl7:scopingOrganization]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.9-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:addr | self::hl7:telecom | self::hl7:associatedPerson | self::hl7:scopingOrganization)]"
         id="d132093e127-true-d132195e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.6-2015-11-25T000000.html"
              test="not(.)">(HeaderParticipantVersicherung)/d132093e127-true-d132195e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.9-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:addr | hl7:telecom | hl7:associatedPerson | hl7:scopingOrganization (rule-reference: d132093e127-true-d132195e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.6']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'POLHOLD'][hl7:scopingOrganization]/hl7:associatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d132093e204-true-d132233e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.6-2015-11-25T000000.html"
              test="not(.)">(HeaderParticipantVersicherung)/d132093e204-true-d132233e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d132093e204-true-d132233e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.6']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'POLHOLD'][hl7:scopingOrganization]/hl7:scopingOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d132093e218-true-d132261e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.6-2015-11-25T000000.html"
              test="not(.)">(HeaderParticipantVersicherung)/d132093e218-true-d132261e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d132093e218-true-d132261e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.7']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.1.7'] | self::hl7:associatedEntity[not(@nullFlavor)][@classCode = 'CAREGIVER'][hl7:scopingOrganization])]"
         id="d132289e43-true-d132312e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.7-2013-10-16T000000.html"
              test="not(.)">(HeaderParticipantBetreuorg)/d132289e43-true-d132312e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.1.7'] | hl7:associatedEntity[not(@nullFlavor)][@classCode = 'CAREGIVER'][hl7:scopingOrganization] (rule-reference: d132289e43-true-d132312e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.7']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'CAREGIVER'][hl7:scopingOrganization]/*[not(@xsi:nil = 'true')][not(self::hl7:scopingOrganization[not(@nullFlavor)])]"
         id="d132289e56-true-d132340e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.7-2013-10-16T000000.html"
              test="not(.)">(HeaderParticipantBetreuorg)/d132289e56-true-d132340e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:scopingOrganization[not(@nullFlavor)] (rule-reference: d132289e56-true-d132340e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.7']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'CAREGIVER'][hl7:scopingOrganization]/hl7:scopingOrganization[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d132289e64-true-d132361e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.7-2013-10-16T000000.html"
              test="not(.)">(HeaderParticipantBetreuorg)/d132289e64-true-d132361e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d132289e64-true-d132361e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.1.8'] | self::hl7:functionCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson])]"
         id="d132389e36-true-d132427e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.8-2017-02-20T000000.html"
              test="not(.)">(HeaderParticipantConsultant)/d132389e36-true-d132427e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.1.8'] | hl7:functionCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson] (rule-reference: d132389e36-true-d132427e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.8']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson]/*[not(@xsi:nil = 'true')][not(self::hl7:addr | self::hl7:telecom | self::hl7:associatedPerson[not(@nullFlavor)] | self::hl7:scopingOrganization)]"
         id="d132389e68-true-d132484e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.8-2017-02-20T000000.html"
              test="not(.)">(HeaderParticipantConsultant)/d132389e68-true-d132484e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:addr | hl7:telecom | hl7:associatedPerson[not(@nullFlavor)] | hl7:scopingOrganization (rule-reference: d132389e68-true-d132484e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.8']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson]/hl7:associatedPerson[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d132389e99-true-d132509e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.8-2017-02-20T000000.html"
              test="not(.)">(HeaderParticipantConsultant)/d132389e99-true-d132509e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d132389e99-true-d132509e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:participant[hl7:templateId[@root='1.2.40.0.34.11.1.1.8']]/hl7:associatedEntity[not(@nullFlavor)][@classCode = 'PROV'][hl7:associatedPerson]/hl7:scopingOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d132389e112-true-d132537e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.1.1.8-2017-02-20T000000.html"
              test="not(.)">(HeaderParticipantConsultant)/d132389e112-true-d132537e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d132389e112-true-d132537e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = 'GDLPUB' and @codeSystem = '1.2.40.0.34.5.21') or @nullFlavor]]]/*[not(@xsi:nil = 'true')][not(self::hl7:serviceEvent[hl7:code[(@code = 'GDLPUB' and @codeSystem = '1.2.40.0.34.5.21') or @nullFlavor]])]"
         id="d129636e68-true-d132576e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e68-true-d132576e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:serviceEvent[hl7:code[(@code = 'GDLPUB' and @codeSystem = '1.2.40.0.34.5.21') or @nullFlavor]] (rule-reference: d129636e68-true-d132576e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = 'GDLPUB' and @codeSystem = '1.2.40.0.34.5.21') or @nullFlavor]]]/hl7:serviceEvent[hl7:code[(@code = 'GDLPUB' and @codeSystem = '1.2.40.0.34.5.21') or @nullFlavor]]/*[not(@xsi:nil = 'true')][not(self::hl7:code[(@code = 'GDLPUB' and @codeSystem = '1.2.40.0.34.5.21') or @nullFlavor] | self::hl7:effectiveTime[not(@nullFlavor)] | self::hl7:performer)]"
         id="d129636e83-true-d132603e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e83-true-d132603e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[(@code = 'GDLPUB' and @codeSystem = '1.2.40.0.34.5.21') or @nullFlavor] | hl7:effectiveTime[not(@nullFlavor)] | hl7:performer (rule-reference: d129636e83-true-d132603e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = 'GDLPUB' and @codeSystem = '1.2.40.0.34.5.21') or @nullFlavor]]]/hl7:serviceEvent[hl7:code[(@code = 'GDLPUB' and @codeSystem = '1.2.40.0.34.5.21') or @nullFlavor]]/hl7:effectiveTime[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:low[not(@nullFlavor)] | self::hl7:high)]"
         id="d129636e88-true-d132628e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e88-true-d132628e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low[not(@nullFlavor)] | hl7:high (rule-reference: d129636e88-true-d132628e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:relatedDocument[@typeCode = 'RPLC'][hl7:parentDocument]/*[not(@xsi:nil = 'true')][not(self::hl7:parentDocument[not(@nullFlavor)])]"
         id="d132649e14-true-d132661e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20011-2014-12-06T000000.html"
              test="not(.)">(HeaderRelatedDocument)/d132649e14-true-d132661e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:parentDocument[not(@nullFlavor)] (rule-reference: d132649e14-true-d132661e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:relatedDocument[@typeCode = 'RPLC'][hl7:parentDocument]/hl7:parentDocument[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)])]"
         id="d132649e25-true-d132675e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.20011-2014-12-06T000000.html"
              test="not(.)">(HeaderRelatedDocument)/d132649e25-true-d132675e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] (rule-reference: d132649e25-true-d132675e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]]/*[not(@xsi:nil = 'true')][not(self::hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]])]"
         id="d129636e96-true-d132739e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e96-true-d132739e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]] (rule-reference: d129636e96-true-d132739e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:effectiveTime | self::hl7:responsibleParty[hl7:assignedEntity] | self::hl7:location[hl7:healthCareFacility])]"
         id="d129636e146-true-d132804e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e146-true-d132804e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:effectiveTime | hl7:responsibleParty[hl7:assignedEntity] | hl7:location[hl7:healthCareFacility] (rule-reference: d129636e146-true-d132804e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText)]"
         id="d129636e155-true-d132826e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e155-true-d132826e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText (rule-reference: d129636e155-true-d132826e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d129636e162-true-d132850e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e162-true-d132850e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d129636e162-true-d132850e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]/hl7:responsibleParty[hl7:assignedEntity]/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity[hl7:assignedPerson])]"
         id="d129636e173-true-d132895e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e173-true-d132895e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity[hl7:assignedPerson] (rule-reference: d129636e173-true-d132895e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[hl7:assignedPerson]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson[not(@nullFlavor)] | self::hl7:representedOrganization)]"
         id="d129636e174-true-d132928e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e174-true-d132928e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:addr | hl7:telecom | hl7:assignedPerson[not(@nullFlavor)] | hl7:representedOrganization (rule-reference: d129636e174-true-d132928e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d132932e42-true-d132960e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="not(.)">(AssignedEntityElements)/d132932e42-true-d132960e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d132932e42-true-d132960e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[hl7:assignedPerson]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d132932e50-true-d132988e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="not(.)">(AssignedEntityElements)/d132932e50-true-d132988e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d132932e50-true-d132988e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]/hl7:location[hl7:healthCareFacility]/*[not(@xsi:nil = 'true')][not(self::hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization])]"
         id="d133014e7-true-d133026e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90011-2014-03-24T000000.html"
              test="not(.)">(EncounterLocation2)/d133014e7-true-d133026e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization] (rule-reference: d133014e7-true-d133026e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/*[not(@xsi:nil = 'true')][not(self::hl7:serviceProviderOrganization[not(@nullFlavor)])]"
         id="d133014e19-true-d133040e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90011-2014-03-24T000000.html"
              test="not(.)">(EncounterLocation2)/d133014e19-true-d133040e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:serviceProviderOrganization[not(@nullFlavor)] (rule-reference: d133014e19-true-d133040e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d133014e34-true-d133069e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90011-2014-03-24T000000.html"
              test="not(.)">(EncounterLocation2)/d133014e34-true-d133069e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d133014e34-true-d133069e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/*[not(@xsi:nil = 'true')][not(self::hl7:nonXMLBody | self::hl7:structuredBody[ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1']] | self::hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])])]"
         id="d129636e179-true-d133264e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e179-true-d133264e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:nonXMLBody | hl7:structuredBody[ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1']] | hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])] (rule-reference: d129636e179-true-d133264e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:nonXMLBody/*[not(@xsi:nil = 'true')][not(self::hl7:text)]"
         id="d129636e185-true-d133278e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e185-true-d133278e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:text (rule-reference: d129636e185-true-d133278e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:component[hl7:section])]"
         id="d129636e191-true-d133317e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e191-true-d133317e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:component[hl7:section] (rule-reference: d129636e191-true-d133317e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1']]/hl7:component[hl7:section]/*[not(@xsi:nil = 'true')][not(self::hl7:section[not(@nullFlavor)][hl7:text])]"
         id="d129636e196-true-d133364e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e196-true-d133364e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[not(@nullFlavor)][hl7:text] (rule-reference: d129636e196-true-d133364e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1']]/hl7:component[hl7:section]/hl7:section[not(@nullFlavor)][hl7:text]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId | self::hl7:id | self::hl7:code | self::hl7:title | self::hl7:text | self::hl7:author[not(@nullFlavor)] | self::hl7:author[@nullFlavor] | self::hl7:entry)]"
         id="d129636e199-true-d133400e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e199-true-d133400e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId | hl7:id | hl7:code | hl7:title | hl7:text | hl7:author[not(@nullFlavor)] | hl7:author[@nullFlavor] | hl7:entry (rule-reference: d129636e199-true-d133400e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1']]/hl7:component[hl7:section]/hl7:section[not(@nullFlavor)][hl7:text]/hl7:author[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time | self::hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization])]"
         id="d133433e133-true-d133467e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d133433e133-true-d133467e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time | hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization] (rule-reference: d133433e133-true-d133467e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1']]/hl7:component[hl7:section]/hl7:section[not(@nullFlavor)][hl7:text]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization[not(@nullFlavor)])]"
         id="d133433e180-true-d133528e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d133433e180-true-d133528e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:telecom | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization[not(@nullFlavor)] (rule-reference: d133433e180-true-d133528e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1']]/hl7:component[hl7:section]/hl7:section[not(@nullFlavor)][hl7:text]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d133433e244-true-d133561e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d133433e244-true-d133561e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d133433e244-true-d133561e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1']]/hl7:component[hl7:section]/hl7:section[not(@nullFlavor)][hl7:text]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName | self::hl7:softwareName)]"
         id="d133433e255-true-d133587e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d133433e255-true-d133587e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName | hl7:softwareName (rule-reference: d133433e255-true-d133587e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1']]/hl7:component[hl7:section]/hl7:section[not(@nullFlavor)][hl7:text]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d133433e277-true-d133618e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d133433e277-true-d133618e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d133433e277-true-d133618e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1']]/hl7:component[hl7:section]/hl7:section[not(@nullFlavor)][hl7:text]/hl7:author[@nullFlavor]/*[not(@xsi:nil = 'true')][not(self::hl7:time[@nullFlavor = 'NA'] | self::hl7:assignedAuthor[@nullFlavor = 'NA'][hl7:id[@nullFlavor = 'NA']])]"
         id="d133433e284-true-d133657e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d133433e284-true-d133657e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time[@nullFlavor = 'NA'] | hl7:assignedAuthor[@nullFlavor = 'NA'][hl7:id[@nullFlavor = 'NA']] (rule-reference: d133433e284-true-d133657e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1']]/hl7:component[hl7:section]/hl7:section[not(@nullFlavor)][hl7:text]/hl7:author[@nullFlavor]/hl7:assignedAuthor[@nullFlavor = 'NA'][hl7:id[@nullFlavor = 'NA']]/*[not(@xsi:nil = 'true')][not(self::hl7:id[@nullFlavor = 'NA'])]"
         id="d133433e302-true-d133676e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.90004-2015-02-17T000000.html"
              test="not(.)">(AuthorElements)/d133433e302-true-d133676e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[@nullFlavor = 'NA'] (rule-reference: d133433e302-true-d133676e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/*[not(@xsi:nil = 'true')][not(self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.1']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.3']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.4']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.5']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.6']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.7']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.8']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.9']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.11']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.12']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.13']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.14']]] | self::hl7:component[hl7:section/hl7:templateId/@root='1.2.40.0.34.11.1.2.6'] | self::hl7:component[hl7:section/hl7:templateId/@root='1.2.40.0.34.11.1.2.7'] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.15']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.5']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.12.2.2']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.1']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.2']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]])]"
         id="d129636e201-true-d134112e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e201-true-d134112e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.1']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.3']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.4']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.5']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.6']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.7']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.8']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.9']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.11']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.12']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.13']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.14']]] | hl7:component[hl7:section/hl7:templateId/@root='1.2.40.0.34.11.1.2.6'] | hl7:component[hl7:section/hl7:templateId/@root='1.2.40.0.34.11.1.2.7'] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.15']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.5']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.12.2.2']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.1']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.2']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]] (rule-reference: d129636e201-true-d134112e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.1']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.1']])]"
         id="d129636e206-true-d134141e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e206-true-d134141e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.1']] (rule-reference: d129636e206-true-d134141e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.1']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.1'] | self::hl7:code[(@code = 'BRIEFT' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text | self::hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.2']]])]"
         id="d134117e3-true-d134191e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134117e3-true-d134191e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.1'] | hl7:code[(@code = 'BRIEFT' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text | hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.2']]] (rule-reference: d134117e3-true-d134191e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.1']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.1']]/hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.2']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.2']])]"
         id="d134117e39-true-d134233e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134117e39-true-d134233e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.2']] (rule-reference: d134117e39-true-d134233e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.1']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.1']]/hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.2']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.3.2'] | self::hl7:value[not(@nullFlavor)])]"
         id="d134218e7-true-d134255e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134218e7-true-d134255e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.3.2'] | hl7:value[not(@nullFlavor)] (rule-reference: d134218e7-true-d134255e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']])]"
         id="d129636e210-true-d134294e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e210-true-d134294e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']] (rule-reference: d129636e210-true-d134294e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.2.1'] | self::hl7:code[(@code = 'PFDIAG' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:entry)]"
         id="d134271e5-true-d134327e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134271e5-true-d134327e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.2.1'] | hl7:code[(@code = 'PFDIAG' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:entry (rule-reference: d134271e5-true-d134327e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']])]"
         id="d129636e215-true-d134397e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e215-true-d134397e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']] (rule-reference: d129636e215-true-d134397e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.2.2'] | self::hl7:code[(@code = 'PFDIAG' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]])]"
         id="d134361e5-true-d134455e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134361e5-true-d134455e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.2.2'] | hl7:code[(@code = 'PFDIAG' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]] (rule-reference: d134361e5-true-d134455e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']])]"
         id="d134361e14-true-d134520e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134361e14-true-d134520e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']] (rule-reference: d134361e14-true-d134520e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] | self::hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27'] | self::hl7:id | self::hl7:code[@nullFlavor = 'NA'] | self::hl7:statusCode[@code = 'active' or @code = 'suspended' or @code = 'completed' or @code = 'aborted'] | self::hl7:effectiveTime | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]])]"
         id="d134484e3-true-d134611e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134484e3-true-d134611e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] | hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27'] | hl7:id | hl7:code[@nullFlavor = 'NA'] | hl7:statusCode[@code = 'active' or @code = 'suspended' or @code = 'completed' or @code = 'aborted'] | hl7:effectiveTime | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]] (rule-reference: d134484e3-true-d134611e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d134484e53-true-d134676e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134484e53-true-d134676e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d134484e53-true-d134676e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']])]"
         id="d134484e61-true-d134712e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134484e61-true-d134712e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']] (rule-reference: d134484e61-true-d134712e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] | self::hl7:id | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.35-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime[not(@nullFlavor)] | self::hl7:value)]"
         id="d134691e7-true-d134783e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134691e7-true-d134783e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] | hl7:id | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.35-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime[not(@nullFlavor)] | hl7:value (rule-reference: d134691e7-true-d134783e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.35-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText)]"
         id="d134691e61-true-d134823e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134691e61-true-d134823e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText (rule-reference: d134691e61-true-d134823e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d134691e88-true-d134842e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134691e88-true-d134842e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d134691e88-true-d134842e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]/hl7:effectiveTime[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d134691e102-true-d134872e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134691e102-true-d134872e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d134691e102-true-d134872e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]/hl7:value/*[not(@xsi:nil = 'true')][not(self::hl7:originalText[not(@nullFlavor)] | self::hl7:qualifier[hl7:name[@code='7']] | self::hl7:qualifier[hl7:name[@code='8']] | self::hl7:qualifier[hl7:name[@code='ART']] | self::hl7:qualifier[hl7:name[@code='TYP']] | self::hl7:qualifier[hl7:name[@code='STAT']] | self::hl7:translation)]"
         id="d134691e123-true-d134906e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134691e123-true-d134906e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText[not(@nullFlavor)] | hl7:qualifier[hl7:name[@code='7']] | hl7:qualifier[hl7:name[@code='8']] | hl7:qualifier[hl7:name[@code='ART']] | hl7:qualifier[hl7:name[@code='TYP']] | hl7:qualifier[hl7:name[@code='STAT']] | hl7:translation (rule-reference: d134691e123-true-d134906e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]/hl7:value/hl7:qualifier[hl7:name[@code='7']]/*[not(@xsi:nil = 'true')][not(self::hl7:name[(@code = '7' and @codeSystem = '2.16.840.1.113883.3.7.1.0')] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.176-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.176-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code])]"
         id="d134691e170-true-d134936e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134691e170-true-d134936e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[(@code = '7' and @codeSystem = '2.16.840.1.113883.3.7.1.0')] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.176-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.176-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code] (rule-reference: d134691e170-true-d134936e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]/hl7:value/hl7:qualifier[hl7:name[@code='8']]/*[not(@xsi:nil = 'true')][not(self::hl7:name[(@code = '8' and @codeSystem = '2.16.840.1.113883.3.7.1.0')] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.168-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d134691e192-true-d134975e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134691e192-true-d134975e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[(@code = '8' and @codeSystem = '2.16.840.1.113883.3.7.1.0')] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.168-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d134691e192-true-d134975e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]/hl7:value/hl7:qualifier[hl7:name[@code='ART']]/*[not(@xsi:nil = 'true')][not(self::hl7:name[(@code = 'ART' and @codeSystem = '1.2.40.0.34.5.211')] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.67-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d134691e208-true-d135014e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134691e208-true-d135014e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[(@code = 'ART' and @codeSystem = '1.2.40.0.34.5.211')] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.67-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d134691e208-true-d135014e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]/hl7:value/hl7:qualifier[hl7:name[@code='TYP']]/*[not(@xsi:nil = 'true')][not(self::hl7:name[(@code = 'TYP' and @codeSystem = '1.2.40.0.34.5.211')] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.69-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d134691e224-true-d135053e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134691e224-true-d135053e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[(@code = 'TYP' and @codeSystem = '1.2.40.0.34.5.211')] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.69-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d134691e224-true-d135053e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.2']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.3.3.1'] and hl7:templateId[@root = '1.2.40.0.34.11.1.3.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28']]/hl7:value/hl7:qualifier[hl7:name[@code='STAT']]/*[not(@xsi:nil = 'true')][not(self::hl7:name[(@code = 'STAT' and @codeSystem = '1.2.40.0.34.5.211')] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.68-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d134691e240-true-d135092e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d134691e240-true-d135092e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[(@code = 'STAT' and @codeSystem = '1.2.40.0.34.5.211')] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.68-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d134691e240-true-d135092e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.3']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.3']])]"
         id="d129636e220-true-d135151e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e220-true-d135151e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.3']] (rule-reference: d129636e220-true-d135151e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.3']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.2.3'] | self::hl7:code[(@code = 'PFMOB' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]])]"
         id="d135116e3-true-d135204e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d135116e3-true-d135204e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.2.3'] | hl7:code[(@code = 'PFMOB' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]] (rule-reference: d135116e3-true-d135204e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.3']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']])]"
         id="d135230e4-true-d135248e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d135230e4-true-d135248e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']] (rule-reference: d135230e4-true-d135248e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.3']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | self::hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title | self::hl7:text)]"
         id="d135233e4-true-d135282e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d135233e4-true-d135282e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title | hl7:text (rule-reference: d135233e4-true-d135282e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.3']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']])]"
         id="d135230e11-true-d135324e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d135230e11-true-d135324e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']] (rule-reference: d135230e11-true-d135324e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.3']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | self::hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text)]"
         id="d135309e4-true-d135358e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d135309e4-true-d135358e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text (rule-reference: d135309e4-true-d135358e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.4']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.4']])]"
         id="d129636e223-true-d135420e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e223-true-d135420e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.4']] (rule-reference: d129636e223-true-d135420e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.4']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.2.4'] | self::hl7:code[(@code = 'PFKLEI' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]])]"
         id="d135385e3-true-d135473e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d135385e3-true-d135473e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.2.4'] | hl7:code[(@code = 'PFKLEI' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]] (rule-reference: d135385e3-true-d135473e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.4']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']])]"
         id="d135499e4-true-d135517e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d135499e4-true-d135517e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']] (rule-reference: d135499e4-true-d135517e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.4']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | self::hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title | self::hl7:text)]"
         id="d135502e4-true-d135551e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d135502e4-true-d135551e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title | hl7:text (rule-reference: d135502e4-true-d135551e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.4']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']])]"
         id="d135499e11-true-d135593e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d135499e11-true-d135593e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']] (rule-reference: d135499e11-true-d135593e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.4']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | self::hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text)]"
         id="d135578e4-true-d135627e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d135578e4-true-d135627e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text (rule-reference: d135578e4-true-d135627e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.5']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.5']])]"
         id="d129636e226-true-d135689e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e226-true-d135689e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.5']] (rule-reference: d129636e226-true-d135689e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.5']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.2.5'] | self::hl7:code[(@code = 'PFERN' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]])]"
         id="d135654e3-true-d135742e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d135654e3-true-d135742e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.2.5'] | hl7:code[(@code = 'PFERN' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]] (rule-reference: d135654e3-true-d135742e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.5']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']])]"
         id="d135768e4-true-d135786e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d135768e4-true-d135786e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']] (rule-reference: d135768e4-true-d135786e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.5']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | self::hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title | self::hl7:text)]"
         id="d135771e4-true-d135820e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d135771e4-true-d135820e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title | hl7:text (rule-reference: d135771e4-true-d135820e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.5']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']])]"
         id="d135768e11-true-d135862e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d135768e11-true-d135862e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']] (rule-reference: d135768e11-true-d135862e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.5']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | self::hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text)]"
         id="d135847e4-true-d135896e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d135847e4-true-d135896e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text (rule-reference: d135847e4-true-d135896e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.6']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.6']])]"
         id="d129636e229-true-d135958e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e229-true-d135958e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.6']] (rule-reference: d129636e229-true-d135958e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.6']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.2.6'] | self::hl7:code[(@code = 'PFAUS' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]])]"
         id="d135923e3-true-d136011e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d135923e3-true-d136011e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.2.6'] | hl7:code[(@code = 'PFAUS' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]] (rule-reference: d135923e3-true-d136011e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']])]"
         id="d136037e4-true-d136055e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136037e4-true-d136055e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']] (rule-reference: d136037e4-true-d136055e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | self::hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title | self::hl7:text)]"
         id="d136040e4-true-d136089e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136040e4-true-d136089e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title | hl7:text (rule-reference: d136040e4-true-d136089e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']])]"
         id="d136037e11-true-d136131e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136037e11-true-d136131e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']] (rule-reference: d136037e11-true-d136131e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | self::hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text)]"
         id="d136116e4-true-d136165e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136116e4-true-d136165e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text (rule-reference: d136116e4-true-d136165e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.7']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.7']])]"
         id="d129636e232-true-d136227e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e232-true-d136227e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.7']] (rule-reference: d129636e232-true-d136227e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.7']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.7']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.2.7'] | self::hl7:code[(@code = 'PFHAUT' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]])]"
         id="d136192e3-true-d136280e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136192e3-true-d136280e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.2.7'] | hl7:code[(@code = 'PFHAUT' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]] (rule-reference: d136192e3-true-d136280e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.7']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.7']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']])]"
         id="d136306e4-true-d136324e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136306e4-true-d136324e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']] (rule-reference: d136306e4-true-d136324e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.7']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.7']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | self::hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title | self::hl7:text)]"
         id="d136309e4-true-d136358e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136309e4-true-d136358e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title | hl7:text (rule-reference: d136309e4-true-d136358e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.7']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.7']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']])]"
         id="d136306e11-true-d136400e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136306e11-true-d136400e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']] (rule-reference: d136306e11-true-d136400e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.7']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.7']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | self::hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text)]"
         id="d136385e4-true-d136434e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136385e4-true-d136434e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text (rule-reference: d136385e4-true-d136434e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.8']])]"
         id="d129636e235-true-d136496e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e235-true-d136496e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.8']] (rule-reference: d129636e235-true-d136496e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.2.8'] | self::hl7:code[(@code = 'PFATM' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]])]"
         id="d136461e3-true-d136549e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136461e3-true-d136549e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.2.8'] | hl7:code[(@code = 'PFATM' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]] (rule-reference: d136461e3-true-d136549e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']])]"
         id="d136575e4-true-d136593e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136575e4-true-d136593e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']] (rule-reference: d136575e4-true-d136593e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | self::hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title | self::hl7:text)]"
         id="d136578e4-true-d136627e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136578e4-true-d136627e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title | hl7:text (rule-reference: d136578e4-true-d136627e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']])]"
         id="d136575e11-true-d136669e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136575e11-true-d136669e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']] (rule-reference: d136575e11-true-d136669e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | self::hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text)]"
         id="d136654e4-true-d136703e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136654e4-true-d136703e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text (rule-reference: d136654e4-true-d136703e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.9']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.9']])]"
         id="d129636e239-true-d136765e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e239-true-d136765e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.9']] (rule-reference: d129636e239-true-d136765e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.9']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.2.9'] | self::hl7:code[(@code = 'PFSCHL' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]])]"
         id="d136730e3-true-d136818e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136730e3-true-d136818e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.2.9'] | hl7:code[(@code = 'PFSCHL' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]] (rule-reference: d136730e3-true-d136818e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.9']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']])]"
         id="d136844e4-true-d136862e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136844e4-true-d136862e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']] (rule-reference: d136844e4-true-d136862e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.9']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | self::hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title | self::hl7:text)]"
         id="d136847e4-true-d136896e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136847e4-true-d136896e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title | hl7:text (rule-reference: d136847e4-true-d136896e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.9']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']])]"
         id="d136844e11-true-d136938e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136844e11-true-d136938e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']] (rule-reference: d136844e11-true-d136938e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.9']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | self::hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text)]"
         id="d136923e4-true-d136972e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136923e4-true-d136972e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text (rule-reference: d136923e4-true-d136972e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4']])]"
         id="d129636e242-true-d137037e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e242-true-d137037e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4']] (rule-reference: d129636e242-true-d137037e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4'] | self::hl7:code[(@code = '38212-7' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]])]"
         id="d136999e3-true-d137098e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d136999e3-true-d137098e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4'] | hl7:code[(@code = '38212-7' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]] (rule-reference: d136999e3-true-d137098e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']])]"
         id="d137130e4-true-d137148e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137130e4-true-d137148e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']] (rule-reference: d137130e4-true-d137148e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | self::hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title | self::hl7:text)]"
         id="d137133e4-true-d137182e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137133e4-true-d137182e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title | hl7:text (rule-reference: d137133e4-true-d137182e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']])]"
         id="d137130e11-true-d137224e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137130e11-true-d137224e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']] (rule-reference: d137130e11-true-d137224e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.10'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.20.2.4']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | self::hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text)]"
         id="d137209e4-true-d137258e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137209e4-true-d137258e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text (rule-reference: d137209e4-true-d137258e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.11']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.11']])]"
         id="d129636e245-true-d137320e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e245-true-d137320e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.11']] (rule-reference: d129636e245-true-d137320e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.11']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.2.11'] | self::hl7:code[(@code = 'PFORIE' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]])]"
         id="d137285e3-true-d137373e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137285e3-true-d137373e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.2.11'] | hl7:code[(@code = 'PFORIE' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]] (rule-reference: d137285e3-true-d137373e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']])]"
         id="d137399e4-true-d137417e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137399e4-true-d137417e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']] (rule-reference: d137399e4-true-d137417e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | self::hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title | self::hl7:text)]"
         id="d137402e4-true-d137451e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137402e4-true-d137451e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title | hl7:text (rule-reference: d137402e4-true-d137451e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']])]"
         id="d137399e11-true-d137493e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137399e11-true-d137493e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']] (rule-reference: d137399e11-true-d137493e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | self::hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text)]"
         id="d137478e4-true-d137527e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137478e4-true-d137527e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text (rule-reference: d137478e4-true-d137527e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.12']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.12']])]"
         id="d129636e248-true-d137589e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e248-true-d137589e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.12']] (rule-reference: d129636e248-true-d137589e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.12']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.12']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.2.12'] | self::hl7:code[(@code = 'PFSOZV' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]])]"
         id="d137554e3-true-d137642e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137554e3-true-d137642e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.2.12'] | hl7:code[(@code = 'PFSOZV' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]] (rule-reference: d137554e3-true-d137642e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.12']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.12']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']])]"
         id="d137668e4-true-d137686e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137668e4-true-d137686e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']] (rule-reference: d137668e4-true-d137686e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.12']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.12']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | self::hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title | self::hl7:text)]"
         id="d137671e4-true-d137720e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137671e4-true-d137720e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title | hl7:text (rule-reference: d137671e4-true-d137720e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.12']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.12']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']])]"
         id="d137668e11-true-d137762e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137668e11-true-d137762e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']] (rule-reference: d137668e11-true-d137762e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.12']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.12']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | self::hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text)]"
         id="d137747e4-true-d137796e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137747e4-true-d137796e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text (rule-reference: d137747e4-true-d137796e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.13']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.13']])]"
         id="d129636e251-true-d137858e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e251-true-d137858e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.13']] (rule-reference: d129636e251-true-d137858e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.13']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.13']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.2.13'] | self::hl7:code[(@code = 'PFKOMM' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]])]"
         id="d137823e3-true-d137911e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137823e3-true-d137911e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.2.13'] | hl7:code[(@code = 'PFKOMM' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]] (rule-reference: d137823e3-true-d137911e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.13']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.13']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']])]"
         id="d137937e4-true-d137955e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137937e4-true-d137955e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']] (rule-reference: d137937e4-true-d137955e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.13']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.13']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | self::hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title | self::hl7:text)]"
         id="d137940e4-true-d137989e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137940e4-true-d137989e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title | hl7:text (rule-reference: d137940e4-true-d137989e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.13']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.13']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']])]"
         id="d137937e11-true-d138031e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d137937e11-true-d138031e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']] (rule-reference: d137937e11-true-d138031e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.13']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.13']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | self::hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text)]"
         id="d138016e4-true-d138065e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d138016e4-true-d138065e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text (rule-reference: d138016e4-true-d138065e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.14']])]"
         id="d129636e254-true-d138127e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e254-true-d138127e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.14']] (rule-reference: d129636e254-true-d138127e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.14']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.2.14'] | self::hl7:code[(@code = 'PFROLL' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]])]"
         id="d138092e3-true-d138180e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d138092e3-true-d138180e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.2.14'] | hl7:code[(@code = 'PFROLL' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]] (rule-reference: d138092e3-true-d138180e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.14']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.14']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']])]"
         id="d138206e4-true-d138224e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d138206e4-true-d138224e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']] (rule-reference: d138206e4-true-d138224e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.14']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.14']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | self::hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title | self::hl7:text)]"
         id="d138209e4-true-d138258e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d138209e4-true-d138258e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title | hl7:text (rule-reference: d138209e4-true-d138258e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.14']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.14']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']])]"
         id="d138206e11-true-d138300e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d138206e11-true-d138300e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']] (rule-reference: d138206e11-true-d138300e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.14']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.14']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | self::hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text)]"
         id="d138285e4-true-d138334e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d138285e4-true-d138334e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text (rule-reference: d138285e4-true-d138334e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section/hl7:templateId/@root='1.2.40.0.34.11.1.2.6']/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.16']])]"
         id="d129636e258-true-d138383e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e258-true-d138383e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.16']] (rule-reference: d129636e258-true-d138383e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section/hl7:templateId/@root='1.2.40.0.34.11.1.2.6']/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.16']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.6'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.25'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.16'] | self::hl7:code[(@code = '8716-3' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title | self::hl7:text | self::hl7:entry)]"
         id="d138361e5-true-d138432e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d138361e5-true-d138432e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.6'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.25'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.16'] | hl7:code[(@code = '8716-3' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title | hl7:text | hl7:entry (rule-reference: d138361e5-true-d138432e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section/hl7:templateId/@root='1.2.40.0.34.11.1.2.7']/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.5.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.16']])]"
         id="d129636e263-true-d138512e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e263-true-d138512e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.5.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.16']] (rule-reference: d129636e263-true-d138512e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section/hl7:templateId/@root='1.2.40.0.34.11.1.2.7']/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.5.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.16']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.7'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.5.3.2'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.25'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.16'] | self::hl7:code[(@code = '8716-3' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title | self::hl7:text | self::hl7:entry[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1']]])]"
         id="d138478e5-true-d138595e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d138478e5-true-d138595e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.7'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.5.3.2'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.25'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.16'] | hl7:code[(@code = '8716-3' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title | hl7:text | hl7:entry[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1']]] (rule-reference: d138478e5-true-d138595e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section/hl7:templateId/@root='1.2.40.0.34.11.1.2.7']/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.5.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.16']]/hl7:entry[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1']]]/*[not(@xsi:nil = 'true')][not(self::hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1']])]"
         id="d138478e33-true-d138675e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d138478e33-true-d138675e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1']] (rule-reference: d138478e33-true-d138675e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section/hl7:templateId/@root='1.2.40.0.34.11.1.2.7']/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.5.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.16']]/hl7:entry[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1'] | self::hl7:id | self::hl7:code[(@code = '46680005' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime | self::hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.31']]])]"
         id="d138642e7-true-d138762e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d138642e7-true-d138762e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1'] | hl7:id | hl7:code[(@code = '46680005' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime | hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.31']]] (rule-reference: d138642e7-true-d138762e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section/hl7:templateId/@root='1.2.40.0.34.11.1.2.7']/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.5.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.16']]/hl7:entry[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.31']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.31']])]"
         id="d138642e88-true-d138837e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d138642e88-true-d138837e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.31']] (rule-reference: d138642e88-true-d138837e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section/hl7:templateId/@root='1.2.40.0.34.11.1.2.7']/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.5.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.16']]/hl7:entry[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.31']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.31']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.3.4'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.2'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.31'] | self::hl7:id | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.34-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime | self::hl7:value[not(@nullFlavor)] | self::hl7:value[@nullFlavor='NA'])]"
         id="d138813e5-true-d138911e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d138813e5-true-d138911e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.3.4'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.2'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.31'] | hl7:id | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.34-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime | hl7:value[not(@nullFlavor)] | hl7:value[@nullFlavor='NA'] (rule-reference: d138813e5-true-d138911e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section/hl7:templateId/@root='1.2.40.0.34.11.1.2.7']/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.5.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.16']]/hl7:entry[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.31']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.31']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.34-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText[not(@nullFlavor)])]"
         id="d138813e53-true-d138957e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d138813e53-true-d138957e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText[not(@nullFlavor)] (rule-reference: d138813e53-true-d138957e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section/hl7:templateId/@root='1.2.40.0.34.11.1.2.7']/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.5.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.16']]/hl7:entry[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.11.1.3.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.32'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.35'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.1']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.31']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.1.3.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.13.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.31']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d138813e80-true-d138976e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d138813e80-true-d138976e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d138813e80-true-d138976e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']])]"
         id="d129636e268-true-d139032e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e268-true-d139032e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']] (rule-reference: d129636e268-true-d139032e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.2.18'] | self::hl7:code[(@code = 'PFMEDBEH' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]])]"
         id="d139003e3-true-d139093e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139003e3-true-d139093e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.2.18'] | hl7:code[(@code = 'PFMEDBEH' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]] (rule-reference: d139003e3-true-d139093e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']])]"
         id="d139003e30-true-d139135e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139003e30-true-d139135e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']] (rule-reference: d139003e30-true-d139135e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | self::hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title | self::hl7:text)]"
         id="d139124e1-true-d139169e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139124e1-true-d139169e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.8'] | hl7:code[(@code = '51898-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title | hl7:text (rule-reference: d139124e1-true-d139169e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']])]"
         id="d139003e33-true-d139211e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139003e33-true-d139211e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']] (rule-reference: d139003e33-true-d139211e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | self::hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text)]"
         id="d139200e1-true-d139245e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139200e1-true-d139245e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text (rule-reference: d139200e1-true-d139245e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.15']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.15']])]"
         id="d129636e271-true-d139295e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e271-true-d139295e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.15']] (rule-reference: d129636e271-true-d139295e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.15']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.3.2.15'] | self::hl7:code[(@code = 'PFMED' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]])]"
         id="d139272e3-true-d139343e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139272e3-true-d139343e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.3.2.15'] | hl7:code[(@code = 'PFMED' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]] (rule-reference: d139272e3-true-d139343e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.15']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']])]"
         id="d139272e26-true-d139385e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139272e26-true-d139385e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']] (rule-reference: d139272e26-true-d139385e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.15']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | self::hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text)]"
         id="d139374e1-true-d139419e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139374e1-true-d139419e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.9'] | hl7:code[(@code = 'RES' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text (rule-reference: d139374e1-true-d139419e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.5']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.5']])]"
         id="d129636e274-true-d139461e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e274-true-d139461e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.5']] (rule-reference: d129636e274-true-d139461e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.5']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.5'] | self::hl7:code[(@code = 'ANM' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text)]"
         id="d139446e3-true-d139495e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139446e3-true-d139495e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.5'] | hl7:code[(@code = 'ANM' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text (rule-reference: d139446e3-true-d139495e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.12.2.2']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.12.2.2']])]"
         id="d129636e277-true-d139537e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e277-true-d139537e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.12.2.2']] (rule-reference: d129636e277-true-d139537e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.12.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.12.2.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.12.2.2'] | self::hl7:code[(@code = 'PUBUMF' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)])]"
         id="d139522e3-true-d139571e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139522e3-true-d139571e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.12.2.2'] | hl7:code[(@code = 'PUBUMF' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] (rule-reference: d139522e3-true-d139571e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.1']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.1']])]"
         id="d129636e281-true-d139619e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e281-true-d139619e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.1']] (rule-reference: d129636e281-true-d139619e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.1']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.4'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.4'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.34'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.1'] | self::hl7:code | self::hl7:title | self::hl7:text)]"
         id="d139598e3-true-d139666e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139598e3-true-d139666e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.4'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.34'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.1'] | hl7:code | hl7:title | hl7:text (rule-reference: d139598e3-true-d139666e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.2']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.2']])]"
         id="d129636e284-true-d139729e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e284-true-d139729e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.2']] (rule-reference: d129636e284-true-d139729e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.2'] | self::hl7:code[(@code = 'ABBEM' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text | self::hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]])]"
         id="d139705e3-true-d139779e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139705e3-true-d139779e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.2'] | hl7:code[(@code = 'ABBEM' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text | hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]] (rule-reference: d139705e3-true-d139779e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.2']]/hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']])]"
         id="d139705e31-true-d139821e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139705e31-true-d139821e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']] (rule-reference: d139705e31-true-d139821e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.2']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.2']]/hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.3.1'] | self::hl7:value[not(@nullFlavor)])]"
         id="d139810e1-true-d139843e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139810e1-true-d139843e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.3.1'] | hl7:value[not(@nullFlavor)] (rule-reference: d139810e1-true-d139843e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']])]"
         id="d129636e287-true-d139883e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d129636e287-true-d139883e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']] (rule-reference: d129636e287-true-d139883e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.2.3'] | self::hl7:code[(@code = 'BEIL' and @codeSystem = '1.2.40.0.34.5.40')] | self::hl7:title | self::hl7:text | self::hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]])]"
         id="d139859e3-true-d139933e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139859e3-true-d139933e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.2.3'] | hl7:code[(@code = 'BEIL' and @codeSystem = '1.2.40.0.34.5.40')] | hl7:title | hl7:text | hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]] (rule-reference: d139859e3-true-d139933e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]/hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']])]"
         id="d139859e34-true-d139975e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139859e34-true-d139975e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']] (rule-reference: d139859e34-true-d139975e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.12']]/hl7:component/hl7:structuredBody[not(ancestor::*//hl7:templateId[@root='1.2.40.0.34.11.12.0.1'])]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]/hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.3.1'] | self::hl7:value[not(@nullFlavor)])]"
         id="d139960e7-true-d139997e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230317T082348/tmp-1.2.40.0.34.11.12-2015-09-18T000000.html"
              test="not(.)">(Pflegesituationsbericht)/d139960e7-true-d139997e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.3.1'] | hl7:value[not(@nullFlavor)] (rule-reference: d139960e7-true-d139997e0)</assert>
   </rule>
</pattern>
