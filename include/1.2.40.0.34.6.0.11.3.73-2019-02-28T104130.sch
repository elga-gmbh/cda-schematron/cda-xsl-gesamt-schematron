<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.73
Name: Device Information Organizer
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130">
   <title>Device Information Organizer</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]"
         id="d41e51305-false-d3594626e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@classCode) = ('CLUSTER')">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von classCode MUSS 'CLUSTER' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@moodCode) = ('EVN')">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73']) &gt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73']) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']) &gt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:id[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:id[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:id[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:id[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:statusCode[@code = 'completed']) &gt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:statusCode[@code = 'completed'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:statusCode[@code = 'completed']) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:statusCode[@code = 'completed'] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount" value="count(hl7:effectiveTime | hl7:effectiveTime)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="$elmcount &gt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Auswahl (hl7:effectiveTime  oder  hl7:effectiveTime) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="$elmcount &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Auswahl (hl7:effectiveTime  oder  hl7:effectiveTime) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:participant[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]) &gt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:participant[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:participant[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:participant[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.74'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.18'][@extension = '2015-08-17']]]) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.74'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.18'][@extension = '2015-08-17']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.75'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.19'][@extension = '2015-08-17']]]) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.75'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.19'][@extension = '2015-08-17']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.76'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.20'][@extension = '2015-08-17']]]) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.76'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.20'][@extension = '2015-08-17']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.77'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.21'][@extension = '2015-08-17']]]) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.77'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.21'][@extension = '2015-08-17']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.78'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.22'][@extension = '2015-08-17']]]) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.78'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.22'][@extension = '2015-08-17']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.79'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.23'][@extension = '2015-08-17']]]) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.79'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.23'][@extension = '2015-08-17']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.80'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.24'][@extension = '2015-08-17']]]) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.80'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.24'][@extension = '2015-08-17']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.82'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.26'][@extension = '2015-08-17']]]) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.82'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.26'][@extension = '2015-08-17']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.84'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.27'][@extension = '2015-08-17']]]) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.84'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.27'][@extension = '2015-08-17']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73']
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73']"
         id="d41e51311-false-d3594998e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceInformationOrganizer): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.73')">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.73' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']"
         id="d41e51320-false-d3595013e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceInformationOrganizer): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.36.4')">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von root MUSS '2.16.840.1.113883.10.20.36.4' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@extension) = ('2015-08-17')">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von extension MUSS '2015-08-17' sein. Gefunden: "<value-of select="@extension"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_DeviceInformationOrganizer): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:id[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]"
         id="d41e51344-false-d3595043e0">
      <extends rule="CD.IPS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceInformationOrganizer): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="@code">(atcdabbr_entry_DeviceInformationOrganizer): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atcdabbr_entry_DeviceInformationOrganizer): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="@codeSystem">(atcdabbr_entry_DeviceInformationOrganizer): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@codeSystem,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@codeSystem,'^[A-Za-z][A-Za-z\d\-]*$')">(atcdabbr_entry_DeviceInformationOrganizer): Attribute @codeSystem MUSS vom Datentyp 'uid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceInformationOrganizer): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_DeviceInformationOrganizer): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']) &gt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:translation[@codeSystem = '2.16.840.1.113883.6.24'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="count(hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']) &lt;= 1">(atcdabbr_entry_DeviceInformationOrganizer): Element hl7:translation[@codeSystem = '2.16.840.1.113883.6.24'] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']"
         id="d41e51362-false-d3595087e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceInformationOrganizer): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="@code">(atcdabbr_entry_DeviceInformationOrganizer): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@codeSystem) = ('2.16.840.1.113883.6.24')">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von codeSystem MUSS '2.16.840.1.113883.6.24' sein. Gefunden: "<value-of select="@codeSystem"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@codeSystemName) = ('MDC') or not(@codeSystemName)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von codeSystemName MUSS 'MDC' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceInformationOrganizer): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:translation
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:translation"
         id="d41e51376-false-d3595111e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceInformationOrganizer): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="@code">(atcdabbr_entry_DeviceInformationOrganizer): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atcdabbr_entry_DeviceInformationOrganizer): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="@codeSystem">(atcdabbr_entry_DeviceInformationOrganizer): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@codeSystem,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@codeSystem,'^[A-Za-z][A-Za-z\d\-]*$')">(atcdabbr_entry_DeviceInformationOrganizer): Attribute @codeSystem MUSS vom Datentyp 'uid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceInformationOrganizer): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_DeviceInformationOrganizer): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]/ips:designation
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]/ips:designation"
         id="d41e51390-false-d3595138e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atcdabbr_entry_DeviceInformationOrganizer): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="@language">(atcdabbr_entry_DeviceInformationOrganizer): Attribut @language MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="not(@language) or (string-length(@language) &gt; 0 and not(matches(@language,'\s')))">(atcdabbr_entry_DeviceInformationOrganizer): Attribute @language MUSS vom Datentyp 'cs' sein  - '<value-of select="@language"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:statusCode[@code = 'completed']
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:statusCode[@code = 'completed']"
         id="d41e51399-false-d3595155e0">
      <extends rule="CS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CS')">(atcdabbr_entry_DeviceInformationOrganizer): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="@nullFlavor or (@code='completed')">(atcdabbr_entry_DeviceInformationOrganizer): Der Elementinhalt MUSS einer von 'code 'completed'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:effectiveTime
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:effectiveTime"
         id="d41e51406-false-d3595171e0">
      <extends rule="IVL_TS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVL_TS')">(atcdabbr_entry_DeviceInformationOrganizer): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVL_TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="@low">(atcdabbr_entry_DeviceInformationOrganizer): Attribut @low MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="@high">(atcdabbr_entry_DeviceInformationOrganizer): Attribut @high MUSS vorkommen.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:effectiveTime
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:effectiveTime"
         id="d41e51447-false-d3595188e0">
      <extends rule="TS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_entry_DeviceInformationOrganizer): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="not(*)">(atcdabbr_entry_DeviceInformationOrganizer): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="@value">(atcdabbr_entry_DeviceInformationOrganizer): Attribut @value MUSS vorkommen.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:participant[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:participant[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('DEV')">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'DEV' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextControlCode) = ('OP') or not(@contextControlCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextControlCode MUSS 'OP' sein. Gefunden: "<value-of select="@contextControlCode"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.74'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.18'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.74'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.18'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.75'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.19'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.75'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.19'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.76'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.20'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.76'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.20'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.77'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.21'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.77'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.21'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.78'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.22'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.78'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.22'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.79'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.23'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.79'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.23'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.80'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.24'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.80'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.24'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.82'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.26'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.82'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.26'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.84'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.27'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.84'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.27'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.85'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.29'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.86'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.30'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.87'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.3'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.87'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.3'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.73
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceInformationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.73'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.4'][@extension = '2015-08-17']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.73-2019-02-28T104130.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_DeviceInformationOrganizer): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>
</pattern>
