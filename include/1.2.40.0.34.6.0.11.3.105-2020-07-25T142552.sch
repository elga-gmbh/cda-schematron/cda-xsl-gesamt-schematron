<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.105
Name: EMS Act Betreuung
Description: Dieses Act Element stellt Informationen dar ob sich ein Patient in Betreuung findet. z.B. Krankenanstalt, Heim, Schule, Kindergarten
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.105-2020-07-25T142552">
   <title>EMS Act Betreuung</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.105
Context: *[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]]
Item: (epims_entry_EmsBetreuung)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.105
Context: *[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]
Item: (epims_entry_EmsBetreuung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]"
         id="d42e15813-false-d145189e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.105-2020-07-25T142552.html"
              test="string(@moodCode) = ('EVN')">(epims_entry_EmsBetreuung): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.105-2020-07-25T142552.html"
              test="string(@classCode) = ('INFRM')">(epims_entry_EmsBetreuung): Der Wert von classCode MUSS 'INFRM' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.105-2020-07-25T142552.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']) &gt;= 1">(epims_entry_EmsBetreuung): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.105-2020-07-25T142552.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']) &lt;= 1">(epims_entry_EmsBetreuung): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.105-2020-07-25T142552.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.31-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &gt;= 1">(epims_entry_EmsBetreuung): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.31-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.105-2020-07-25T142552.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.31-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &lt;= 1">(epims_entry_EmsBetreuung): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.31-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.105-2020-07-25T142552.html"
              test="count(hl7:effectiveTime) &lt;= 1">(epims_entry_EmsBetreuung): Element hl7:effectiveTime kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.105
Context: *[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']
Item: (epims_entry_EmsBetreuung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']"
         id="d42e15819-false-d145232e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.105-2020-07-25T142552.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(epims_entry_EmsBetreuung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.105-2020-07-25T142552.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.105')">(epims_entry_EmsBetreuung): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.105' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.105
Context: *[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.31-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]
Item: (epims_entry_EmsBetreuung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.31-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]"
         id="d42e15824-false-d145249e0">
      <extends rule="CD"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.105-2020-07-25T142552.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(epims_entry_EmsBetreuung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.105-2020-07-25T142552.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.6.0.10.31-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(epims_entry_EmsBetreuung): Der Elementinhalt MUSS einer von '1.2.40.0.34.6.0.10.31 EMS Betreuung VS (DYNAMIC)' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.105
Context: *[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]/hl7:effectiveTime
Item: (epims_entry_EmsBetreuung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.105']]/hl7:effectiveTime"
         id="d42e15829-false-d145269e0">
      <extends rule="IVL_TS"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.105-2020-07-25T142552.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVL_TS')">(epims_entry_EmsBetreuung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVL_TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
