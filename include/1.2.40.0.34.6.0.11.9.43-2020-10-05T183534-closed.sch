<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.9.43
Name: Device Product Instance Template
Description: Das "Device Product Instance Template" beschreibt die verpflichtenden Eigenschaften des verwendeten Gerätes. Mindestens eine eindeutige ID, die Klassifizierung und der Name (samt Hersteller) des Gerätes sind für jedes verwendetes Gerät anzugeben. Dieses Template definiert das participantRole-Element des Device Information Organizer. In vielen Fällen ist das Gerät
                ein IEEE 11073 kompatibles Gerät. In diesem Dokument vorhandene medizinische Beobachtungen besitzen ein author-Element, welches in den Daten dieses Templates vorhanden sein muss. Alle weiteren Informationen zum verwendeten Gerät können im darüberliegenden Device Information Organizer kodiert werden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534-closed">
   <title>Device Product Instance Template</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']])]"
         id="d41e62756-true-d3618034e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="not(.)">(atcdabbr_entry_DeviceProductInstanceTemplate)/d41e62756-true-d3618034e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']] (rule-reference: d41e62756-true-d3618034e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17'] | self::hl7:id[not(@nullFlavor)] | self::hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']] | self::hl7:scopingEntitiy)]"
         id="d41e62798-true-d3618092e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="not(.)">(atcdabbr_entry_DeviceProductInstanceTemplate)/d41e62798-true-d3618092e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17'] | hl7:id[not(@nullFlavor)] | hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']] | hl7:scopingEntitiy (rule-reference: d41e62798-true-d3618092e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/*[not(@xsi:nil = 'true')][not(self::hl7:code[@codeSystem = '2.16.840.1.113883.6.24'] | self::hl7:manufacturerModelName[not(@nullFlavor)])]"
         id="d41e62853-true-d3618146e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="not(.)">(atcdabbr_entry_DeviceProductInstanceTemplate)/d41e62853-true-d3618146e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[@codeSystem = '2.16.840.1.113883.6.24'] | hl7:manufacturerModelName[not(@nullFlavor)] (rule-reference: d41e62853-true-d3618146e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:code[@codeSystem = '2.16.840.1.113883.6.24']/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation)]"
         id="d41e62858-true-d3618168e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="not(.)">(atcdabbr_entry_DeviceProductInstanceTemplate)/d41e62858-true-d3618168e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation (rule-reference: d41e62858-true-d3618168e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:code[@codeSystem = '2.16.840.1.113883.6.24']/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3618172e41-true-d3618184e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d3618172e41-true-d3618184e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3618172e41-true-d3618184e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:scopingEntitiy/*[not(@xsi:nil = 'true')][not(self::hl7:desc)]"
         id="d41e62888-true-d3618213e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="not(.)">(atcdabbr_entry_DeviceProductInstanceTemplate)/d41e62888-true-d3618213e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:desc (rule-reference: d41e62888-true-d3618213e0)</assert>
   </rule>
</pattern>
