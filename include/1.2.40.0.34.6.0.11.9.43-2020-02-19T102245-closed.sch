<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.9.43
Name: Device Product Instance Template
Description: Das "Device Product Instance Template" beschreibt die verpflichtenden Eigenschaften des verwendeten Gerätes. Mindestens eine eindeutige ID, die Klassifizierung und der Name (samt Hersteller) des Gerätes sind für jedes verwendetes Gerät anzugeben. Dieses Template definiert das participantRole-Element des Device Information Organizer. In vielen Fällen ist das Gerät
                ein IEEE 11073 kompatibles Gerät. In diesem Dokument vorhandene medizinische Beobachtungen besitzen ein author-Element, welches in den Daten dieses Templates vorhanden sein muss. Alle weiteren Informationen zum verwendeten Gerät können im darüberliegenden Device Information Organizer kodiert werden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245-closed">
   <title>Device Product Instance Template</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']])]"
         id="d41e62902-true-d3618508e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="not(.)">(atcdabbr_entry_DeviceProductInstanceTemplate)/d41e62902-true-d3618508e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']] (rule-reference: d41e62902-true-d3618508e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17'] | self::hl7:id[not(@nullFlavor)] | self::hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']] | self::hl7:scopingEntitiy)]"
         id="d41e62942-true-d3618558e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="not(.)">(atcdabbr_entry_DeviceProductInstanceTemplate)/d41e62942-true-d3618558e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17'] | hl7:id[not(@nullFlavor)] | hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']] | hl7:scopingEntitiy (rule-reference: d41e62942-true-d3618558e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/*[not(@xsi:nil = 'true')][not(self::hl7:code[@codeSystem = '2.16.840.1.113883.6.24'] | self::hl7:manufacturerModelName[not(@nullFlavor)])]"
         id="d41e62997-true-d3618603e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="not(.)">(atcdabbr_entry_DeviceProductInstanceTemplate)/d41e62997-true-d3618603e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[@codeSystem = '2.16.840.1.113883.6.24'] | hl7:manufacturerModelName[not(@nullFlavor)] (rule-reference: d41e62997-true-d3618603e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:code[@codeSystem = '2.16.840.1.113883.6.24']/*[not(@xsi:nil = 'true')][not(self::hl7:translation)]"
         id="d41e63002-true-d3618618e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="not(.)">(atcdabbr_entry_DeviceProductInstanceTemplate)/d41e63002-true-d3618618e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:translation (rule-reference: d41e63002-true-d3618618e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:scopingEntitiy/*[not(@xsi:nil = 'true')][not(self::hl7:desc)]"
         id="d41e63030-true-d3618642e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="not(.)">(atcdabbr_entry_DeviceProductInstanceTemplate)/d41e63030-true-d3618642e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:desc (rule-reference: d41e63030-true-d3618642e0)</assert>
   </rule>
</pattern>
