<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.8.2.2.1
Name: Abgabe Section
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000">
   <title>Abgabe Section</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.2.2.1
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]
Item: (AbgabeSection)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.2.2.1
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]
Item: (AbgabeSection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]"
         id="d42e16259-false-d147832e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="string(@classCode) = ('DOCSECT') or not(@classCode)">(AbgabeSection): Der Wert von classCode MUSS 'DOCSECT' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1']) &gt;= 1">(AbgabeSection): Element hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1']) &lt;= 1">(AbgabeSection): Element hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3']) &gt;= 1">(AbgabeSection): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3']) &lt;= 1">(AbgabeSection): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']) &gt;= 1">(AbgabeSection): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']) &lt;= 1">(AbgabeSection): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="count(hl7:id) &lt;= 1">(AbgabeSection): Element hl7:id kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="count(hl7:code[(@code = '60590-7' and @codeSystem = '2.16.840.1.113883.6.1')]) &gt;= 1">(AbgabeSection): Element hl7:code[(@code = '60590-7' and @codeSystem = '2.16.840.1.113883.6.1')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="count(hl7:code[(@code = '60590-7' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt;= 1">(AbgabeSection): Element hl7:code[(@code = '60590-7' and @codeSystem = '2.16.840.1.113883.6.1')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(AbgabeSection): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(AbgabeSection): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(AbgabeSection): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(AbgabeSection): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="count(hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]) &gt;= 1">(AbgabeSection): Element hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]] ist required [min 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.2.2.1
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]/hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1']
Item: (AbgabeSection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]/hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1']"
         id="d42e16266-false-d147977e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(AbgabeSection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="string(@root) = ('1.2.40.0.34.11.8.2.2.1')">(AbgabeSection): Der Wert von root MUSS '1.2.40.0.34.11.8.2.2.1' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.2.2.1
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3']
Item: (AbgabeSection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3']"
         id="d42e16271-false-d147992e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(AbgabeSection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="string(@root) = ('1.3.6.1.4.1.19376.1.9.1.2.3')">(AbgabeSection): Der Wert von root MUSS '1.3.6.1.4.1.19376.1.9.1.2.3' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.2.2.1
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']
Item: (AbgabeSection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']"
         id="d42e16279-false-d148007e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(AbgabeSection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.1.8')">(AbgabeSection): Der Wert von root MUSS '2.16.840.1.113883.10.20.1.8' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.2.2.1
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]/hl7:id
Item: (AbgabeSection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]/hl7:id"
         id="d42e16288-false-d148021e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(AbgabeSection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.2.2.1
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]/hl7:code[(@code = '60590-7' and @codeSystem = '2.16.840.1.113883.6.1')]
Item: (AbgabeSection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]/hl7:code[(@code = '60590-7' and @codeSystem = '2.16.840.1.113883.6.1')]"
         id="d42e16298-false-d148032e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(AbgabeSection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="@nullFlavor or (@code='60590-7' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='Medication Dispensed.Brief' and @codeSystemName='LOINC')">(AbgabeSection): Der Elementinhalt MUSS einer von 'code '60590-7' codeSystem '2.16.840.1.113883.6.1' displayName='Medication Dispensed.Brief' codeSystemName='LOINC'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.2.2.1
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]/hl7:title[not(@nullFlavor)]
Item: (AbgabeSection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]/hl7:title[not(@nullFlavor)]"
         id="d42e16303-false-d148048e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(AbgabeSection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="text()='Abgabe'">(AbgabeSection): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Abgabe'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.2.2.1
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]/hl7:text[not(@nullFlavor)]
Item: (AbgabeSection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]/hl7:text[not(@nullFlavor)]"
         id="d42e16312-false-d148062e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(AbgabeSection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.2.2.1
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]
Item: (AbgabeSection)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.8.2.2.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.2.3'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.8']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.8.2.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.34'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.3'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.4']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.8.2.2.1-2014-01-31T000000.html"
              test="string(@typeCode) = ('DRIV') or not(@typeCode)">(AbgabeSection): Der Wert von typeCode MUSS 'DRIV' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
   </rule>
</pattern>
