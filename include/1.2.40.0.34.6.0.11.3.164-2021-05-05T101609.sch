<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.164
Name: Anamnese Observation - Labor und Mikrobiologie
Description: Codierung eines Aspekts der Krankengeschichte und ob dieser für einen Patienten zutrifft oder nicht.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609">
   <title>Anamnese Observation - Labor und Mikrobiologie</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.164
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]
Item: (atlab_entry_AnamneseObservationLaborUndMikrobiologie)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.164
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]
Item: (atlab_entry_AnamneseObservationLaborUndMikrobiologie)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]"
         id="d42e21638-false-d1381580e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="string(@classCode) = ('OBS')">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Der Wert von classCode MUSS 'OBS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="string(@moodCode) = ('EVN')">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']) &gt;= 1">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']) &lt;= 1">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="count(hl7:id) &lt;= 1">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Element hl7:id kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.66-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &gt;= 1">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.66-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.66-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &lt;= 1">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.66-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="count(hl7:text) &lt;= 1">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Element hl7:text kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="count(hl7:statusCode[@code = 'completed']) &gt;= 1">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Element hl7:statusCode[@code = 'completed'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="count(hl7:statusCode[@code = 'completed']) &lt;= 1">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Element hl7:statusCode[@code = 'completed'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="count(hl7:effectiveTime) &lt;= 1">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Element hl7:effectiveTime kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="count(hl7:value[@xsi:type='BL'][not(@nullFlavor)]) &gt;= 1">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Element hl7:value[@xsi:type='BL'][not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="count(hl7:value[@xsi:type='BL'][not(@nullFlavor)]) &lt;= 1">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Element hl7:value[@xsi:type='BL'][not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.164
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']
Item: (atlab_entry_AnamneseObservationLaborUndMikrobiologie)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']"
         id="d42e21644-false-d1381656e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.164')">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.164' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.164
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]/hl7:id
Item: (atlab_entry_AnamneseObservationLaborUndMikrobiologie)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]/hl7:id"
         id="d42e21653-false-d1381670e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.164
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.66-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]
Item: (atlab_entry_AnamneseObservationLaborUndMikrobiologie)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.66-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]"
         id="d42e21660-false-d1381683e0">
      <extends rule="CD"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.6.0.10.66-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Der Elementinhalt MUSS einer von '1.2.40.0.34.6.0.10.66 ELGA_AnamneseLaborMikrobiologie_VS (DYNAMIC)' sein.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="@code">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="@codeSystem">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Attribute @codeSystem MUSS vom Datentyp 'oid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="@displayName">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Attribut @displayName MUSS vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]/hl7:text
Item: (atcdabrr_other_NarrativeTextReference)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]/hl7:text"
         id="d1381684e92-false-d1381722e0">
      <extends rule="ED"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(atcdabrr_other_NarrativeTextReference): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(atcdabrr_other_NarrativeTextReference): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(atcdabrr_other_NarrativeTextReference): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]/hl7:text/hl7:reference[not(@nullFlavor)]
Item: (atcdabrr_other_NarrativeTextReference)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]/hl7:text/hl7:reference[not(@nullFlavor)]"
         id="d1381684e94-false-d1381741e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabrr_other_NarrativeTextReference): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="@value">(atcdabrr_other_NarrativeTextReference): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="starts-with(@value,'#') or starts-with(@value,'http')">(atcdabrr_other_NarrativeTextReference): The @value attribute content MUST conform to the format '#xxx', where xxx is the ID of the corresponding 'content'-element, or begin with the 'http' or 'https' url-scheme.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.164
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]/hl7:statusCode[@code = 'completed']
Item: (atlab_entry_AnamneseObservationLaborUndMikrobiologie)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]/hl7:statusCode[@code = 'completed']"
         id="d42e21685-false-d1381756e0">
      <extends rule="CS"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CS')">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="@nullFlavor or (@code='completed')">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Der Elementinhalt MUSS einer von 'code 'completed'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.164
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]/hl7:effectiveTime
Item: (atlab_entry_AnamneseObservationLaborUndMikrobiologie)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]/hl7:effectiveTime"
         id="d42e21690-false-d1381772e0">
      <extends rule="IVL_TS"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVL_TS')">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVL_TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.164
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]/hl7:value[@xsi:type='BL'][not(@nullFlavor)]
Item: (atlab_entry_AnamneseObservationLaborUndMikrobiologie)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.164']]/hl7:value[@xsi:type='BL'][not(@nullFlavor)]"
         id="d42e21696-false-d1381780e0">
      <extends rule="BL"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'BL')">(atlab_entry_AnamneseObservationLaborUndMikrobiologie): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:BL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
