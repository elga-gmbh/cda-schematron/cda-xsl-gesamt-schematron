<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.102
Name: Serienmessungs-Gruppe Entry
Description: Das Serienmessungs-Gruppe Entry verknüpft die Perioden-Information mit den Messwerten. Dieses Entry beinhaltet mindestens zwei entryRelationship/observation Elemente, wobei eines die Perioden-Information und das andere die Messwerte dokumentiert.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910-closed">
   <title>Serienmessungs-Gruppe Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']])]"
         id="d42e55768-true-d5173621e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910.html"
              test="not(.)">(atcdabbr_entry_SerienmessungsGruppeEntry)/d42e55768-true-d5173621e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']] (rule-reference: d42e55768-true-d5173621e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17'] | self::hl7:code[@nullFlavor='NA'] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]])]"
         id="d42e55808-true-d5173678e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910.html"
              test="not(.)">(atcdabbr_entry_SerienmessungsGruppeEntry)/d42e55808-true-d5173678e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17'] | hl7:code[@nullFlavor='NA'] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]] (rule-reference: d42e55808-true-d5173678e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']])]"
         id="d42e55838-true-d5173716e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910.html"
              test="not(.)">(atcdabbr_entry_SerienmessungsGruppeEntry)/d42e55838-true-d5173716e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']] (rule-reference: d42e55838-true-d5173716e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17'] | self::hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:value)]"
         id="d5173698e7-true-d5173765e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910.html"
              test="not(.)">(atcdabbr_entry_SerienmessungsGruppeEntry)/d5173698e7-true-d5173765e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17'] | hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:value (rule-reference: d5173698e7-true-d5173765e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText[not(@nullFlavor)] | self::hl7:translation[@codeSystem = '2.16.840.1.113883.6.24'] | self::hl7:translation | self::ips:designation)]"
         id="d5173698e31-true-d5173809e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910.html"
              test="not(.)">(atcdabbr_entry_SerienmessungsGruppeEntry)/d5173698e31-true-d5173809e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText[not(@nullFlavor)] | hl7:translation[@codeSystem = '2.16.840.1.113883.6.24'] | hl7:translation | ips:designation (rule-reference: d5173698e31-true-d5173809e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:originalText[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5173698e48-true-d5173823e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910.html"
              test="not(.)">(atcdabbr_entry_SerienmessungsGruppeEntry)/d5173698e48-true-d5173823e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5173698e48-true-d5173823e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5173698e102-true-d5173858e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910.html"
              test="not(.)">(atcdabbr_entry_SerienmessungsGruppeEntry)/d5173698e102-true-d5173858e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5173698e102-true-d5173858e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:value/*[not(@xsi:nil = 'true')][not(self::hl7:origin[not(@nullFlavor)] | self::hl7:scale[not(@nullFlavor)] | self::hl7:digits[not(@nullFlavor)])]"
         id="d5173698e119-true-d5173893e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910.html"
              test="not(.)">(atcdabbr_entry_SerienmessungsGruppeEntry)/d5173698e119-true-d5173893e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:origin[not(@nullFlavor)] | hl7:scale[not(@nullFlavor)] | hl7:digits[not(@nullFlavor)] (rule-reference: d5173698e119-true-d5173893e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']])]"
         id="d42e55848-true-d5173931e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910.html"
              test="not(.)">(atcdabbr_entry_SerienmessungsGruppeEntry)/d42e55848-true-d5173931e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']] (rule-reference: d42e55848-true-d5173931e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17'] | self::hl7:code[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:value[not(@nullFlavor)])]"
         id="d5173913e7-true-d5173971e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910.html"
              test="not(.)">(atcdabbr_entry_SerienmessungsGruppeEntry)/d5173913e7-true-d5173971e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17'] | hl7:code[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:value[not(@nullFlavor)] (rule-reference: d5173913e7-true-d5173971e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5173913e41-true-d5174002e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910.html"
              test="not(.)">(atcdabbr_entry_SerienmessungsGruppeEntry)/d5173913e41-true-d5174002e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5173913e41-true-d5174002e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]/hl7:value[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:head[not(@nullFlavor)] | self::hl7:increment[not(@nullFlavor)])]"
         id="d5173913e53-true-d5174026e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910.html"
              test="not(.)">(atcdabbr_entry_SerienmessungsGruppeEntry)/d5173913e53-true-d5174026e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:head[not(@nullFlavor)] | hl7:increment[not(@nullFlavor)] (rule-reference: d5173913e53-true-d5174026e0)</assert>
   </rule>
</pattern>
