<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.2.2.15
Name: Ausstehende Befunde
Description: Beinhaltet die Hinweise auf noch ausstehende Befunde in narrativer Form als Information für den Dokumentempfänger. In vorläufigen Entlassungsdokumenten sind oftmals noch nicht alle Befunde ausformuliert. Diese Sektion dient dazu, die noch ausstehenden Befunde als Information für den Dokumentempfänger bekanntzugeben. Dabei soll ein kurzer Vermerk für jeden
                ausstehenden Befund angegeben werden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.2.2.15-2011-12-19T000000">
   <title>Ausstehende Befunde</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.15
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]]
Item: (AusstehendeBefunde)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.15
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]
Item: (AusstehendeBefunde)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]"
         id="d42e4311-false-d54934e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.15-2011-12-19T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']) &gt;= 1">(AusstehendeBefunde): Element hl7:templateId[@root = '1.2.40.0.34.11.2.2.15'] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.15-2011-12-19T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']) &lt;= 1">(AusstehendeBefunde): Element hl7:templateId[@root = '1.2.40.0.34.11.2.2.15'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.15-2011-12-19T000000.html"
              test="count(hl7:code[(@code = 'BEFAUS' and @codeSystem = '1.2.40.0.34.5.40')]) &gt;= 1">(AusstehendeBefunde): Element hl7:code[(@code = 'BEFAUS' and @codeSystem = '1.2.40.0.34.5.40')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.15-2011-12-19T000000.html"
              test="count(hl7:code[(@code = 'BEFAUS' and @codeSystem = '1.2.40.0.34.5.40')]) &lt;= 1">(AusstehendeBefunde): Element hl7:code[(@code = 'BEFAUS' and @codeSystem = '1.2.40.0.34.5.40')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.15-2011-12-19T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(AusstehendeBefunde): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.15-2011-12-19T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(AusstehendeBefunde): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.15-2011-12-19T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(AusstehendeBefunde): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.15-2011-12-19T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(AusstehendeBefunde): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.15
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]/hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']
Item: (AusstehendeBefunde)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]/hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']"
         id="d42e4313-false-d54977e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.15-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(AusstehendeBefunde): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.15-2011-12-19T000000.html"
              test="string(@root) = ('1.2.40.0.34.11.2.2.15')">(AusstehendeBefunde): Der Wert von root MUSS '1.2.40.0.34.11.2.2.15' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.15
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]/hl7:code[(@code = 'BEFAUS' and @codeSystem = '1.2.40.0.34.5.40')]
Item: (AusstehendeBefunde)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]/hl7:code[(@code = 'BEFAUS' and @codeSystem = '1.2.40.0.34.5.40')]"
         id="d42e4318-false-d54992e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.15-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(AusstehendeBefunde): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.15-2011-12-19T000000.html"
              test="@nullFlavor or (@code='BEFAUS' and @codeSystem='1.2.40.0.34.5.40')">(AusstehendeBefunde): Der Elementinhalt MUSS einer von 'code 'BEFAUS' codeSystem '1.2.40.0.34.5.40'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.15
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]/hl7:title[not(@nullFlavor)]
Item: (AusstehendeBefunde)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]/hl7:title[not(@nullFlavor)]"
         id="d42e4326-false-d55008e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.15-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(AusstehendeBefunde): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.15-2011-12-19T000000.html"
              test="text()='Ausstehende Befunde'">(AusstehendeBefunde): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Ausstehende Befunde'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.15
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]/hl7:text[not(@nullFlavor)]
Item: (AusstehendeBefunde)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.15']]/hl7:text[not(@nullFlavor)]"
         id="d42e4332-false-d55022e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.15-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(AusstehendeBefunde): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
