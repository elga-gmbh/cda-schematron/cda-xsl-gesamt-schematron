<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.60
Name: EMS Hospitalisierung
Description: Im Zuge der Arztmeldung ist es möglich festzuhalten ob ein Patient/eine Patientin in ein Kran-kenhaus eingewiesen wurde. Diese Information wird in einem act Element codiert. Ist die Ad-resse der Krankenanstalt bekannt, kann diese noch beim patientRole-Element im CDA-Header als temporäre
                        Aufenthaltsadresse geführt werden. Hierbei ist beim addr-Element das @use Attribute mit dem Wert „TMP“ zu verwenden
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.60-2020-02-20T160212-closed">
   <title>EMS Hospitalisierung</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.60'] and hl7:templateId[@root = '1.2.40.0.34.11.6.3.6']]]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.60'] and hl7:templateId[@root = '1.2.40.0.34.11.6.3.6']])]"
         id="d42e21693-true-d235661e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.60-2020-02-20T160212.html"
              test="not(.)">(epims_entry_hospitalization)/d42e21693-true-d235661e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.60'] and hl7:templateId[@root = '1.2.40.0.34.11.6.3.6']] (rule-reference: d42e21693-true-d235661e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.60'] and hl7:templateId[@root = '1.2.40.0.34.11.6.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.60'] and hl7:templateId[@root = '1.2.40.0.34.11.6.3.6']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.60'] | self::hl7:templateId[@root = '1.2.40.0.34.11.6.3.6'] | self::hl7:code[(@code = '32485007' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:effectiveTime)]"
         id="d42e21713-true-d235698e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.60-2020-02-20T160212.html"
              test="not(.)">(epims_entry_hospitalization)/d42e21713-true-d235698e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.60'] | hl7:templateId[@root = '1.2.40.0.34.11.6.3.6'] | hl7:code[(@code = '32485007' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:effectiveTime (rule-reference: d42e21713-true-d235698e0)</assert>
   </rule>
</pattern>
