<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.3
Name: Immunization Recommendation Entry
Description: 
                 Enthält die Dokumentation einer einzelnen Impfempfehlung (die geplante einzelne Gabe eines Impfstoffes). 
                 Impfempfehlungen können von einem Arzt stammen oder automatisiert von einem Regelwerk erstellt werden. 
                 Impfempfehlungen können sich auf ein bestimmtes Produkt und sollten sich auf ein bestimmtes Impfschema beziehen. 
                 Impfempfehlungen können auch auf eine Kontraindikation für eine Impfung dokumentieren (Empfehlung für eine Nicht-Impfung). 
            
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404-closed">
   <title>Immunization Recommendation Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/*[not(@xsi:nil = 'true')][not(self::hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']])]"
         id="d42e42782-true-d1393150e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d42e42782-true-d1393150e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']] (rule-reference: d42e42782-true-d1393150e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3'] | self::hl7:templateId[@root='2.16.840.1.113883.10.20.1.25'][not(@nullFlavor)] | self::hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.12.2'][not(@nullFlavor)] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.3-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or (@code = 'IMMUNIZ' and @codeSystem = '2.16.840.1.113883.5.4')] | self::hl7:text | self::hl7:statusCode[@code = 'active'] | self::hl7:effectiveTime | self::hl7:routeCode[@nullFlavor = 'NA'] | self::hl7:approachSiteCode[@nullFlavor = 'NA'] | self::hl7:doseQuantity[not(@nullFlavor|hl7:low|hl7:high|hl7:center|hl7:width)] | self::hl7:doseQuantity[not(hl7:low|hl7:high)] | self::hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.32']] | self::hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.31']] | self::hl7:author[not(@nullFlavor)][hl7:assignedAuthor] | self::hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']] | self::hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']] | self::hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.2']]] | self::hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]] | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]] | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] | self::hl7:precondition[hl7:criterion[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.10']]])]"
         id="d42e43120-true-d1393893e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d42e43120-true-d1393893e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3'] | hl7:templateId[@root='2.16.840.1.113883.10.20.1.25'][not(@nullFlavor)] | hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.12.2'][not(@nullFlavor)] | hl7:id[not(@nullFlavor)] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.3-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or (@code = 'IMMUNIZ' and @codeSystem = '2.16.840.1.113883.5.4')] | hl7:text | hl7:statusCode[@code = 'active'] | hl7:effectiveTime | hl7:routeCode[@nullFlavor = 'NA'] | hl7:approachSiteCode[@nullFlavor = 'NA'] | hl7:doseQuantity[not(@nullFlavor|hl7:low|hl7:high|hl7:center|hl7:width)] | hl7:doseQuantity[not(hl7:low|hl7:high)] | hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.32']] | hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.31']] | hl7:author[not(@nullFlavor)][hl7:assignedAuthor] | hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']] | hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']] | hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.2']]] | hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]] | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]] | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] | hl7:precondition[hl7:criterion[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.10']]] (rule-reference: d42e43120-true-d1393893e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1393922e54-true-d1393934e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1393922e54-true-d1393934e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1393922e54-true-d1393934e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low[@value] | self::hl7:low[@nullFlavor='UNK'] | self::hl7:high[@value] | self::hl7:high[@nullFlavor='UNK'])]"
         id="d42e43254-true-d1393957e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d42e43254-true-d1393957e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low[@value] | hl7:low[@nullFlavor='UNK'] | hl7:high[@value] | hl7:high[@nullFlavor='UNK'] (rule-reference: d42e43254-true-d1393957e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.32']]/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturedProduct[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.32'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']])]"
         id="d42e43320-true-d1394044e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d42e43320-true-d1394044e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturedProduct[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.32'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']] (rule-reference: d42e43320-true-d1394044e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.32']]/hl7:manufacturedProduct[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.32'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.32'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53'] | self::hl7:id[not(@nullFlavor)] | self::hl7:manufacturedMaterial[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']] | self::hl7:manufacturerOrganization)]"
         id="d1393990e3-true-d1394147e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1393990e3-true-d1394147e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.32'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53'] | hl7:id[not(@nullFlavor)] | hl7:manufacturedMaterial[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']] | hl7:manufacturerOrganization (rule-reference: d1393990e3-true-d1394147e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.32']]/hl7:manufacturedProduct[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.32'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1'] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.14-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.10-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:name | self::hl7:lotNumberText[not(@nullFlavor)] | self::hl7:lotNumberText[@nullFlavor='NA'] | self::hl7:lotNumberText[@nullFlavor='UNK'] | self::pharm:ingredient[pharm:ingredient])]"
         id="d1393990e48-true-d1394218e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1393990e48-true-d1394218e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1'] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.14-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.10-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:name | hl7:lotNumberText[not(@nullFlavor)] | hl7:lotNumberText[@nullFlavor='NA'] | hl7:lotNumberText[@nullFlavor='UNK'] | pharm:ingredient[pharm:ingredient] (rule-reference: d1393990e48-true-d1394218e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.32']]/hl7:manufacturedProduct[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.32'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.14-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.10-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation)]"
         id="d1393990e62-true-d1394250e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1393990e62-true-d1394250e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation (rule-reference: d1393990e62-true-d1394250e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.32']]/hl7:manufacturedProduct[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.32'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.14-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.10-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1394254e41-true-d1394266e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d1394254e41-true-d1394266e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1394254e41-true-d1394266e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.32']]/hl7:manufacturedProduct[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.32'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/pharm:ingredient[pharm:ingredient]/*[not(@xsi:nil = 'true')][not(self::pharm:ingredient[not(@nullFlavor)])]"
         id="d1393990e225-true-d1394304e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1393990e225-true-d1394304e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  pharm:ingredient[not(@nullFlavor)] (rule-reference: d1393990e225-true-d1394304e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.32']]/hl7:manufacturedProduct[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.32'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/pharm:ingredient[pharm:ingredient]/pharm:ingredient[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::pharm:code | self::pharm:name)]"
         id="d1393990e232-true-d1394323e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1393990e232-true-d1394323e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  pharm:code | pharm:name (rule-reference: d1393990e232-true-d1394323e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.32']]/hl7:manufacturedProduct[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.32'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturedMaterial[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.1']]/pharm:ingredient[pharm:ingredient]/pharm:ingredient[not(@nullFlavor)]/pharm:code/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation)]"
         id="d1393990e241-true-d1394342e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1393990e241-true-d1394342e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation (rule-reference: d1393990e241-true-d1394342e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.32']]/hl7:manufacturedProduct[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.32'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturerOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1393990e282-true-d1394388e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1393990e282-true-d1394388e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1393990e282-true-d1394388e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.32']]/hl7:manufacturedProduct[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.32'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53']]/hl7:manufacturerOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1394362e63-true-d1394449e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1394362e63-true-d1394449e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1394362e63-true-d1394449e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.31']]/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturedProduct[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2']])]"
         id="d42e43325-true-d1394510e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d42e43325-true-d1394510e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturedProduct[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2']] (rule-reference: d42e43325-true-d1394510e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.31']]/hl7:manufacturedProduct[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.31'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] | self::hl7:manufacturedMaterial[not(@nullFlavor)][hl7:code[@nullFlavor = 'NA']])]"
         id="d1394494e3-true-d1394548e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1394494e3-true-d1394548e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.31'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2'] | hl7:manufacturedMaterial[not(@nullFlavor)][hl7:code[@nullFlavor = 'NA']] (rule-reference: d1394494e3-true-d1394548e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:consumable[hl7:manufacturedProduct/hl7:templateId[@root='1.2.40.0.34.6.0.11.9.31']]/hl7:manufacturedProduct[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.53'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7.2']]/hl7:manufacturedMaterial[not(@nullFlavor)][hl7:code[@nullFlavor = 'NA']]/*[not(@xsi:nil = 'true')][not(self::hl7:code[@nullFlavor = 'NA'])]"
         id="d1394494e31-true-d1394580e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1394494e31-true-d1394580e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[@nullFlavor = 'NA'] (rule-reference: d1394494e31-true-d1394580e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:author[not(@nullFlavor)][hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e43332-true-d1394707e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d42e43332-true-d1394707e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e43332-true-d1394707e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:author[not(@nullFlavor)][hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1394590e41-true-d1394810e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1394590e41-true-d1394810e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1394590e41-true-d1394810e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:author[not(@nullFlavor)][hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1394590e91-true-d1394869e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1394590e91-true-d1394869e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1394590e91-true-d1394869e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:author[not(@nullFlavor)][hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1394590e137-true-d1394926e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1394590e137-true-d1394926e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1394590e137-true-d1394926e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:author[not(@nullFlavor)][hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1394930e92-true-d1394960e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1394930e92-true-d1394960e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1394930e92-true-d1394960e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:author[not(@nullFlavor)][hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1394590e160-true-d1395008e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1394590e160-true-d1395008e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1394590e160-true-d1395008e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:author[not(@nullFlavor)][hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1394590e176-true-d1395053e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1394590e176-true-d1395053e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1394590e176-true-d1395053e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:author[not(@nullFlavor)][hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1395023e94-true-d1395116e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1395023e94-true-d1395116e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1395023e94-true-d1395116e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44'] | self::hl7:time[not(@nullFlavor)] | self::hl7:participantRole[not(@nullFlavor)])]"
         id="d42e43338-true-d1395231e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d42e43338-true-d1395231e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44'] | hl7:time[not(@nullFlavor)] | hl7:participantRole[not(@nullFlavor)] (rule-reference: d42e43338-true-d1395231e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']]/hl7:participantRole[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:playingDevice | self::hl7:playingEntity | self::hl7:scopingEntity)]"
         id="d1395161e86-true-d1395281e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1395161e86-true-d1395281e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:playingDevice | hl7:playingEntity | hl7:scopingEntity (rule-reference: d1395161e86-true-d1395281e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']]/hl7:participantRole[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1395161e115-true-d1395340e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1395161e115-true-d1395340e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1395161e115-true-d1395340e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']]/hl7:participantRole[not(@nullFlavor)]/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1395161e145-true-d1395405e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1395161e145-true-d1395405e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1395161e145-true-d1395405e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']]/hl7:participantRole[not(@nullFlavor)]/hl7:playingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1395161e147-true-d1395429e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1395161e147-true-d1395429e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1395161e147-true-d1395429e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']]/hl7:participantRole[not(@nullFlavor)]/hl7:playingEntity/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1395161e153-true-d1395458e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1395161e153-true-d1395458e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1395161e153-true-d1395458e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']]/hl7:participantRole[not(@nullFlavor)]/hl7:scopingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:desc)]"
         id="d1395161e252-true-d1395492e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1395161e252-true-d1395492e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:desc (rule-reference: d1395161e252-true-d1395492e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46'] | self::hl7:participantRole[not(@nullFlavor)])]"
         id="d42e43413-true-d1395522e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d42e43413-true-d1395522e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46'] | hl7:participantRole[not(@nullFlavor)] (rule-reference: d42e43413-true-d1395522e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']]/hl7:participantRole[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)][not(@nullFlavor)])]"
         id="d1395502e45-true-d1395538e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1395502e45-true-d1395538e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)][not(@nullFlavor)] (rule-reference: d1395502e45-true-d1395538e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47'] | self::hl7:time | self::hl7:participantRole[not(@nullFlavor)])]"
         id="d42e43454-true-d1395616e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d42e43454-true-d1395616e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47'] | hl7:time | hl7:participantRole[not(@nullFlavor)] (rule-reference: d42e43454-true-d1395616e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']]/hl7:participantRole[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:playingDevice | self::hl7:playingEntity | self::hl7:scopingEntity)]"
         id="d1395546e52-true-d1395666e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1395546e52-true-d1395666e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:playingDevice | hl7:playingEntity | hl7:scopingEntity (rule-reference: d1395546e52-true-d1395666e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']]/hl7:participantRole[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1395546e81-true-d1395725e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1395546e81-true-d1395725e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1395546e81-true-d1395725e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']]/hl7:participantRole[not(@nullFlavor)]/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1395546e111-true-d1395790e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1395546e111-true-d1395790e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1395546e111-true-d1395790e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']]/hl7:participantRole[not(@nullFlavor)]/hl7:playingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1395546e113-true-d1395814e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1395546e113-true-d1395814e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1395546e113-true-d1395814e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']]/hl7:participantRole[not(@nullFlavor)]/hl7:playingEntity/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1395546e119-true-d1395843e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1395546e119-true-d1395843e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1395546e119-true-d1395843e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']]/hl7:participantRole[not(@nullFlavor)]/hl7:scopingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:desc)]"
         id="d1395546e224-true-d1395877e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1395546e224-true-d1395877e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:desc (rule-reference: d1395546e224-true-d1395877e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.2']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.2']])]"
         id="d42e43495-true-d1395910e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d42e43495-true-d1395910e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.2']] (rule-reference: d42e43495-true-d1395910e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.2'] | self::hl7:id | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text | self::hl7:statusCode[@code = 'completed'])]"
         id="d1395887e12-true-d1395956e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1395887e12-true-d1395956e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.2'] | hl7:id | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.4-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text | hl7:statusCode[@code = 'completed'] (rule-reference: d1395887e12-true-d1395956e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.2']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1395979e54-true-d1395991e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1395979e54-true-d1395991e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1395979e54-true-d1395991e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']])]"
         id="d42e43510-true-d1396478e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d42e43510-true-d1396478e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']] (rule-reference: d42e43510-true-d1396478e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[(@code = '48767-8' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:participant[@typeCode][hl7:participantRole])]"
         id="d1396007e16-true-d1396998e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1396007e16-true-d1396998e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2'] | hl7:id[not(@nullFlavor)] | hl7:code[(@code = '48767-8' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:participant[@typeCode][hl7:participantRole] (rule-reference: d1396007e16-true-d1396998e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1397029e54-true-d1397041e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1397029e54-true-d1397041e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1397029e54-true-d1397041e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | self::hl7:time | self::hl7:assignedEntity[not(@nullFlavor)])]"
         id="d1396007e86-true-d1397196e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1396007e86-true-d1397196e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | hl7:time | hl7:assignedEntity[not(@nullFlavor)] (rule-reference: d1396007e86-true-d1397196e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1397057e14-true-d1397302e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1397057e14-true-d1397302e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1397057e14-true-d1397302e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1397306e101-true-d1397375e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1397306e101-true-d1397375e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1397306e101-true-d1397375e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1397306e174-true-d1397451e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1397306e174-true-d1397451e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1397306e174-true-d1397451e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1397306e186-true-d1397473e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1397306e186-true-d1397473e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1397306e186-true-d1397473e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1397461e12-true-d1397502e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1397461e12-true-d1397502e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1397461e12-true-d1397502e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1397306e198-true-d1397553e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1397306e198-true-d1397553e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1397306e198-true-d1397553e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1397527e58-true-d1397614e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1397527e58-true-d1397614e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1397527e58-true-d1397614e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1396007e89-true-d1397781e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1396007e89-true-d1397781e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1396007e89-true-d1397781e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1397659e45-true-d1397889e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1397659e45-true-d1397889e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1397659e45-true-d1397889e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1397659e64-true-d1397948e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1397659e64-true-d1397948e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1397659e64-true-d1397948e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1397659e110-true-d1398005e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1397659e110-true-d1398005e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1397659e110-true-d1398005e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1398009e92-true-d1398039e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1398009e92-true-d1398039e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1398009e92-true-d1398039e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1397659e133-true-d1398087e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1397659e133-true-d1398087e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1397659e133-true-d1398087e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1397659e149-true-d1398132e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1397659e149-true-d1398132e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1397659e149-true-d1398132e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1398102e67-true-d1398195e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1398102e67-true-d1398195e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1398102e67-true-d1398195e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1396007e95-true-d1398363e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1396007e95-true-d1398363e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1396007e95-true-d1398363e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1398240e9-true-d1398495e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1398240e9-true-d1398495e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1398240e9-true-d1398495e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1398368e50-true-d1398561e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1398368e50-true-d1398561e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1398368e50-true-d1398561e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1398368e120-true-d1398623e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1398368e120-true-d1398623e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1398368e120-true-d1398623e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1398368e132-true-d1398645e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1398368e132-true-d1398645e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1398368e132-true-d1398645e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1398633e12-true-d1398674e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1398633e12-true-d1398674e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1398633e12-true-d1398674e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1398368e143-true-d1398725e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1398368e143-true-d1398725e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1398368e143-true-d1398725e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1398699e58-true-d1398786e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1398699e58-true-d1398786e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1398699e58-true-d1398786e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1398240e11-true-d1398863e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1398240e11-true-d1398863e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1398240e11-true-d1398863e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1398240e20-true-d1398916e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1398240e20-true-d1398916e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1398240e20-true-d1398916e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1398240e24-true-d1398971e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1398240e24-true-d1398971e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1398240e24-true-d1398971e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1398964e10-true-d1398998e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1398964e10-true-d1398998e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1398964e10-true-d1398998e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/*[not(@xsi:nil = 'true')][not(self::hl7:time | self::hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:participantRole)]"
         id="d1396007e101-true-d1399126e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1396007e101-true-d1399126e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time | hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:participantRole (rule-reference: d1396007e101-true-d1399126e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:playingDevice | self::hl7:playingEntity | self::hl7:scopingEntity)]"
         id="d1399029e11-true-d1399234e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1399029e11-true-d1399234e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:addr | hl7:telecom | hl7:playingDevice | hl7:playingEntity | hl7:scopingEntity (rule-reference: d1399029e11-true-d1399234e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1399029e22-true-d1399296e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1399029e22-true-d1399296e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1399029e22-true-d1399296e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:manufacturerModelName | self::hl7:softwareName)]"
         id="d1399029e78-true-d1399374e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1399029e78-true-d1399374e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:manufacturerModelName | hl7:softwareName (rule-reference: d1399029e78-true-d1399374e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:quantity | self::hl7:name | self::sdtc:birthTime | self::hl7:desc)]"
         id="d1399029e80-true-d1399435e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1399029e80-true-d1399435e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:quantity | hl7:name | sdtc:birthTime | hl7:desc (rule-reference: d1399029e80-true-d1399435e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:scopingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:desc)]"
         id="d1399029e83-true-d1399493e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1399029e83-true-d1399493e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:desc (rule-reference: d1399029e83-true-d1399493e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']])]"
         id="d42e43532-true-d1399539e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d42e43532-true-d1399539e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']] (rule-reference: d42e43532-true-d1399539e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[not(@nullFlavor)] | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d1399516e20-true-d1399570e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1399516e20-true-d1399570e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22'] | hl7:id[not(@nullFlavor)] | hl7:code[not(@nullFlavor)] | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d1399516e20-true-d1399570e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1399588e54-true-d1399600e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1399588e54-true-d1399600e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1399588e54-true-d1399600e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d42e43554-true-d1399639e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d42e43554-true-d1399639e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d42e43554-true-d1399639e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d1399616e7-true-d1399672e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1399616e7-true-d1399672e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d1399616e7-true-d1399672e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1399692e54-true-d1399704e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1399692e54-true-d1399704e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1399692e54-true-d1399704e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:precondition[hl7:criterion[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.10']]]/*[not(@xsi:nil = 'true')][not(self::hl7:criterion[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.10']])]"
         id="d42e43564-true-d1399749e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d42e43564-true-d1399749e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:criterion[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.10']] (rule-reference: d42e43564-true-d1399749e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:precondition[hl7:criterion[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.10']]]/hl7:criterion[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.10']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.10'] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:code[@nullFlavor='NI'] | self::hl7:text | self::hl7:value[not(@nullFlavor)] | self::hl7:value[@nullFlavor='UNK'] | self::hl7:value[@nullFlavor='NAV'] | self::hl7:value[@nullFlavor='NA'])]"
         id="d1399720e10-true-d1399779e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1399720e10-true-d1399779e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.10'] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:code[@nullFlavor='NI'] | hl7:text | hl7:value[not(@nullFlavor)] | hl7:value[@nullFlavor='UNK'] | hl7:value[@nullFlavor='NAV'] | hl7:value[@nullFlavor='NA'] (rule-reference: d1399720e10-true-d1399779e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:precondition[hl7:criterion[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.10']]]/hl7:criterion[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.10']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1399800e54-true-d1399812e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1399800e54-true-d1399812e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1399800e54-true-d1399812e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:precondition[hl7:criterion[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.10']]]/hl7:criterion[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.10']]/hl7:value[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText)]"
         id="d1399720e68-true-d1399828e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.3-2022-01-25T141404.html"
              test="not(.)">(atcdabbr_entry_ImmunizationRecommendation)/d1399720e68-true-d1399828e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText (rule-reference: d1399720e68-true-d1399828e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]]/hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.3']]/hl7:precondition[hl7:criterion[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.10']]]/hl7:criterion[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.10']]/hl7:value[not(@nullFlavor)]/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1399832e41-true-d1399844e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d1399832e41-true-d1399844e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1399832e41-true-d1399844e0)</assert>
   </rule>
</pattern>
