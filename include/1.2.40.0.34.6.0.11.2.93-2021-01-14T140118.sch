<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.93
Name: Probeninformation (Specimen Section)
Description:  Die Dokumentation des Untersuchungsmaterials kann auf zwei Arten erfolgen:  Enthält ein Befund nur einen Befundbereich ("Laboratory Specialty Section"), so kann die Codierung innerhalb der einen Section erfolgen.  ODER  Bei Verwendung von mehreren Befundbereichen in einem Befund kann es zu Überschneidungen der Untersuchungsmaterialien
                        kommen (ein spezielles Untersuchungsmaterial kann in zwei Befundbereichen analysiert werden). Die CDA Level 3 Codierung eines Untersuchungsmaterials darf jedoch nur einmal im gesamten Befund erfolgen. Daher sind die Informationen zu den Untersuchungsmaterialien in einer eigenen, führenden "Probeninformation (Specimen Section)" zu codieren. WICHTIG: 
                Derzeit werden  Mikrobiologiebefunde  mit mehreren Untersuchungsmaterialien NICHT unterstützt. Das bedeutet, dass in einem Mikrobiologiebefund  nur genau ein Untersuchungsmaterial  dokumentiert werden darf. Falls mehrere Untersuchungsmaterialien zu einem Auftrag eingese
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118">
   <title>Probeninformation (Specimen Section)</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.93
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]
Item: (atlab_section_ProbeninformationSpecimenSection)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.93
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]
Item: (atlab_section_ProbeninformationSpecimenSection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]"
         id="d42e20248-false-d1343472e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="string(@classCode) = ('DOCSECT') or not(@classCode)">(atlab_section_ProbeninformationSpecimenSection): Der Wert von classCode MUSS 'DOCSECT' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="string(@moodCode) = ('EVN') or not(@moodCode)">(atlab_section_ProbeninformationSpecimenSection): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']) &gt;= 1">(atlab_section_ProbeninformationSpecimenSection): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']) &lt;= 1">(atlab_section_ProbeninformationSpecimenSection): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="count(hl7:id) &lt;= 1">(atlab_section_ProbeninformationSpecimenSection): Element hl7:id kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="count(hl7:code[(@code = '10' and @codeSystem = '1.2.40.0.34.5.11')]) &gt;= 1">(atlab_section_ProbeninformationSpecimenSection): Element hl7:code[(@code = '10' and @codeSystem = '1.2.40.0.34.5.11')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="count(hl7:code[(@code = '10' and @codeSystem = '1.2.40.0.34.5.11')]) &lt;= 1">(atlab_section_ProbeninformationSpecimenSection): Element hl7:code[(@code = '10' and @codeSystem = '1.2.40.0.34.5.11')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(atlab_section_ProbeninformationSpecimenSection): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(atlab_section_ProbeninformationSpecimenSection): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(atlab_section_ProbeninformationSpecimenSection): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(atlab_section_ProbeninformationSpecimenSection): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="count(hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.160']]]) &gt;= 1">(atlab_section_ProbeninformationSpecimenSection): Element hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.160']]] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="count(hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.160']]]) &lt;= 1">(atlab_section_ProbeninformationSpecimenSection): Element hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.160']]] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.93
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']
Item: (atlab_section_ProbeninformationSpecimenSection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']"
         id="d42e20257-false-d1343612e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_section_ProbeninformationSpecimenSection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.2.93')">(atlab_section_ProbeninformationSpecimenSection): Der Wert von root MUSS '1.2.40.0.34.6.0.11.2.93' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.93
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]/hl7:id
Item: (atlab_section_ProbeninformationSpecimenSection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]/hl7:id"
         id="d42e20266-false-d1343626e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_section_ProbeninformationSpecimenSection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.93
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]/hl7:code[(@code = '10' and @codeSystem = '1.2.40.0.34.5.11')]
Item: (atlab_section_ProbeninformationSpecimenSection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]/hl7:code[(@code = '10' and @codeSystem = '1.2.40.0.34.5.11')]"
         id="d42e20273-false-d1343637e0">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atlab_section_ProbeninformationSpecimenSection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="@nullFlavor or (@code='10' and @codeSystem='1.2.40.0.34.5.11' and @displayName='Probeninformation')">(atlab_section_ProbeninformationSpecimenSection): Der Elementinhalt MUSS einer von 'code '10' codeSystem '1.2.40.0.34.5.11' displayName='Probeninformation'' sein.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="string(@codeSystemName) = ('ELGA_LaborparameterErgaenzung') or not(@codeSystemName)">(atlab_section_ProbeninformationSpecimenSection): Der Wert von codeSystemName MUSS 'ELGA_LaborparameterErgaenzung' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atlab_section_ProbeninformationSpecimenSection): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.93
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]/hl7:title[not(@nullFlavor)]
Item: (atlab_section_ProbeninformationSpecimenSection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]/hl7:title[not(@nullFlavor)]"
         id="d42e20280-false-d1343659e0">
      <extends rule="ST"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atlab_section_ProbeninformationSpecimenSection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="text()='Probeninformation'">(atlab_section_ProbeninformationSpecimenSection): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Probeninformation'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.93
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]/hl7:text[not(@nullFlavor)]
Item: (atlab_section_ProbeninformationSpecimenSection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]/hl7:text[not(@nullFlavor)]"
         id="d42e20286-false-d1343673e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(atlab_section_ProbeninformationSpecimenSection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.93
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.160']]]
Item: (atlab_section_ProbeninformationSpecimenSection)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.160']]]">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="string(@typeCode) = ('DRIV')">(atlab_section_ProbeninformationSpecimenSection): Der Wert von typeCode MUSS 'DRIV' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atlab_section_ProbeninformationSpecimenSection): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.93
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]
Item: (atlab_section_ProbeninformationSpecimenSection)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.93']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atlab_section_ProbeninformationSpecimenSection): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atlab_section_ProbeninformationSpecimenSection): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>
</pattern>
