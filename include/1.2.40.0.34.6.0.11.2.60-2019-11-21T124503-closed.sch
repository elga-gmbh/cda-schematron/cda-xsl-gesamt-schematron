<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.60
Name: Medizinische Geräte und Implantate - kodiert
Description: 
                 Diese Sektion enthält Informationen über intra- und extrakorporale Medizinprodukte oder Medizingeräte, von denen der Gesundheitszustand des Patienten direkt abhängig ist. Das umfasst z.B. Implantate, Prothesen, Pumpen, Herzschrittmacher etc. von denen ein GDA Kenntnis haben soll. 
                 Heilbehelfe wie Gehhilfen, Rollstuhl etc sind nicht notwendigerweise anzuführen. 
            
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503-closed">
   <title>Medizinische Geräte und Implantate - kodiert</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']])]"
         id="d42e24950-true-d1290594e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d42e24950-true-d1290594e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']] (rule-reference: d42e24950-true-d1290594e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[(@code = '46264-8' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]])]"
         id="d42e25153-true-d1291275e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d42e25153-true-d1291275e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6'] | hl7:id[not(@nullFlavor)] | hl7:code[(@code = '46264-8' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]] (rule-reference: d42e25153-true-d1291275e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e25197-true-d1291433e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d42e25197-true-d1291433e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e25197-true-d1291433e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1291311e46-true-d1291541e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1291311e46-true-d1291541e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1291311e46-true-d1291541e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1291311e65-true-d1291600e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1291311e65-true-d1291600e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1291311e65-true-d1291600e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1291311e111-true-d1291657e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1291311e111-true-d1291657e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1291311e111-true-d1291657e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1291661e92-true-d1291691e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1291661e92-true-d1291691e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1291661e92-true-d1291691e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1291311e134-true-d1291739e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1291311e134-true-d1291739e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1291311e134-true-d1291739e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1291311e150-true-d1291784e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1291311e150-true-d1291784e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1291311e150-true-d1291784e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1291754e67-true-d1291847e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1291754e67-true-d1291847e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1291754e67-true-d1291847e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e25204-true-d1292015e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d42e25204-true-d1292015e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e25204-true-d1292015e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1291892e15-true-d1292147e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1291892e15-true-d1292147e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1291892e15-true-d1292147e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1292020e50-true-d1292213e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292020e50-true-d1292213e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1292020e50-true-d1292213e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1292020e120-true-d1292275e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292020e120-true-d1292275e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1292020e120-true-d1292275e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1292020e132-true-d1292297e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292020e132-true-d1292297e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1292020e132-true-d1292297e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1292285e12-true-d1292326e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292285e12-true-d1292326e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1292285e12-true-d1292326e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1292020e143-true-d1292377e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292020e143-true-d1292377e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1292020e143-true-d1292377e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1292351e58-true-d1292438e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292351e58-true-d1292438e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1292351e58-true-d1292438e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1291892e17-true-d1292515e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1291892e17-true-d1292515e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1291892e17-true-d1292515e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1291892e26-true-d1292568e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1291892e26-true-d1292568e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1291892e26-true-d1292568e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1291892e30-true-d1292623e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1291892e30-true-d1292623e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1291892e30-true-d1292623e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1292616e10-true-d1292650e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292616e10-true-d1292650e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1292616e10-true-d1292650e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/*[not(@xsi:nil = 'true')][not(self::hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']])]"
         id="d42e25216-true-d1292977e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d42e25216-true-d1292977e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']] (rule-reference: d42e25216-true-d1292977e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26'] | self::hl7:id | self::hl7:text | self::hl7:effectiveTime | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']] | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d1292681e9-true-d1293316e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292681e9-true-d1293316e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26'] | hl7:id | hl7:text | hl7:effectiveTime | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']] | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d1292681e9-true-d1293316e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1292681e32-true-d1293347e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292681e32-true-d1293347e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1292681e32-true-d1293347e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low)]"
         id="d1292681e47-true-d1293366e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292681e47-true-d1293366e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low (rule-reference: d1292681e47-true-d1293366e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1292681e69-true-d1293498e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292681e69-true-d1293498e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1292681e69-true-d1293498e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1293376e46-true-d1293606e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1293376e46-true-d1293606e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1293376e46-true-d1293606e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1293376e65-true-d1293665e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1293376e65-true-d1293665e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1293376e65-true-d1293665e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1293376e111-true-d1293722e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1293376e111-true-d1293722e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1293376e111-true-d1293722e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1293726e92-true-d1293756e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1293726e92-true-d1293756e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1293726e92-true-d1293756e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1293376e134-true-d1293804e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1293376e134-true-d1293804e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1293376e134-true-d1293804e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1293376e150-true-d1293849e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1293376e150-true-d1293849e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1293376e150-true-d1293849e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1293819e67-true-d1293912e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1293819e67-true-d1293912e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1293819e67-true-d1293912e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1292681e76-true-d1294080e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292681e76-true-d1294080e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1292681e76-true-d1294080e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1293957e15-true-d1294212e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1293957e15-true-d1294212e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1293957e15-true-d1294212e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1294085e50-true-d1294278e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1294085e50-true-d1294278e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1294085e50-true-d1294278e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1294085e120-true-d1294340e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1294085e120-true-d1294340e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1294085e120-true-d1294340e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1294085e132-true-d1294362e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1294085e132-true-d1294362e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1294085e132-true-d1294362e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1294350e12-true-d1294391e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1294350e12-true-d1294391e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1294350e12-true-d1294391e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1294085e143-true-d1294442e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1294085e143-true-d1294442e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1294085e143-true-d1294442e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1294416e58-true-d1294503e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1294416e58-true-d1294503e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1294416e58-true-d1294503e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1293957e17-true-d1294580e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1293957e17-true-d1294580e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1293957e17-true-d1294580e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1293957e26-true-d1294633e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1293957e26-true-d1294633e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1293957e26-true-d1294633e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1293957e30-true-d1294688e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1293957e30-true-d1294688e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1293957e30-true-d1294688e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1294681e10-true-d1294715e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1294681e10-true-d1294715e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1294681e10-true-d1294715e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/*[not(@xsi:nil = 'true')][not(self::hl7:participantRole)]"
         id="d1292681e88-true-d1294765e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292681e88-true-d1294765e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:participantRole (rule-reference: d1292681e88-true-d1294765e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:playingDevice)]"
         id="d1292681e95-true-d1294799e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292681e95-true-d1294799e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:playingDevice (rule-reference: d1292681e95-true-d1294799e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:code)]"
         id="d1292681e111-true-d1294833e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292681e111-true-d1294833e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code (rule-reference: d1292681e111-true-d1294833e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation)]"
         id="d1292681e117-true-d1294857e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292681e117-true-d1294857e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation (rule-reference: d1292681e117-true-d1294857e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference)]"
         id="d1292681e217-true-d1294871e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292681e217-true-d1294871e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference (rule-reference: d1292681e217-true-d1294871e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d1292681e242-true-d1294909e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1292681e242-true-d1294909e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d1292681e242-true-d1294909e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d1294886e5-true-d1294942e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1294886e5-true-d1294942e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d1294886e5-true-d1294942e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1294962e54-true-d1294974e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1294962e54-true-d1294974e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1294962e54-true-d1294974e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']])]"
         id="d42e25229-true-d1295269e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d42e25229-true-d1295269e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']] (rule-reference: d42e25229-true-d1295269e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | self::hl7:id[not(@nullFlavor)] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant)]"
         id="d1294990e8-true-d1295582e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1294990e8-true-d1295582e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | hl7:id[not(@nullFlavor)] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | hl7:author[hl7:assignedAuthor] | hl7:informant (rule-reference: d1294990e8-true-d1295582e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1294990e59-true-d1295736e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1294990e59-true-d1295736e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1294990e59-true-d1295736e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1295614e45-true-d1295844e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1295614e45-true-d1295844e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1295614e45-true-d1295844e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1295614e64-true-d1295903e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1295614e64-true-d1295903e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1295614e64-true-d1295903e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1295614e110-true-d1295960e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1295614e110-true-d1295960e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1295614e110-true-d1295960e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1295964e92-true-d1295994e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1295964e92-true-d1295994e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1295964e92-true-d1295994e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1295614e133-true-d1296042e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1295614e133-true-d1296042e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1295614e133-true-d1296042e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1295614e149-true-d1296087e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1295614e149-true-d1296087e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1295614e149-true-d1296087e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1296057e67-true-d1296150e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1296057e67-true-d1296150e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1296057e67-true-d1296150e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1294990e65-true-d1296318e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1294990e65-true-d1296318e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1294990e65-true-d1296318e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1296195e5-true-d1296450e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1296195e5-true-d1296450e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1296195e5-true-d1296450e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1296323e50-true-d1296516e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1296323e50-true-d1296516e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1296323e50-true-d1296516e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1296323e120-true-d1296578e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1296323e120-true-d1296578e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1296323e120-true-d1296578e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1296323e132-true-d1296600e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1296323e132-true-d1296600e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1296323e132-true-d1296600e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1296588e12-true-d1296629e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1296588e12-true-d1296629e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1296588e12-true-d1296629e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1296323e143-true-d1296680e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1296323e143-true-d1296680e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1296323e143-true-d1296680e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1296654e58-true-d1296741e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1296654e58-true-d1296741e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1296654e58-true-d1296741e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1296195e7-true-d1296818e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1296195e7-true-d1296818e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1296195e7-true-d1296818e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1296195e16-true-d1296871e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1296195e16-true-d1296871e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1296195e16-true-d1296871e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1296195e20-true-d1296926e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1296195e20-true-d1296926e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1296195e20-true-d1296926e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1296919e10-true-d1296953e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2019-11-21T124503.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1296919e10-true-d1296953e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1296919e10-true-d1296953e0)</assert>
   </rule>
</pattern>
