<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.1.2.3
Name: Beilagen
Description: Sonstige Beilagen, außer denjenigen Dokumenten, die in „Patientenverfügungen und andere juridische Dokumente“ angegeben sind. Achtung: Ein „Referenzieren“ auf Beilagen ist nicht gestattet. Beigelegte Dokumente/Bilder müssen dem Dokument in technisch eingebetteter Form beiliegen.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.1.2.3-2011-12-19T000000">
   <title>Beilagen</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.1.2.3
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]]
Item: (Beilagen)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.1.2.3
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]
Item: (Beilagen)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]"
         id="d42e3114-false-d37890e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.1.2.3-2011-12-19T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']) &gt;= 1">(Beilagen): Element hl7:templateId[@root = '1.2.40.0.34.11.1.2.3'] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.1.2.3-2011-12-19T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']) &lt;= 1">(Beilagen): Element hl7:templateId[@root = '1.2.40.0.34.11.1.2.3'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.1.2.3-2011-12-19T000000.html"
              test="count(hl7:code[(@code = 'BEIL' and @codeSystem = '1.2.40.0.34.5.40')]) &gt;= 1">(Beilagen): Element hl7:code[(@code = 'BEIL' and @codeSystem = '1.2.40.0.34.5.40')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.1.2.3-2011-12-19T000000.html"
              test="count(hl7:code[(@code = 'BEIL' and @codeSystem = '1.2.40.0.34.5.40')]) &lt;= 1">(Beilagen): Element hl7:code[(@code = 'BEIL' and @codeSystem = '1.2.40.0.34.5.40')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.1.2.3-2011-12-19T000000.html"
              test="count(hl7:title) &gt;= 1">(Beilagen): Element hl7:title ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.1.2.3-2011-12-19T000000.html"
              test="count(hl7:title) &lt;= 1">(Beilagen): Element hl7:title kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.1.2.3-2011-12-19T000000.html"
              test="count(hl7:text) &gt;= 1">(Beilagen): Element hl7:text ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.1.2.3-2011-12-19T000000.html"
              test="count(hl7:text) &lt;= 1">(Beilagen): Element hl7:text kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.1.2.3-2011-12-19T000000.html"
              test="count(hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]]) &lt;= 1">(Beilagen): Element hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.1.2.3
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]/hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']
Item: (Beilagen)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]/hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']"
         id="d42e3118-false-d37943e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.1.2.3-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(Beilagen): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.1.2.3-2011-12-19T000000.html"
              test="string(@root) = ('1.2.40.0.34.11.1.2.3')">(Beilagen): Der Wert von root MUSS '1.2.40.0.34.11.1.2.3' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.1.2.3
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]/hl7:code[(@code = 'BEIL' and @codeSystem = '1.2.40.0.34.5.40')]
Item: (Beilagen)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]/hl7:code[(@code = 'BEIL' and @codeSystem = '1.2.40.0.34.5.40')]"
         id="d42e3125-false-d37958e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.1.2.3-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(Beilagen): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.1.2.3-2011-12-19T000000.html"
              test="@nullFlavor or (@code='BEIL' and @codeSystem='1.2.40.0.34.5.40')">(Beilagen): Der Elementinhalt MUSS einer von 'code 'BEIL' codeSystem '1.2.40.0.34.5.40'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.1.2.3
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]/hl7:title
Item: (Beilagen)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]/hl7:title"
         id="d42e3136-false-d37974e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.1.2.3-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(Beilagen): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.1.2.3-2011-12-19T000000.html"
              test="text()='Beilagen'">(Beilagen): Der Elementinhalt von 'hl7:title' MUSS ''Beilagen'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.1.2.3
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]/hl7:text
Item: (Beilagen)
-->


   <!--
Template derived rules for ID: 1.2.40.0.34.11.1.2.3
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.3']]/hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]]
Item: (Beilagen)
--></pattern>
