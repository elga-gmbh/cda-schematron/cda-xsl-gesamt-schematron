<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.2.2.10
Name: Termine, Kontrollen, Wiederbestellung
Description: Auflistung weiterer Behandlungstermine, Kontrollen und Wiederbestellungen.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.2.2.10-2011-12-19T000000">
   <title>Termine, Kontrollen, Wiederbestellung</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.10
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]]
Item: (TermineKontrollenWiederbestellung)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.10
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]
Item: (TermineKontrollenWiederbestellung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]"
         id="d42e3899-false-d53887e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.10-2011-12-19T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']) &gt;= 1">(TermineKontrollenWiederbestellung): Element hl7:templateId[@root = '1.2.40.0.34.11.2.2.10'] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.10-2011-12-19T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']) &lt;= 1">(TermineKontrollenWiederbestellung): Element hl7:templateId[@root = '1.2.40.0.34.11.2.2.10'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.10-2011-12-19T000000.html"
              test="count(hl7:code[(@code = 'TERMIN' and @codeSystem = '1.2.40.0.34.5.40')]) &gt;= 1">(TermineKontrollenWiederbestellung): Element hl7:code[(@code = 'TERMIN' and @codeSystem = '1.2.40.0.34.5.40')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.10-2011-12-19T000000.html"
              test="count(hl7:code[(@code = 'TERMIN' and @codeSystem = '1.2.40.0.34.5.40')]) &lt;= 1">(TermineKontrollenWiederbestellung): Element hl7:code[(@code = 'TERMIN' and @codeSystem = '1.2.40.0.34.5.40')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.10-2011-12-19T000000.html"
              test="count(hl7:title) &gt;= 1">(TermineKontrollenWiederbestellung): Element hl7:title ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.10-2011-12-19T000000.html"
              test="count(hl7:title) &lt;= 1">(TermineKontrollenWiederbestellung): Element hl7:title kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.10-2011-12-19T000000.html"
              test="count(hl7:text) &gt;= 1">(TermineKontrollenWiederbestellung): Element hl7:text ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.10-2011-12-19T000000.html"
              test="count(hl7:text) &lt;= 1">(TermineKontrollenWiederbestellung): Element hl7:text kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.10
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]/hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']
Item: (TermineKontrollenWiederbestellung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]/hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']"
         id="d42e3901-false-d53930e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.10-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(TermineKontrollenWiederbestellung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.10-2011-12-19T000000.html"
              test="string(@root) = ('1.2.40.0.34.11.2.2.10')">(TermineKontrollenWiederbestellung): Der Wert von root MUSS '1.2.40.0.34.11.2.2.10' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.10
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]/hl7:code[(@code = 'TERMIN' and @codeSystem = '1.2.40.0.34.5.40')]
Item: (TermineKontrollenWiederbestellung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]/hl7:code[(@code = 'TERMIN' and @codeSystem = '1.2.40.0.34.5.40')]"
         id="d42e3906-false-d53945e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.10-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(TermineKontrollenWiederbestellung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.10-2011-12-19T000000.html"
              test="@nullFlavor or (@code='TERMIN' and @codeSystem='1.2.40.0.34.5.40')">(TermineKontrollenWiederbestellung): Der Elementinhalt MUSS einer von 'code 'TERMIN' codeSystem '1.2.40.0.34.5.40'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.10
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]/hl7:title
Item: (TermineKontrollenWiederbestellung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]/hl7:title"
         id="d42e3914-false-d53961e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.10-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(TermineKontrollenWiederbestellung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.10-2011-12-19T000000.html"
              test="text()='Termine, Kontrollen, Wiederbestellung'">(TermineKontrollenWiederbestellung): Der Elementinhalt von 'hl7:title' MUSS ''Termine, Kontrollen, Wiederbestellung'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.10
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]/hl7:text
Item: (TermineKontrollenWiederbestellung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.10']]/hl7:text"
         id="d42e3920-false-d53975e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.10-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(TermineKontrollenWiederbestellung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
