<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.90
Name: Device Measurement Range Observation
Description: Die Device Measurement Range Observation kann mit einem oder mehreren Elementen in einem Device Information Organizer vorhanden sein. Der Messbereich oder die Messspanne einer Messung sagt aus, in welchem Bereich ein Messwert liegen kann, um noch vom Gerät gemessen werden zu können. Beispielsweise kann der Messbereich eines Thermometers zwischen 0 und 100 Grad
                Celsius liegen. Der Messbereich wird hier in den Einheiten der Messgröße angegeben und weil ein Gerät mehr als nur eine Messgröße messen kann, können mehrere Device Measurement Range Observation-Elemente existieren. Wenn das Gerät die Daten über den Messbereich oder die anderen Geräte-Details nicht zurückgibt, können diese, wenn gewünscht, auch manuell oder über andere Wege
                eingetragen werden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820">
   <title>Device Measurement Range Observation</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.90
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceMeasurementRangeObservation)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.90
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]
Item: (atcdabbr_entry_DeviceMeasurementRangeObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]"
         id="d41e56492-false-d3613681e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="string(@classCode) = ('OBS')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Der Wert von classCode MUSS 'OBS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="string(@moodCode) = ('DEF')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Der Wert von moodCode MUSS 'DEF' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90']) &gt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90']) &lt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']) &gt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']) &lt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="count(hl7:code[(@code = '67198' and @codeSystem = '2.16.840.1.113883.6.24')]) &gt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Element hl7:code[(@code = '67198' and @codeSystem = '2.16.840.1.113883.6.24')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="count(hl7:code[(@code = '67198' and @codeSystem = '2.16.840.1.113883.6.24')]) &lt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Element hl7:code[(@code = '67198' and @codeSystem = '2.16.840.1.113883.6.24')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="count(hl7:text[not(@nullFlavor)][hl7:reference]) &gt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Element hl7:text[not(@nullFlavor)][hl7:reference] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="count(hl7:text[not(@nullFlavor)][hl7:reference]) &lt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Element hl7:text[not(@nullFlavor)][hl7:reference] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:value[@xsi:type='IVL_PQ'] | hl7:value[@xsi:type='ST'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="$elmcount &gt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Auswahl (hl7:value[@xsi:type='IVL_PQ']  oder  hl7:value[@xsi:type='ST']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="$elmcount &lt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Auswahl (hl7:value[@xsi:type='IVL_PQ']  oder  hl7:value[@xsi:type='ST']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="count(hl7:value[@xsi:type='IVL_PQ']) &lt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Element hl7:value[@xsi:type='IVL_PQ'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="count(hl7:value[@xsi:type='ST']) &lt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Element hl7:value[@xsi:type='ST'] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.90
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90']
Item: (atcdabbr_entry_DeviceMeasurementRangeObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90']"
         id="d41e56498-false-d3613749e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.90')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.90' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.90
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']
Item: (atcdabbr_entry_DeviceMeasurementRangeObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']"
         id="d41e56507-false-d3613764e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.36.5')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Der Wert von root MUSS '2.16.840.1.113883.10.20.36.5' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="string(@extension) = ('2015-08-17')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Der Wert von extension MUSS '2015-08-17' sein. Gefunden: "<value-of select="@extension"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.90
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:code[(@code = '67198' and @codeSystem = '2.16.840.1.113883.6.24')]
Item: (atcdabbr_entry_DeviceMeasurementRangeObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:code[(@code = '67198' and @codeSystem = '2.16.840.1.113883.6.24')]"
         id="d41e56518-false-d3613786e0">
      <extends rule="CD.IPS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="string(@code) = ('67198')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Der Wert von code MUSS '67198' sein. Gefunden: "<value-of select="@code"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="string(@codeSystem) = ('2.16.840.1.113883.6.24')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Der Wert von codeSystem MUSS '2.16.840.1.113883.6.24' sein. Gefunden: "<value-of select="@codeSystem"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="string(@codeSystemName) = ('MDC')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Der Wert von codeSystemName MUSS 'MDC' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="string(@displayName) = ('MDC_ATTR_NU_RANGE_MSMT: Measurement range')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Der Wert von displayName MUSS 'MDC_ATTR_NU_RANGE_MSMT: Measurement range' sein. Gefunden: "<value-of select="@displayName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.90
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:code[(@code = '67198' and @codeSystem = '2.16.840.1.113883.6.24')]/hl7:translation
Item: (atcdabbr_entry_DeviceMeasurementRangeObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:code[(@code = '67198' and @codeSystem = '2.16.840.1.113883.6.24')]/hl7:translation"
         id="d41e56528-false-d3613824e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="@code">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="@codeSystem">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@codeSystem,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@codeSystem,'^[A-Za-z][A-Za-z\d\-]*$')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribute @codeSystem MUSS vom Datentyp 'uid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.90
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:code[(@code = '67198' and @codeSystem = '2.16.840.1.113883.6.24')]/ips:designation
Item: (atcdabbr_entry_DeviceMeasurementRangeObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:code[(@code = '67198' and @codeSystem = '2.16.840.1.113883.6.24')]/ips:designation"
         id="d41e56543-false-d3613851e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="@language">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribut @language MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(@language) or (string-length(@language) &gt; 0 and not(matches(@language,'\s')))">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribute @language MUSS vom Datentyp 'cs' sein  - '<value-of select="@language"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.90
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)][hl7:reference]
Item: (atcdabbr_entry_DeviceMeasurementRangeObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)][hl7:reference]"
         id="d41e56553-false-d3613867e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.90
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)][hl7:reference]/hl7:reference[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceMeasurementRangeObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)][hl7:reference]/hl7:reference[not(@nullFlavor)]"
         id="d41e56558-false-d3613883e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="@value">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(@value) or string-length(@value)&gt;0">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribute @value MUSS vom Datentyp 'st' sein  - '<value-of select="@value"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.90
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:value[@xsi:type='IVL_PQ']
Item: (atcdabbr_entry_DeviceMeasurementRangeObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:value[@xsi:type='IVL_PQ']"
         id="d41e56566-false-d3613894e0">
      <extends rule="IVL_PQ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(hl7:low/@value) or matches(string(hl7:low/@value), '^[-+]?\d*\.?[0-9]+([eE][-+]?\d+)?$')">(atcdabbr_entry_DeviceMeasurementRangeObservation): value/low @value ist keine gültige PQ Zahl<value-of select="hl7:low/@value"/>
      </assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(hl7:high/@value) or matches(string(hl7:high/@value), '^[-+]?\d*\.?[0-9]+([eE][-+]?\d+)?$')">(atcdabbr_entry_DeviceMeasurementRangeObservation): value/high @value ist keine gültige PQ Zahl<value-of select="hl7:high/@value"/>
      </assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(hl7:center/@value) or matches(string(hl7:center/@value), '^[-+]?\d*\.?[0-9]+([eE][-+]?\d+)?$')">(atcdabbr_entry_DeviceMeasurementRangeObservation): value/center @value ist keine gültige PQ Zahl<value-of select="hl7:center/@value"/>
      </assert>
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="@nullFlavor or ($xsiLocalName='IVL_PQ' and $xsiLocalNS='urn:hl7-org:v3')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVL_PQ" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="count(hl7:low[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Element hl7:low[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="count(hl7:low[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Element hl7:low[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="count(hl7:high[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Element hl7:high[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="count(hl7:high[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceMeasurementRangeObservation): Element hl7:high[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.90
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:value[@xsi:type='IVL_PQ']/hl7:low[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceMeasurementRangeObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:value[@xsi:type='IVL_PQ']/hl7:low[not(@nullFlavor)]"
         id="d41e56570-false-d3613929e0">
      <extends rule="IVXB_PQ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVXB_PQ')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVXB_PQ" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="@value">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(@value) or matches(string(@value), '^[-+]?\d*\.?[0-9]+([eE][-+]?\d+)?$')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribute @value ist keine gültige real Zahl <value-of select="@value"/>
      </assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="@unit">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribut @unit MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(@unit) or (string-length(@unit) &gt; 0 and not(matches(@unit,'\s')))">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribute @unit MUSS vom Datentyp 'cs' sein  - '<value-of select="@unit"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.90
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:value[@xsi:type='IVL_PQ']/hl7:high[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceMeasurementRangeObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:value[@xsi:type='IVL_PQ']/hl7:high[not(@nullFlavor)]"
         id="d41e56586-false-d3613950e0">
      <extends rule="IVXB_PQ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVXB_PQ')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVXB_PQ" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="@value">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(@value) or matches(string(@value), '^[-+]?\d*\.?[0-9]+([eE][-+]?\d+)?$')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribute @value ist keine gültige real Zahl <value-of select="@value"/>
      </assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="@unit">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribut @unit MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(@unit) or (string-length(@unit) &gt; 0 and not(matches(@unit,'\s')))">(atcdabbr_entry_DeviceMeasurementRangeObservation): Attribute @unit MUSS vom Datentyp 'cs' sein  - '<value-of select="@unit"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.90
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:value[@xsi:type='ST']
Item: (atcdabbr_entry_DeviceMeasurementRangeObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:value[@xsi:type='ST']"
         id="d41e56603-false-d3613969e0">
      <extends rule="ST"/>
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="@nullFlavor or ($xsiLocalName='ST' and $xsiLocalNS='urn:hl7-org:v3')">(atcdabbr_entry_DeviceMeasurementRangeObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
