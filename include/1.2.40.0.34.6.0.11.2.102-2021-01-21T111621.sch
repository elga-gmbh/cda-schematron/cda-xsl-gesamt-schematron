<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.102
Name: Laboratory Specialty Section
Description: Die Section "Laboratory Specialty Section" entspricht den Befundbereichen basierend auf dem Value Set  "ELGA_Laborstruktur" , wobei für Befundbereiche NUR Einträge des Level 1 verwendet werden dürfen. Bei der Verwendung der Befundbereiche ist die Reihenfolge gemäß Value Set verpflichtend einzuhalten.  Achtung:  Aus dem Value Set
                "ELGA_Laborstruktur" werden der Code "10 - Probeninformation" durch die Section "Probeninformation (Specimen Section)" und der Code "20 - Befundbewertung" durch die Section "Befundbewertung" abgedeckt und dürfen somit im Rahmen dieses Templates NICHT VERWENDET werden.  Darstellung der Ergebnisse  "section/text" enthält den narrativen Text, der der CDA Level
                2 Darstellung der medizinischen Inhalte entspricht.  "section/text" darf keine medizinisch relevanten Inhalte enthalten, die nicht aus den CDA Level 3 codierten Daten abgeleitet werden können.  Die CDA Level 3 codierten Informationen sind über das Template "Laboratory Report D
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621">
   <title>Laboratory Specialty Section</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.102
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]
Item: (atlab_section_LaboratorySpecialtySection)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.102
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]
Item: (atlab_section_LaboratorySpecialtySection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]"
         id="d42e14873-false-d695030e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="string(@classCode) = ('DOCSECT') or not(@classCode)">(atlab_section_LaboratorySpecialtySection): Der Wert von classCode MUSS 'DOCSECT' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="string(@moodCode) = ('EVN') or not(@moodCode)">(atlab_section_LaboratorySpecialtySection): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']) &gt;= 1">(atlab_section_LaboratorySpecialtySection): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']) &lt;= 1">(atlab_section_LaboratorySpecialtySection): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.3.2.1']) = 0">(atlab_section_LaboratorySpecialtySection): Element hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.3.2.1'] DARF NICHT vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="count(hl7:id[not(@nullFlavor)]) &lt;= 1">(atlab_section_LaboratorySpecialtySection): Element hl7:id[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.47-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &gt;= 1">(atlab_section_LaboratorySpecialtySection): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.47-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.47-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &lt;= 1">(atlab_section_LaboratorySpecialtySection): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.47-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(atlab_section_LaboratorySpecialtySection): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(atlab_section_LaboratorySpecialtySection): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(atlab_section_LaboratorySpecialtySection): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(atlab_section_LaboratorySpecialtySection): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="count(hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']]) &gt;= 1">(atlab_section_LaboratorySpecialtySection): Element hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="count(hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']]) &lt;= 1">(atlab_section_LaboratorySpecialtySection): Element hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.102
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']
Item: (atlab_section_LaboratorySpecialtySection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']"
         id="d42e14879-false-d695301e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_section_LaboratorySpecialtySection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.2.102')">(atlab_section_LaboratorySpecialtySection): Der Wert von root MUSS '1.2.40.0.34.6.0.11.2.102' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.102
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.3.2.1']
Item: (atlab_section_LaboratorySpecialtySection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.3.2.1']"
         id="d42e14887-false-d695313e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_section_LaboratorySpecialtySection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="string(@root) = ('1.3.6.1.4.1.19376.1.3.3.2.1')">(atlab_section_LaboratorySpecialtySection): Der Wert von root MUSS '1.3.6.1.4.1.19376.1.3.3.2.1' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.102
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]/hl7:id[not(@nullFlavor)]
Item: (atlab_section_LaboratorySpecialtySection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]/hl7:id[not(@nullFlavor)]"
         id="d42e14900-false-d695325e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_section_LaboratorySpecialtySection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.102
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.47-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]
Item: (atlab_section_LaboratorySpecialtySection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.47-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]"
         id="d42e14907-false-d695338e0">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atlab_section_LaboratorySpecialtySection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.10.47-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(atlab_section_LaboratorySpecialtySection): Der Elementinhalt MUSS einer von '1.2.40.0.34.10.47 ELGA_Laborstruktur (DYNAMIC)' sein.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="@code">(atlab_section_LaboratorySpecialtySection): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atlab_section_LaboratorySpecialtySection): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="@codeSystem">(atlab_section_LaboratorySpecialtySection): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(atlab_section_LaboratorySpecialtySection): Attribute @codeSystem MUSS vom Datentyp 'oid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atlab_section_LaboratorySpecialtySection): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="@displayName">(atlab_section_LaboratorySpecialtySection): Attribut @displayName MUSS vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atlab_section_LaboratorySpecialtySection): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.102
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]/hl7:title[not(@nullFlavor)]
Item: (atlab_section_LaboratorySpecialtySection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]/hl7:title[not(@nullFlavor)]"
         id="d42e14932-false-d695376e0">
      <extends rule="ST"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atlab_section_LaboratorySpecialtySection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.102
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]/hl7:text[not(@nullFlavor)]
Item: (atlab_section_LaboratorySpecialtySection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]/hl7:text[not(@nullFlavor)]"
         id="d42e14938-false-d695386e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(atlab_section_LaboratorySpecialtySection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.102
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]/hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']]
Item: (atlab_section_LaboratorySpecialtySection)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]/hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']]">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="string(@typeCode) = ('DRIV')">(atlab_section_LaboratorySpecialtySection): Der Wert von typeCode MUSS 'DRIV' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atlab_section_LaboratorySpecialtySection): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.102
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]
Item: (atlab_section_LaboratorySpecialtySection)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.102']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atlab_section_LaboratorySpecialtySection): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.102-2021-01-21T111621.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atlab_section_LaboratorySpecialtySection): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>
</pattern>
