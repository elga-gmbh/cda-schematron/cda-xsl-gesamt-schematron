<?xml version="1.0" encoding="UTF-8"?>
<valueSets>
   <valueSet xmlns:cda="urn:hl7-org:v3"
             xmlns:hl7="urn:hl7-org:v3"
             xmlns:sdtc="urn:hl7-org:sdtc"
             xmlns:hl7at="urn:hl7-at:v3"
             xmlns:ips="urn:hl7-org:ips"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xmlns:pharm="urn:ihe:pharm:medication"
             id="1.2.40.0.34.10.187"
             name="ELGA_Probenmaterial_VS"
             displayName="ELGA_Probenmaterial_VS"
             effectiveDate="2021-08-25T00:00:00"
             statusCode="draft"
             versionLabel="&#x9;1.4.0+20250114"
             lastModifiedDate="2025-01-16T09:06:46">
      <desc language="de-DE" lastTranslated="2025-01-16T09:05:11">Materialien.</desc>
      <sourceCodeSystem id="2.16.840.1.113883.6.96"
                        identifierName="SNOMED Clinical Terms"
                        canonicalUri="http://snomed.info/sct"
                        canonicalUriDSTU2="http://snomed.info/sct"
                        canonicalUriSTU3="http://snomed.info/sct"
                        canonicalUriR4="http://snomed.info/sct"
                        canonicalUriR4B="http://snomed.info/sct"
                        canonicalUriR5="http://snomed.info/sct"
                        hl7v2table0396="SCT"/>
      <publishingAuthority name="HL7 Austria" inherited="project">
         <addrLine>www.hl7.at</addrLine>
      </publishingAuthority>
      <copyright language="en-US" inherited="true">This artefact includes content from SNOMED Clinical Terms® (SNOMED CT®) which is copyright of the International Health Terminology Standards Development Organisation (IHTSDO). Implementers of these artefacts must have the appropriate SNOMED CT Affiliate license - for more information contact http://www.snomed.org/snomed-ct/getsnomed-ct or info@snomed.org.</copyright>
      <conceptList>
         <concept code="123038009"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen (specimen)"
                  level="0"
                  type="S"/>
         <concept code="119297000"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Blood specimen (specimen)"
                  level="1"
                  type="S"/>
         <concept code="446131002"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Blood specimen obtained for blood culture (specimen)"
                  level="2"
                  type="L"/>
         <concept code="445295009"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Blood specimen with edetic acid (specimen)"
                  level="2"
                  type="L"/>
         <concept code="708049000"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Plasma specimen with ethylenediamine tetraacetic acid (specimen) "
                  level="2"
                  type="L"/>
         <concept code="1351816006"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Whole blood specimen with citrate (specimen)"
                  level="2"
                  type="L"/>
         <concept code="708048008"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Plasma specimen with citrate (specimen)"
                  level="2"
                  type="L"/>
         <concept code="441510007"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Blood specimen with anticoagulant (specimen)"
                  level="2"
                  type="L"/>
         <concept code="119361006"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Plasma specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="122590004"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Serum specimen from patient (specimen)"
                  level="2"
                  type="L"/>
         <concept code="122552005"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Arterial blood specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="122554006"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Capillary blood specimen (specimen) "
                  level="2"
                  type="L"/>
         <concept code="122555007"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Venous blood specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="122556008"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Cord blood specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="788707000"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Plasma specimen or serum specimen or whole blood specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258580003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Whole blood sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258603007"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Respiratory sample (specimen)"
                  level="1"
                  type="S"/>
         <concept code="410580001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Respiratory fluid specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="309164002"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Upper respiratory swab sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258606004"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Lower respiratory sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258607008"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Bronchoalveolar lavage fluid sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258446004"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Bronchial fluid sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="122610009"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen from lung obtained by biopsy (specimen)"
                  level="2"
                  type="L"/>
         <concept code="445297001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Swab of internal nose (specimen)"
                  level="2"
                  type="L"/>
         <concept code="168141000"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Nasal fluid sample (specimen) "
                  level="2"
                  type="L"/>
         <concept code="258500001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Nasopharyngeal swab (specimen) "
                  level="2"
                  type="L"/>
         <concept code="258411007"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Nasopharyngeal aspirate (specimen) "
                  level="2"
                  type="L"/>
         <concept code="258529004"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Throat swab (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258469001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Pharyngeal washings (specimen)"
                  level="2"
                  type="L"/>
         <concept code="119334006"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Sputum specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="697989009"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Anterior nares swab (specimen)"
                  level="2"
                  type="L"/>
         <concept code="472867001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Swab from tonsil (specimen)"
                  level="2"
                  type="L"/>
         <concept code="445447003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen from trachea obtained by aspiration (specimen)"
                  level="2"
                  type="L"/>
         <concept code="16223051000119101"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Nasal sinus contents sample (specimen) "
                  level="2"
                  type="L"/>
         <concept code="433324003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen from eye region (specimen)"
                  level="1"
                  type="S"/>
         <concept code="445160003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Swab of eye (specimen)"
                  level="2"
                  type="L"/>
         <concept code="472894002"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Swab from cornea (specimen) "
                  level="2"
                  type="L"/>
         <concept code="258438000"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Vitreous humor sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258498002"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Conjunctival swab (specimen) "
                  level="2"
                  type="L"/>
         <concept code="440473005"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Contact lens submitted as specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="472927003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Contact lens case submitted as specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="16212771000119105"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen from eyelid obtained by biopsy (specimen) "
                  level="2"
                  type="L"/>
         <concept code="309165001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Ear sample (specimen)"
                  level="1"
                  type="S"/>
         <concept code="258466008"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Middle ear fluid sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="472901003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Swab from nasal sinus (specimen)"
                  level="2"
                  type="L"/>
         <concept code="309166000"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Ear swab sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="122580007"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Cerumen specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258412000"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Oropharyngeal aspirate (specimen)"
                  level="2"
                  type="L"/>
         <concept code="127465007"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen from gastrointestinal tract (specimen)"
                  level="1"
                  type="S"/>
         <concept code="258662004"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Adhesive slide (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258527002"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Anal swab (specimen)"
                  level="2"
                  type="L"/>
         <concept code="309225009"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Colonic biopsy sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="122574004"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Duodenal fluid specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="449446003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen from gallbladder (specimen)"
                  level="2"
                  type="L"/>
         <concept code="119341000"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Bile specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="309211008"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Gastric biopsy sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258459007"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Gastric fluid sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="418932006"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Oral swab (specimen)"
                  level="2"
                  type="L"/>
         <concept code="309210009"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Esophageal brushings sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="472886009"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Swab from gastrostomy stoma (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258525005"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Perineal swab (specimen) "
                  level="2"
                  type="L"/>
         <concept code="168140004"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Peritoneal dialysate sample (specimen) "
                  level="2"
                  type="L"/>
         <concept code="168139001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Peritoneal fluid sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258528007"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Rectal swab (specimen)"
                  level="2"
                  type="L"/>
         <concept code="119339001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Stool specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="472866005"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Swab from tongue (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258570002"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Genitourinary sample (specimen)"
                  level="1"
                  type="S"/>
         <concept code="258524009"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Cervical swab (specimen)"
                  level="2"
                  type="L"/>
         <concept code="708112008"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Retrouterine pouch fluid aspirate (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258508008"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Genital swab (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258512002"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Glans penis swab (specimen)"
                  level="2"
                  type="L"/>
         <concept code="119347001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Seminal fluid specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258511009"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Penile urethral swab (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258520000"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Vaginal swab (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258523003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Vulval swab (specimen)"
                  level="2"
                  type="L"/>
         <concept code="446130001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Urine specimen from bladder (specimen) "
                  level="2"
                  type="L"/>
         <concept code="446846006"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Urine specimen obtained via indwelling urinary catheter (specimen) "
                  level="2"
                  type="L"/>
         <concept code="16221251000119108"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Urine specimen obtained via straight catheter (specimen) "
                  level="2"
                  type="L"/>
         <concept code="698276005"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="First stream urine sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="122575003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Urine specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="122565001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Urinary catheter specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258574006"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Mid-stream urine sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="442173007"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Urine specimen from nephrostomy tube (specimen)"
                  level="2"
                  type="L"/>
         <concept code="444937002"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Urine specimen from ureter (specimen)"
                  level="2"
                  type="L"/>
         <concept code="702701006"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen from cervix or vagina (specimen) "
                  level="2"
                  type="L"/>
         <concept code="127476006"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen from fallopian tube (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258548000"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Dermatological sample (specimen)"
                  level="1"
                  type="S"/>
         <concept code="445364004"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Swab of axilla (specimen) "
                  level="2"
                  type="L"/>
         <concept code="472896000"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Swab from blister (specimen)"
                  level="2"
                  type="L"/>
         <concept code="119326000"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Hair specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258503004"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Skin swab (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258549008"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Skin scale sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="445444005"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Swab of groin (specimen) "
                  level="2"
                  type="L"/>
         <concept code="445367006"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Swab of umbilicus (specimen) "
                  level="2"
                  type="L"/>
         <concept code="119327009"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Nail specimen (specimen) "
                  level="2"
                  type="L"/>
         <concept code="447098004"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen from nail obtained by scraping (specimen)"
                  level="2"
                  type="L"/>
         <concept code="119365002"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen from wound (specimen)"
                  level="2"
                  type="L"/>
         <concept code="472882006"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Swab from superficial wound (specimen) "
                  level="2"
                  type="L"/>
         <concept code="472884007"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Swab from deep wound (specimen)"
                  level="2"
                  type="L"/>
         <concept code="122568004"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Fluid specimen from wound (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258539005"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Bone and joint sample (specimen)"
                  level="1"
                  type="S"/>
         <concept code="258448003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Bursa fluid sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="309124001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Joint biopsy sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="431361003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Joint fluid specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="430268003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen from bone (specimen)"
                  level="2"
                  type="L"/>
         <concept code="119359002"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Bone marrow specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258415003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Biopsy sample (specimen)"
                  level="1"
                  type="S"/>
         <concept code="258497007"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Abscess swab (specimen)"
                  level="2"
                  type="L"/>
         <concept code="446972001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen from abscess obtained by aspiration (specimen)"
                  level="2"
                  type="L"/>
         <concept code="309201001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Ascitic fluid sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="119295008"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen obtained by aspiration (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258455001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Drainage fluid sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="119373006"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Amniotic fluid specimen (specimen) "
                  level="2"
                  type="L"/>
         <concept code="258588005"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Hematoma sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="16211011000119108"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen from deep surgical wound (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258450006"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Cerebrospinal fluid sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="309078004"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Lymph node tissue sample (specimen)"
                  level="2"
                  type="L"/>
         <concept code="122571007"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Pericardial fluid specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="418564007"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Pleural fluid specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="127454002"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Device specimen (specimen) [veraltet]"
                  level="1"
                  type="D"/>
         <concept code="898201001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen from device (specimen)"
                  level="1"
                  type="S"/>
         <concept code="258647001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Drain tip (specimen) [veraltet]"
                  level="2"
                  type="D"/>
         <concept code="1003706008"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen from drain tip (specimen)"
                  level="2"
                  type="L"/>
         <concept code="258540007"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Prosthetic joint sample (specimen) [veraltet]"
                  level="2"
                  type="D"/>
         <concept code="1003711005"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen from prosthetic joint (specimen)"
                  level="2"
                  type="L"/>
         <concept code="473415003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName=" Intrauterine contraceptive device submitted as specimen (specimen) "
                  level="2"
                  type="L"/>
         <concept code="472932002"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Vascular catheter submitted as specimen (specimen) "
                  level="2"
                  type="L"/>
         <concept code="119312009"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Catheter tip specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="472919007"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Device submitted as specimen (specimen) "
                  level="2"
                  type="L"/>
         <concept code="472938003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Tracheal tube submitted as specimen (specimen) "
                  level="2"
                  type="L"/>
         <concept code="439961009"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Implant submitted as specimen (specimen)"
                  level="2"
                  type="L"/>
         <concept code="257261003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Swab (specimen)"
                  level="1"
                  type="L"/>
         <concept code="258454002"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Dialysate sample (specimen)"
                  level="1"
                  type="L"/>
         <concept code="119303007"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Microbial isolate specimen (specimen)"
                  level="1"
                  type="L"/>
         <concept code="119328004"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Mother's milk specimen (specimen)"
                  level="1"
                  type="L"/>
         <concept code="258441009"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Exudate sample (specimen)"
                  level="1"
                  type="L"/>
         <concept code="119323008"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Pus specimen (specimen) "
                  level="1"
                  type="L"/>
         <concept code="258442002"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Fluid sample (specimen) "
                  level="1"
                  type="L"/>
         <concept code="708317005"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Pooled specimen from vaginal introitus and rectal swab (specimen) "
                  level="1"
                  type="L"/>
         <concept code="309051001"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Body fluid sample (specimen) "
                  level="1"
                  type="L"/>
         <concept code="119376003"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Tissue specimen (specimen)"
                  level="1"
                  type="L"/>
         <concept code="119324002"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Specimen of unknown material (specimen) "
                  level="1"
                  type="L"/>
         <concept code="408836004"
                  codeSystem="2.16.840.1.113883.6.96"
                  displayName="Sample not obtained (situation)"
                  level="1"
                  type="S"/>
      </conceptList>
   </valueSet>
</valueSets>
