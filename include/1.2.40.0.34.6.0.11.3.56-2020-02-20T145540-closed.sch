<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.56
Name: Notification Organizer EMS Arztmeldung
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.56-2020-02-20T145540-closed">
   <title>Notification Organizer EMS Arztmeldung</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/*[not(@xsi:nil = 'true')][not(self::hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']])]"
         id="d42e18259-true-d162668e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.56-2020-02-20T145540.html"
              test="not(.)">(epims_entry_notificationOrganizerArzt)/d42e18259-true-d162668e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']] (rule-reference: d42e18259-true-d162668e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1'] | self::hl7:statusCode[@code = 'completed'] | self::hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.1.1.1']]] | self::hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.1.1.2']]][not(@nullFlavor)])]"
         id="d42e18285-true-d162708e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.56-2020-02-20T145540.html"
              test="not(.)">(epims_entry_notificationOrganizerArzt)/d42e18285-true-d162708e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1'] | hl7:statusCode[@code = 'completed'] | hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.1.1.1']]] | hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.1.1.2']]][not(@nullFlavor)] (rule-reference: d42e18285-true-d162708e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.1.1.1']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.1']])]"
         id="d42e18307-true-d162743e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.56-2020-02-20T145540.html"
              test="not(.)">(epims_entry_notificationOrganizerArzt)/d42e18307-true-d162743e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.1']] (rule-reference: d42e18307-true-d162743e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.1.1.1']]]/hl7:observation[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.1'] | self::hl7:code[(@code = '170516003' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:statusCode[@code = 'completed'] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.58-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d162731e10-true-d162785e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.56-2020-02-20T145540.html"
              test="not(.)">(epims_entry_notificationOrganizerArzt)/d162731e10-true-d162785e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.1'] | hl7:code[(@code = '170516003' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:statusCode[@code = 'completed'] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.58-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d162731e10-true-d162785e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.1.1.1']]]/hl7:observation[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.1']]/hl7:code[(@code = '170516003' and @codeSystem = '2.16.840.1.113883.6.96')]/*[not(@xsi:nil = 'true')][not(self::hl7:qualifier[hl7:name[(@code = '246087005' and @codeSystem = '2.16.840.1.113883.6.96')]])]"
         id="d162731e24-true-d162808e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.56-2020-02-20T145540.html"
              test="not(.)">(epims_entry_notificationOrganizerArzt)/d162731e24-true-d162808e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:qualifier[hl7:name[(@code = '246087005' and @codeSystem = '2.16.840.1.113883.6.96')]] (rule-reference: d162731e24-true-d162808e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.1.1.1']]]/hl7:observation[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.1']]/hl7:code[(@code = '170516003' and @codeSystem = '2.16.840.1.113883.6.96')]/hl7:qualifier[hl7:name[(@code = '246087005' and @codeSystem = '2.16.840.1.113883.6.96')]]/*[not(@xsi:nil = 'true')][not(self::hl7:name[(@code = '246087005' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:value[(@code = '116154003' and @codeSystem = '2.16.840.1.113883.6.96')])]"
         id="d162731e28-true-d162832e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.56-2020-02-20T145540.html"
              test="not(.)">(epims_entry_notificationOrganizerArzt)/d162731e28-true-d162832e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[(@code = '246087005' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:value[(@code = '116154003' and @codeSystem = '2.16.840.1.113883.6.96')] (rule-reference: d162731e28-true-d162832e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.1.1.2']]][not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.57'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2'] and hl7:templateId[@root = '1.2.40.0.34.11.6.3.2']])]"
         id="d42e18319-true-d162885e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.56-2020-02-20T145540.html"
              test="not(.)">(epims_entry_notificationOrganizerArzt)/d42e18319-true-d162885e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.57'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2'] and hl7:templateId[@root = '1.2.40.0.34.11.6.3.2']] (rule-reference: d42e18319-true-d162885e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.1.1.2']]][not(@nullFlavor)]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.57'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2'] and hl7:templateId[@root = '1.2.40.0.34.11.6.3.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.57'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2'] | self::hl7:templateId[@root = '1.2.40.0.34.11.6.3.2'] | self::hl7:id[@root = '1.2.40.0.34.3.1.1'] | self::hl7:templateId | self::hl7:code[(@code = '416341003' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.29-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.19-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:informant[@typeCode = 'INF'][hl7:relatedEntity[@classCode = 'PAT']])]"
         id="d162863e6-true-d162961e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.56-2020-02-20T145540.html"
              test="not(.)">(epims_entry_notificationOrganizerArzt)/d162863e6-true-d162961e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.57'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2'] | hl7:templateId[@root = '1.2.40.0.34.11.6.3.2'] | hl7:id[@root = '1.2.40.0.34.3.1.1'] | hl7:templateId | hl7:code[(@code = '416341003' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.29-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.19-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:informant[@typeCode = 'INF'][hl7:relatedEntity[@classCode = 'PAT']] (rule-reference: d162863e6-true-d162961e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.1.1.2']]][not(@nullFlavor)]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.57'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2'] and hl7:templateId[@root = '1.2.40.0.34.11.6.3.2']]/hl7:id[@root = '1.2.40.0.34.3.1.1']/*[not(@xsi:nil = 'true')][not(self::hl7:id)]"
         id="d162863e48-true-d162994e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.56-2020-02-20T145540.html"
              test="not(.)">(epims_entry_notificationOrganizerArzt)/d162863e48-true-d162994e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id (rule-reference: d162863e48-true-d162994e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.1.1.2']]][not(@nullFlavor)]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.57'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2'] and hl7:templateId[@root = '1.2.40.0.34.11.6.3.2']]/hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.19-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]/*[not(@xsi:nil = 'true')][not(self::hl7:qualifier[hl7:name[(@code = '8' and @codeSystem = '2.16.840.1.113883.3.7.1.0')]] | self::hl7:qualifier[hl7:name[(@code = 'Krankheitsmerkmal' and @codeSystem = '1.2.40.0.34.5.101')]])]"
         id="d162863e111-true-d163047e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.56-2020-02-20T145540.html"
              test="not(.)">(epims_entry_notificationOrganizerArzt)/d162863e111-true-d163047e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:qualifier[hl7:name[(@code = '8' and @codeSystem = '2.16.840.1.113883.3.7.1.0')]] | hl7:qualifier[hl7:name[(@code = 'Krankheitsmerkmal' and @codeSystem = '1.2.40.0.34.5.101')]] (rule-reference: d162863e111-true-d163047e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.1.1.2']]][not(@nullFlavor)]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.57'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2'] and hl7:templateId[@root = '1.2.40.0.34.11.6.3.2']]/hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.19-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]/hl7:qualifier[hl7:name[(@code = '8' and @codeSystem = '2.16.840.1.113883.3.7.1.0')]]/*[not(@xsi:nil = 'true')][not(self::hl7:name[(@code = '8' and @codeSystem = '2.16.840.1.113883.3.7.1.0')] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.168-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d162863e120-true-d163075e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.56-2020-02-20T145540.html"
              test="not(.)">(epims_entry_notificationOrganizerArzt)/d162863e120-true-d163075e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[(@code = '8' and @codeSystem = '2.16.840.1.113883.3.7.1.0')] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.168-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d162863e120-true-d163075e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.1.1.2']]][not(@nullFlavor)]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.57'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2'] and hl7:templateId[@root = '1.2.40.0.34.11.6.3.2']]/hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.19-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]/hl7:qualifier[hl7:name[(@code = 'Krankheitsmerkmal' and @codeSystem = '1.2.40.0.34.5.101')]]/*[not(@xsi:nil = 'true')][not(self::hl7:name[(@code = 'Krankheitsmerkmal' and @codeSystem = '1.2.40.0.34.5.101')] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.18-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d162863e133-true-d163117e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.56-2020-02-20T145540.html"
              test="not(.)">(epims_entry_notificationOrganizerArzt)/d162863e133-true-d163117e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[(@code = 'Krankheitsmerkmal' and @codeSystem = '1.2.40.0.34.5.101')] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.18-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d162863e133-true-d163117e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.1.1.2']]][not(@nullFlavor)]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.57'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2'] and hl7:templateId[@root = '1.2.40.0.34.11.6.3.2']]/hl7:informant[@typeCode = 'INF'][hl7:relatedEntity[@classCode = 'PAT']]/*[not(@xsi:nil = 'true')][not(self::hl7:relatedEntity[not(@nullFlavor)][@classCode = 'PAT'])]"
         id="d163135e13-true-d163147e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.58-2020-02-20T153320.html"
              test="not(.)">(epims_entry_informant)/d163135e13-true-d163147e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:relatedEntity[not(@nullFlavor)][@classCode = 'PAT'] (rule-reference: d163135e13-true-d163147e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.56'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.1.1.2']]][not(@nullFlavor)]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.57'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2'] and hl7:templateId[@root = '1.2.40.0.34.11.6.3.2']]/hl7:informant[@typeCode = 'INF'][hl7:relatedEntity[@classCode = 'PAT']]/hl7:relatedEntity[not(@nullFlavor)][@classCode = 'PAT']/*[not(@xsi:nil = 'true')][not(self::hl7:effectiveTime[not(@nullFlavor)])]"
         id="d163135e17-true-d163161e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.58-2020-02-20T153320.html"
              test="not(.)">(epims_entry_informant)/d163135e17-true-d163161e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:effectiveTime[not(@nullFlavor)] (rule-reference: d163135e17-true-d163161e0)</assert>
   </rule>
</pattern>
