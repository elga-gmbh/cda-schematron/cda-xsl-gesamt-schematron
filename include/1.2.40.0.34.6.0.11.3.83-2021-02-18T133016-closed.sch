<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.83
Name: Device Regulation Status Information Observation
Description: Die Device Regulation Status Information Observation dokumentiert den Regulations-Status des verwendeten Gerätes. In PCHA konformen Geräten, kommen die Informationen aus dem RegCertDataList Attribut oder dem RegCertData Werten des Device Information Service eines Bluetooth Low Energy Health device. Ein Gerät ist entweder reguliert oder nicht reguliert.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.83-2021-02-18T133016-closed">
   <title>Device Regulation Status Information Observation</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']])]"
         id="d41e54331-true-d3607564e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2021-02-18T133016.html"
              test="not(.)">(atcdabbr_entry_DeviceRegulationStatusInformationObservation)/d41e54331-true-d3607564e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']] (rule-reference: d41e54331-true-d3607564e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17'] | self::hl7:code[(@code = '532354' and @codeSystem = '2.16.840.1.113883.6.24')] | self::hl7:text[not(@nullFlavor)][hl7:reference] | self::hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'BN')])]"
         id="d41e54371-true-d3607606e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2021-02-18T133016.html"
              test="not(.)">(atcdabbr_entry_DeviceRegulationStatusInformationObservation)/d41e54371-true-d3607606e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17'] | hl7:code[(@code = '532354' and @codeSystem = '2.16.840.1.113883.6.24')] | hl7:text[not(@nullFlavor)][hl7:reference] | hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'BN')] (rule-reference: d41e54371-true-d3607606e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:code[(@code = '532354' and @codeSystem = '2.16.840.1.113883.6.24')]/*[not(@xsi:nil = 'true')][not(self::hl7:translation | self::ips:designation)]"
         id="d41e54397-true-d3607638e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2021-02-18T133016.html"
              test="not(.)">(atcdabbr_entry_DeviceRegulationStatusInformationObservation)/d41e54397-true-d3607638e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:translation | ips:designation (rule-reference: d41e54397-true-d3607638e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)][hl7:reference]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d41e54432-true-d3607662e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2021-02-18T133016.html"
              test="not(.)">(atcdabbr_entry_DeviceRegulationStatusInformationObservation)/d41e54432-true-d3607662e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d41e54432-true-d3607662e0)</assert>
   </rule>
</pattern>
