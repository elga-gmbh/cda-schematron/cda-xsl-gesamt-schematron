<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.87
Name: Device Accuracy Observation
Description: Die Device Accuracy Observation kann mit einem oder mehreren Elementen in einem Device Information Organizer vorhanden sein. Die Genauigkeit oder Präzession einer Messung sagt aus wie viel der Messwert vom tatsächlichen Wert abweichen könnte. Beispielsweise können die Werte eines Geräts mit +/-3% vom tatsächlichen Wert abweichen. Die Genauigkeit wird hier
                angegeben in den Einheiten der Messgröße, und weil ein Gerät mehr als nur eine Messgröße messen kann, können mehrere Device Accuracy Observation-Elemente existieren. Wenn das Gerät die Daten über die Genauigkeit oder den anderen Geräte-Details nicht zurückgibt, können diese, wenn gewünscht, auch manuell oder über andere Wege eingetragen werden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.87-2020-02-19T171035-closed">
   <title>Device Accuracy Observation</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.87'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.3'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.87'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.3'][@extension = '2015-08-17']])]"
         id="d41e55582-true-d3611389e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.87-2020-02-19T171035.html"
              test="not(.)">(atcdabbr_entry_DeviceAccuracyObservation)/d41e55582-true-d3611389e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.87'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.3'][@extension = '2015-08-17']] (rule-reference: d41e55582-true-d3611389e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.87'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.3'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.87'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.3'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.87'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.3'][@extension = '2015-08-17'] | self::hl7:code[(@code = '67194' and @codeSystem = '2.16.840.1.113883.6.24')] | self::hl7:text[not(@nullFlavor)] | self::hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')])]"
         id="d41e55622-true-d3611431e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.87-2020-02-19T171035.html"
              test="not(.)">(atcdabbr_entry_DeviceAccuracyObservation)/d41e55622-true-d3611431e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.87'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.3'][@extension = '2015-08-17'] | hl7:code[(@code = '67194' and @codeSystem = '2.16.840.1.113883.6.24')] | hl7:text[not(@nullFlavor)] | hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')] (rule-reference: d41e55622-true-d3611431e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.87'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.3'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.87'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.3'][@extension = '2015-08-17']]/hl7:code[(@code = '67194' and @codeSystem = '2.16.840.1.113883.6.24')]/*[not(@xsi:nil = 'true')][not(self::hl7:translation | self::ips:designation)]"
         id="d41e55648-true-d3611463e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.87-2020-02-19T171035.html"
              test="not(.)">(atcdabbr_entry_DeviceAccuracyObservation)/d41e55648-true-d3611463e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:translation | ips:designation (rule-reference: d41e55648-true-d3611463e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.87'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.3'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.87'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.3'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d41e55683-true-d3611487e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.87-2020-02-19T171035.html"
              test="not(.)">(atcdabbr_entry_DeviceAccuracyObservation)/d41e55683-true-d3611487e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d41e55683-true-d3611487e0)</assert>
   </rule>
</pattern>
