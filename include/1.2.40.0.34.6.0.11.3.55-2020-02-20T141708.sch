<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.55
Name: EMS Abnahmeinformationen (Specimen Collection)
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708">
   <title>EMS Abnahmeinformationen (Specimen Collection)</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.55
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]
Item: (epims_entry_specimenCollection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]"
         id="d42e18125-false-d160885e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]) &gt;= 1">(epims_entry_specimenCollection): Element hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]) &lt;= 1">(epims_entry_specimenCollection): Element hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.55
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]
Item: (epims_entry_specimenCollection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]"
         id="d42e18156-false-d160932e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="string(@classCode) = ('PROC')">(epims_entry_specimenCollection): Der Wert von classCode MUSS 'PROC' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="string(@moodCode) = ('EVN')">(epims_entry_specimenCollection): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55']) &gt;= 1">(epims_entry_specimenCollection): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55']) &lt;= 1">(epims_entry_specimenCollection): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']) &gt;= 1">(epims_entry_specimenCollection): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']) &lt;= 1">(epims_entry_specimenCollection): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:code[(@code = '33882-2' and @codeSystem = '2.16.840.1.113883.6.1')]) &gt;= 1">(epims_entry_specimenCollection): Element hl7:code[(@code = '33882-2' and @codeSystem = '2.16.840.1.113883.6.1')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:code[(@code = '33882-2' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt;= 1">(epims_entry_specimenCollection): Element hl7:code[(@code = '33882-2' and @codeSystem = '2.16.840.1.113883.6.1')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:effectiveTime) &gt;= 1">(epims_entry_specimenCollection): Element hl7:effectiveTime ist required [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:effectiveTime) &lt;= 1">(epims_entry_specimenCollection): Element hl7:effectiveTime kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:methodCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.16-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]) &lt;= 1">(epims_entry_specimenCollection): Element hl7:methodCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.16-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:targetSiteCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.52-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]) &lt;= 1">(epims_entry_specimenCollection): Element hl7:targetSiteCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.52-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]) &lt;= 1">(epims_entry_specimenCollection): Element hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:participant[not(@nullFlavor)][@typeCode = 'PRD'][hl7:participantRole[@classCode = 'SPEC']]) &gt;= 1">(epims_entry_specimenCollection): Element hl7:participant[not(@nullFlavor)][@typeCode = 'PRD'][hl7:participantRole[@classCode = 'SPEC']] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:participant[not(@nullFlavor)][@typeCode = 'PRD'][hl7:participantRole[@classCode = 'SPEC']]) &lt;= 1">(epims_entry_specimenCollection): Element hl7:participant[not(@nullFlavor)][@typeCode = 'PRD'][hl7:participantRole[@classCode = 'SPEC']] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]]) &lt;= 1">(epims_entry_specimenCollection): Element hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.55
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55']
Item: (epims_entry_specimenCollection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55']"
         id="d42e18162-false-d161037e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(epims_entry_specimenCollection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.55')">(epims_entry_specimenCollection): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.55' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.55
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']
Item: (epims_entry_specimenCollection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']"
         id="d42e18167-false-d161052e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(epims_entry_specimenCollection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="string(@root) = ('1.3.6.1.4.1.19376.1.3.1.2')">(epims_entry_specimenCollection): Der Wert von root MUSS '1.3.6.1.4.1.19376.1.3.1.2' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.55
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:code[(@code = '33882-2' and @codeSystem = '2.16.840.1.113883.6.1')]
Item: (epims_entry_specimenCollection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:code[(@code = '33882-2' and @codeSystem = '2.16.840.1.113883.6.1')]"
         id="d42e18172-false-d161067e0">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(epims_entry_specimenCollection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="@nullFlavor or (@code='33882-2' and @codeSystem='2.16.840.1.113883.6.1')">(epims_entry_specimenCollection): Der Elementinhalt MUSS einer von 'code '33882-2' codeSystem '2.16.840.1.113883.6.1'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.55
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:effectiveTime
Item: (epims_entry_specimenCollection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:effectiveTime"
         id="d42e18178-false-d161087e0">
      <let name="elmcount"
           value="count(hl7:low[@value] | hl7:low[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="$elmcount &gt;= 1">(epims_entry_specimenCollection): Auswahl (hl7:low[@value]  oder  hl7:low[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="$elmcount &lt;= 1">(epims_entry_specimenCollection): Auswahl (hl7:low[@value]  oder  hl7:low[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:low[@value]) &lt;= 1">(epims_entry_specimenCollection): Element hl7:low[@value] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:low[@nullFlavor='UNK']) &lt;= 1">(epims_entry_specimenCollection): Element hl7:low[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:high[@value] | hl7:high[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="$elmcount &gt;= 1">(epims_entry_specimenCollection): Auswahl (hl7:high[@value]  oder  hl7:high[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="$elmcount &lt;= 1">(epims_entry_specimenCollection): Auswahl (hl7:high[@value]  oder  hl7:high[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:high[@value]) &lt;= 1">(epims_entry_specimenCollection): Element hl7:high[@value] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:high[@nullFlavor='UNK']) &lt;= 1">(epims_entry_specimenCollection): Element hl7:high[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:effectiveTime
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:effectiveTime/hl7:low[@value]
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:effectiveTime/hl7:low[@value]"
         id="d161125e45-false-d161133e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_other_TimeIntervalInformationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="not(*)">(atcdabbr_other_TimeIntervalInformationMinimal): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:effectiveTime/hl7:low[@nullFlavor='UNK']
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:effectiveTime/hl7:low[@nullFlavor='UNK']"
         id="d161125e47-false-d161144e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_other_TimeIntervalInformationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="not(*)">(atcdabbr_other_TimeIntervalInformationMinimal): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="string(@nullFlavor) = ('UNK')">(atcdabbr_other_TimeIntervalInformationMinimal): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:effectiveTime
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:effectiveTime/hl7:high[@value]
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:effectiveTime/hl7:high[@value]"
         id="d161125e55-false-d161161e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_other_TimeIntervalInformationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="not(*)">(atcdabbr_other_TimeIntervalInformationMinimal): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:effectiveTime/hl7:high[@nullFlavor='UNK']
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:effectiveTime/hl7:high[@nullFlavor='UNK']"
         id="d161125e57-false-d161172e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_other_TimeIntervalInformationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="not(*)">(atcdabbr_other_TimeIntervalInformationMinimal): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="string(@nullFlavor) = ('UNK')">(atcdabbr_other_TimeIntervalInformationMinimal): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.55
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:methodCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.16-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]
Item: (epims_entry_specimenCollection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:methodCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.16-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]"
         id="d42e18183-false-d161192e0">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(epims_entry_specimenCollection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.6.0.10.16-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(epims_entry_specimenCollection): Der Elementinhalt MUSS einer von '1.2.40.0.34.6.0.10.16 EMS MaterialMethode VS (DYNAMIC)' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.55
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:targetSiteCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.52-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]
Item: (epims_entry_specimenCollection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:targetSiteCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.52-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]"
         id="d42e18188-false-d161215e0">
      <extends rule="CD"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(epims_entry_specimenCollection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.10.52-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(epims_entry_specimenCollection): Der Elementinhalt MUSS einer von '1.2.40.0.34.10.52 ELGA_HumanActSite (DYNAMIC)' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.55
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]
Item: (epims_entry_specimenCollection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]"
         id="d42e18196-false-d161242e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="string(@typeCode) = ('PRF')">(epims_entry_specimenCollection): Der Wert von typeCode MUSS 'PRF' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]) &gt;= 1">(epims_entry_specimenCollection): Element hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]) &lt;= 1">(epims_entry_specimenCollection): Element hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.55
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]
Item: (epims_entry_specimenCollection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]"
         id="d42e18204-false-d161276e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:id) &gt;= 1">(epims_entry_specimenCollection): Element hl7:id ist required [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:addr) &lt;= 1">(epims_entry_specimenCollection): Element hl7:addr kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:assignedPerson[not(@nullFlavor)]) &gt;= 1">(epims_entry_specimenCollection): Element hl7:assignedPerson[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:assignedPerson[not(@nullFlavor)]) &lt;= 1">(epims_entry_specimenCollection): Element hl7:assignedPerson[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:representedOrganization) &lt;= 1">(epims_entry_specimenCollection): Element hl7:representedOrganization kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.90003
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id
Item: (AssignedEntityElements)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id"
         id="d161313e22-false-d161321e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(AssignedEntityElements): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.90003
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr
Item: (AssignedEntityElements)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr"
         id="d161313e33-false-d161331e0">
      <extends rule="AD"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'AD')">(AssignedEntityElements): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:AD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.90003
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:telecom
Item: (AssignedEntityElements)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:telecom"
         id="d161313e45-false-d161341e0">
      <extends rule="TEL.AT"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(AssignedEntityElements): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.90003
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]
Item: (AssignedEntityElements)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]"
         id="d161313e56-false-d161354e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="string(@classCode) = ('PSN') or not(@classCode)">(AssignedEntityElements): Der Wert von classCode MUSS 'PSN' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(AssignedEntityElements): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(AssignedEntityElements): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(AssignedEntityElements): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.90001
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]
Item: (PersonElements)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]"
         id="d161351e43-false-d161384e0">
      <extends rule="PN"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90001-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PN')">(PersonElements): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:PN" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.90003
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization
Item: (AssignedEntityElements)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization"
         id="d161313e64-false-d161397e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="string(@classCode) = ('ORG') or not(@classCode)">(AssignedEntityElements): Der Wert von classCode MUSS 'ORG' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(AssignedEntityElements): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(AssignedEntityElements): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(AssignedEntityElements): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90003-2011-12-19T000000.html"
              test="count(hl7:addr) &lt;= 1">(AssignedEntityElements): Element hl7:addr kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.90002
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:id
Item: (OrganizationElements)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:id"
         id="d161394e38-false-d161438e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90002-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(OrganizationElements): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.90002
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:name[not(@nullFlavor)]
Item: (OrganizationElements)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:name[not(@nullFlavor)]"
         id="d161394e40-false-d161448e0">
      <extends rule="ON"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90002-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ON')">(OrganizationElements): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ON" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.90002
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:telecom
Item: (OrganizationElements)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:telecom"
         id="d161394e43-false-d161458e0">
      <extends rule="TEL.AT"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90002-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(OrganizationElements): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.90002
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr
Item: (OrganizationElements)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:performer[@typeCode = 'PRF'][hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr"
         id="d161394e45-false-d161468e0">
      <extends rule="AD"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.90002-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'AD')">(OrganizationElements): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:AD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.55
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:participant[not(@nullFlavor)][@typeCode = 'PRD'][hl7:participantRole[@classCode = 'SPEC']]
Item: (epims_entry_specimenCollection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:participant[not(@nullFlavor)][@typeCode = 'PRD'][hl7:participantRole[@classCode = 'SPEC']]"
         id="d42e18214-false-d161478e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="string(@typeCode) = ('PRD')">(epims_entry_specimenCollection): Der Wert von typeCode MUSS 'PRD' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="string(@contextControlCode) = ('OP') or not(@contextControlCode)">(epims_entry_specimenCollection): Der Wert von contextControlCode MUSS 'OP' sein. Gefunden: "<value-of select="@contextControlCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:participantRole[hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]) &gt;= 1">(epims_entry_specimenCollection): Element hl7:participantRole[hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:participantRole[hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]) &lt;= 1">(epims_entry_specimenCollection): Element hl7:participantRole[hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.55
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:participant[not(@nullFlavor)][@typeCode = 'PRD'][hl7:participantRole[@classCode = 'SPEC']]/hl7:participantRole[hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]
Item: (epims_entry_specimenCollection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:participant[not(@nullFlavor)][@typeCode = 'PRD'][hl7:participantRole[@classCode = 'SPEC']]/hl7:participantRole[hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]"
         id="d42e18224-false-d161508e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="string(@classCode) = ('SPEC')">(epims_entry_specimenCollection): Der Wert von classCode MUSS 'SPEC' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:id[not(@nullFlavor)]) &gt;= 1">(epims_entry_specimenCollection): Element hl7:id[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:id[not(@nullFlavor)]) &lt;= 1">(epims_entry_specimenCollection): Element hl7:id[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]) &gt;= 1">(epims_entry_specimenCollection): Element hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]) &lt;= 1">(epims_entry_specimenCollection): Element hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.55
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:participant[not(@nullFlavor)][@typeCode = 'PRD'][hl7:participantRole[@classCode = 'SPEC']]/hl7:participantRole[hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:id[not(@nullFlavor)]
Item: (epims_entry_specimenCollection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:participant[not(@nullFlavor)][@typeCode = 'PRD'][hl7:participantRole[@classCode = 'SPEC']]/hl7:participantRole[hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:id[not(@nullFlavor)]"
         id="d42e18228-false-d161539e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(epims_entry_specimenCollection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.55
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:participant[not(@nullFlavor)][@typeCode = 'PRD'][hl7:participantRole[@classCode = 'SPEC']]/hl7:participantRole[hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]
Item: (epims_entry_specimenCollection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:participant[not(@nullFlavor)][@typeCode = 'PRD'][hl7:participantRole[@classCode = 'SPEC']]/hl7:participantRole[hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]"
         id="d42e18236-false-d161552e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &gt;= 1">(epims_entry_specimenCollection): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &lt;= 1">(epims_entry_specimenCollection): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.55
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:participant[not(@nullFlavor)][@typeCode = 'PRD'][hl7:participantRole[@classCode = 'SPEC']]/hl7:participantRole[hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]
Item: (epims_entry_specimenCollection)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:participant[not(@nullFlavor)][@typeCode = 'PRD'][hl7:participantRole[@classCode = 'SPEC']]/hl7:participantRole[hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:playingEntity[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]"
         id="d42e18238-false-d161574e0">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(epims_entry_specimenCollection): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.6.0.10.17-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(epims_entry_specimenCollection): Der Elementinhalt MUSS einer von '1.2.40.0.34.6.0.10.17 EMS Material VS (DYNAMIC)' sein.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.55
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]]
Item: (epims_entry_specimenCollection)
-->
   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]]">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="string(@typeCode) = ('COMP')">(epims_entry_specimenCollection): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.55-2020-02-20T141708.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(epims_entry_specimenCollection): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.30022
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]]
Item: (SpecimenReceived)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.30022
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]]/hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]
Item: (SpecimenReceived)
-->
   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]]/hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.30022-2014-03-04T000000.html"
              test="string(@classCode) = ('ACT')">(SpecimenReceived): Der Wert von classCode MUSS 'ACT' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.30022-2014-03-04T000000.html"
              test="string(@moodCode) = ('EVN')">(SpecimenReceived): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.30022-2014-03-04T000000.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']) &gt;= 1">(SpecimenReceived): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3'] ist required [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.30022-2014-03-04T000000.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']) &lt;= 1">(SpecimenReceived): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.30022-2014-03-04T000000.html"
              test="count(hl7:code[(@code = 'SPRECEIVE' and @codeSystem = '1.3.5.1.4.1.19376.1.5.3.2')]) &gt;= 1">(SpecimenReceived): Element hl7:code[(@code = 'SPRECEIVE' and @codeSystem = '1.3.5.1.4.1.19376.1.5.3.2')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.30022-2014-03-04T000000.html"
              test="count(hl7:code[(@code = 'SPRECEIVE' and @codeSystem = '1.3.5.1.4.1.19376.1.5.3.2')]) &lt;= 1">(SpecimenReceived): Element hl7:code[(@code = 'SPRECEIVE' and @codeSystem = '1.3.5.1.4.1.19376.1.5.3.2')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.30022-2014-03-04T000000.html"
              test="count(hl7:effectiveTime) &gt;= 1">(SpecimenReceived): Element hl7:effectiveTime ist required [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.30022-2014-03-04T000000.html"
              test="count(hl7:effectiveTime) &lt;= 1">(SpecimenReceived): Element hl7:effectiveTime kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.30022
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]]/hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']
Item: (SpecimenReceived)
-->
   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]]/hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.30022-2014-03-04T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(SpecimenReceived): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.30022-2014-03-04T000000.html"
              test="string(@root) = ('1.3.6.1.4.1.19376.1.3.1.3')">(SpecimenReceived): Der Wert von root MUSS '1.3.6.1.4.1.19376.1.3.1.3' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.30022
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]]/hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]/hl7:code[(@code = 'SPRECEIVE' and @codeSystem = '1.3.5.1.4.1.19376.1.5.3.2')]
Item: (SpecimenReceived)
-->
   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]]/hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]/hl7:code[(@code = 'SPRECEIVE' and @codeSystem = '1.3.5.1.4.1.19376.1.5.3.2')]">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.30022-2014-03-04T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(SpecimenReceived): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.30022-2014-03-04T000000.html"
              test="@nullFlavor or (@code='SPRECEIVE' and @codeSystem='1.3.5.1.4.1.19376.1.5.3.2')">(SpecimenReceived): Der Elementinhalt MUSS einer von 'code 'SPRECEIVE' codeSystem '1.3.5.1.4.1.19376.1.5.3.2'' sein.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.30022
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]]/hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]/hl7:effectiveTime
Item: (SpecimenReceived)
-->
   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]]/hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]/hl7:effectiveTime">
      <extends rule="TS"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.30022-2014-03-04T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(SpecimenReceived): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.30022-2014-03-04T000000.html"
              test="not(*)">(SpecimenReceived): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.30022
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]]/hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.4.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]
Item: (SpecimenReceived)
-->
   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.2']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]]/hl7:act[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.3']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.4.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.11.30022-2014-03-04T000000.html"
              test="string(@typeCode) = ('COMP')">(SpecimenReceived): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
   </rule>
</pattern>
