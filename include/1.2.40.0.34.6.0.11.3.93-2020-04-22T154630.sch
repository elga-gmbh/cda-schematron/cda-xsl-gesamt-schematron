<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.93
Name: EMS Organizer Arzt
Description: Der Laboratory Battery Organizer ist ein Strukturierungshilfsmittel und beinhaltet eine Sammlung von Laboratory Observations. Laut ELGA Implementierungsleitfaden für den Laborbefund kann ein Befund mehrere Organizer enthalten und jeweils Ergebnisse einer Befundgruppe beinhalten. Die EMS Arztmeldung enthält jedoch nur einen Organizer welcher die benötigten EMS
                Parameter als einzelne Observations geführt werden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630">
   <title>EMS Organizer Arzt</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.93
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]
Item: (epims_entry_EMSOrganizerArzt)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.93
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]
Item: (epims_entry_EMSOrganizerArzt)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]"
         id="d42e23115-false-d291006e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="string(@classCode) = ('BATTERY')">(epims_entry_EMSOrganizerArzt): Der Wert von classCode MUSS 'BATTERY' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="string(@moodCode) = ('EVN')">(epims_entry_EMSOrganizerArzt): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93']) &gt;= 1">(epims_entry_EMSOrganizerArzt): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93']) &lt;= 1">(epims_entry_EMSOrganizerArzt): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']) &gt;= 1">(epims_entry_EMSOrganizerArzt): Element hl7:templateId[@root = '1.2.40.0.34.11.6.2.1'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']) &lt;= 1">(epims_entry_EMSOrganizerArzt): Element hl7:templateId[@root = '1.2.40.0.34.11.6.2.1'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="count(hl7:code[(@code = '30' and @codeSystem = '1.2.40.0.34.5.189')]) &gt;= 1">(epims_entry_EMSOrganizerArzt): Element hl7:code[(@code = '30' and @codeSystem = '1.2.40.0.34.5.189')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="count(hl7:code[(@code = '30' and @codeSystem = '1.2.40.0.34.5.189')]) &lt;= 1">(epims_entry_EMSOrganizerArzt): Element hl7:code[(@code = '30' and @codeSystem = '1.2.40.0.34.5.189')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="count(hl7:statusCode[@code = 'completed']) &gt;= 1">(epims_entry_EMSOrganizerArzt): Element hl7:statusCode[@code = 'completed'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="count(hl7:statusCode[@code = 'completed']) &lt;= 1">(epims_entry_EMSOrganizerArzt): Element hl7:statusCode[@code = 'completed'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="count(hl7:effectiveTime) &lt;= 1">(epims_entry_EMSOrganizerArzt): Element hl7:effectiveTime kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="count(hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]) &lt;= 1">(epims_entry_EMSOrganizerArzt): Element hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.93
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93']
Item: (epims_entry_EMSOrganizerArzt)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93']"
         id="d42e23121-false-d291084e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(epims_entry_EMSOrganizerArzt): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.93')">(epims_entry_EMSOrganizerArzt): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.93' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.93
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']
Item: (epims_entry_EMSOrganizerArzt)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']"
         id="d42e23126-false-d291099e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(epims_entry_EMSOrganizerArzt): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="string(@root) = ('1.2.40.0.34.11.6.2.1')">(epims_entry_EMSOrganizerArzt): Der Wert von root MUSS '1.2.40.0.34.11.6.2.1' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.93
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:code[(@code = '30' and @codeSystem = '1.2.40.0.34.5.189')]
Item: (epims_entry_EMSOrganizerArzt)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:code[(@code = '30' and @codeSystem = '1.2.40.0.34.5.189')]"
         id="d42e23131-false-d291114e0">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(epims_entry_EMSOrganizerArzt): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="@nullFlavor or (@code='30' and @codeSystem='1.2.40.0.34.5.189' and @displayName='EMS_Organizer' and @codeSystemName='EMS_struktur_elemente')">(epims_entry_EMSOrganizerArzt): Der Elementinhalt MUSS einer von 'code '30' codeSystem '1.2.40.0.34.5.189' displayName='EMS_Organizer' codeSystemName='EMS_struktur_elemente'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.93
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:statusCode[@code = 'completed']
Item: (epims_entry_EMSOrganizerArzt)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:statusCode[@code = 'completed']"
         id="d42e23137-false-d291131e0">
      <extends rule="CS"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CS')">(epims_entry_EMSOrganizerArzt): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="@nullFlavor or (@code='completed')">(epims_entry_EMSOrganizerArzt): Der Elementinhalt MUSS einer von 'code 'completed'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.93
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:effectiveTime
Item: (epims_entry_EMSOrganizerArzt)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:effectiveTime"
         id="d42e23142-false-d291151e0">
      <extends rule="IVL_TS"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVL_TS')">(epims_entry_EMSOrganizerArzt): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVL_TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="elmcount"
           value="count(hl7:low[@value] | hl7:low[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="$elmcount &gt;= 1">(epims_entry_EMSOrganizerArzt): Auswahl (hl7:low[@value]  oder  hl7:low[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="$elmcount &lt;= 1">(epims_entry_EMSOrganizerArzt): Auswahl (hl7:low[@value]  oder  hl7:low[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="count(hl7:low[@value]) &lt;= 1">(epims_entry_EMSOrganizerArzt): Element hl7:low[@value] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="count(hl7:low[@nullFlavor='UNK']) &lt;= 1">(epims_entry_EMSOrganizerArzt): Element hl7:low[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:high[@value] | hl7:high[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="$elmcount &gt;= 1">(epims_entry_EMSOrganizerArzt): Auswahl (hl7:high[@value]  oder  hl7:high[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="$elmcount &lt;= 1">(epims_entry_EMSOrganizerArzt): Auswahl (hl7:high[@value]  oder  hl7:high[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="count(hl7:high[@value]) &lt;= 1">(epims_entry_EMSOrganizerArzt): Element hl7:high[@value] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="count(hl7:high[@nullFlavor='UNK']) &lt;= 1">(epims_entry_EMSOrganizerArzt): Element hl7:high[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:effectiveTime
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:effectiveTime/hl7:low[@value]
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:effectiveTime/hl7:low[@value]"
         id="d291154e53-false-d291200e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_other_TimeIntervalInformationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="not(*)">(atcdabbr_other_TimeIntervalInformationMinimal): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:effectiveTime/hl7:low[@nullFlavor='UNK']
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:effectiveTime/hl7:low[@nullFlavor='UNK']"
         id="d291154e55-false-d291211e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_other_TimeIntervalInformationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="not(*)">(atcdabbr_other_TimeIntervalInformationMinimal): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="string(@nullFlavor) = ('UNK')">(atcdabbr_other_TimeIntervalInformationMinimal): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:effectiveTime
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:effectiveTime/hl7:high[@value]
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:effectiveTime/hl7:high[@value]"
         id="d291154e63-false-d291228e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_other_TimeIntervalInformationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="not(*)">(atcdabbr_other_TimeIntervalInformationMinimal): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:effectiveTime/hl7:high[@nullFlavor='UNK']
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:effectiveTime/hl7:high[@nullFlavor='UNK']"
         id="d291154e65-false-d291239e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_other_TimeIntervalInformationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="not(*)">(atcdabbr_other_TimeIntervalInformationMinimal): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="string(@nullFlavor) = ('UNK')">(atcdabbr_other_TimeIntervalInformationMinimal): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.93
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']
Item: (epims_entry_EMSOrganizerArzt)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']"
         id="d42e23150-false-d291259e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="string(@typeCode) = ('COMP')">(epims_entry_EMSOrganizerArzt): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="not(@contextConductionInd) or string(@contextConductionInd)=('true','false')">(epims_entry_EMSOrganizerArzt): Attribute @contextConductionInd MUSS vom Datentyp 'bl' sein  - '<value-of select="@contextConductionInd"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.69
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']
Item: (epims_entry_EmsOrganizerObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']"
         id="d291256e62-false-d291284e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="string(@classCode) = ('OBS')">(epims_entry_EmsOrganizerObservation): Der Wert von classCode MUSS 'OBS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="string(@moodCode) = ('EVN')">(epims_entry_EmsOrganizerObservation): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="count(hl7:code[not(@nullFlavor)]) &gt;= 1">(epims_entry_EmsOrganizerObservation): Element hl7:code[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="count(hl7:code[not(@nullFlavor)]) &lt;= 1">(epims_entry_EmsOrganizerObservation): Element hl7:code[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="count(hl7:effectiveTime) &lt;= 1">(epims_entry_EmsOrganizerObservation): Element hl7:effectiveTime kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="count(hl7:value) &gt;= 1">(epims_entry_EmsOrganizerObservation): Element hl7:value ist required [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="count(hl7:value) &lt;= 1">(epims_entry_EmsOrganizerObservation): Element hl7:value kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.69
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']/hl7:code[not(@nullFlavor)]
Item: (epims_entry_EmsOrganizerObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']/hl7:code[not(@nullFlavor)]"
         id="d291256e68-false-d291322e0">
      <extends rule="CD"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(epims_entry_EmsOrganizerObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.69
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']/hl7:effectiveTime
Item: (epims_entry_EmsOrganizerObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']/hl7:effectiveTime"
         id="d291256e74-false-d291332e0">
      <extends rule="IVL_TS"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVL_TS')">(epims_entry_EmsOrganizerObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVL_TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.69
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']/hl7:value
Item: (epims_entry_EmsOrganizerObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']/hl7:value"
         id="d291256e76-false-d291342e0">
      <extends rule="ANY"/>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.69
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']/hl7:value/hl7:qualifier
Item: (epims_entry_EmsOrganizerObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']/hl7:value/hl7:qualifier"
         id="d291256e82-false-d291353e0">
      <extends rule="CR"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CR')">(epims_entry_EmsOrganizerObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CR" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="count(hl7:name) &gt;= 1">(epims_entry_EmsOrganizerObservation): Element hl7:name ist required [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="count(hl7:name) &lt;= 1">(epims_entry_EmsOrganizerObservation): Element hl7:name kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="count(hl7:value) &gt;= 1">(epims_entry_EmsOrganizerObservation): Element hl7:value ist required [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="count(hl7:value) &lt;= 1">(epims_entry_EmsOrganizerObservation): Element hl7:value kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.69
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']/hl7:value/hl7:qualifier/hl7:name
Item: (epims_entry_EmsOrganizerObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']/hl7:value/hl7:qualifier/hl7:name"
         id="d291256e87-false-d291380e0">
      <extends rule="CV"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CV')">(epims_entry_EmsOrganizerObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CV" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.69
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']/hl7:value/hl7:qualifier/hl7:value
Item: (epims_entry_EmsOrganizerObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']/hl7:value/hl7:qualifier/hl7:value"
         id="d291256e89-false-d291390e0">
      <extends rule="CD"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(epims_entry_EmsOrganizerObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.93
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]
Item: (epims_entry_EMSOrganizerArzt)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]"
         id="d42e23163-false-d291404e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="string(@typeCode) = ('COMP')">(epims_entry_EMSOrganizerArzt): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="not(@contextConductionInd) or string(@contextConductionInd)=('true','false')">(epims_entry_EMSOrganizerArzt): Attribute @contextConductionInd MUSS vom Datentyp 'bl' sein  - '<value-of select="@contextConductionInd"/>'</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="count(hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]) &gt;= 1">(epims_entry_EMSOrganizerArzt): Element hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]] ist required [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="count(hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]) &lt;= 1">(epims_entry_EMSOrganizerArzt): Element hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.62
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]
Item: (epims_entry_foreignCountry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]"
         id="d291416e36-false-d291436e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="string(@classCode) = ('OBS')">(epims_entry_foreignCountry): Der Wert von classCode MUSS 'OBS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="string(@moodCode) = ('EVN')">(epims_entry_foreignCountry): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="count(hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]) &gt;= 1">(epims_entry_foreignCountry): Element hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="count(hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]) &lt;= 1">(epims_entry_foreignCountry): Element hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="count(hl7:effectiveTime) &lt;= 1">(epims_entry_foreignCountry): Element hl7:effectiveTime kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="count(hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')]) &gt;= 1">(epims_entry_foreignCountry): Element hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="count(hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')]) &lt;= 1">(epims_entry_foreignCountry): Element hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.62
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]
Item: (epims_entry_foreignCountry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]"
         id="d291416e42-false-d291476e0">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(epims_entry_foreignCountry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="@nullFlavor or (@code='ILLOC' and @codeSystem='1.2.40.0.34.5.101')">(epims_entry_foreignCountry): Der Elementinhalt MUSS einer von 'code 'ILLOC' codeSystem '1.2.40.0.34.5.101'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.62
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:effectiveTime
Item: (epims_entry_foreignCountry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:effectiveTime"
         id="d291416e47-false-d291492e0">
      <extends rule="IVL_TS"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVL_TS')">(epims_entry_foreignCountry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVL_TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.62
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')]
Item: (epims_entry_foreignCountry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')]"
         id="d291416e53-false-d291502e0">
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="@nullFlavor or ($xsiLocalName='CD' and $xsiLocalNS='urn:hl7-org:v3')">(epims_entry_foreignCountry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="string(@code) = ('AL') or not(@code)">(epims_entry_foreignCountry): Der Wert von code MUSS 'AL' sein. Gefunden: "<value-of select="@code"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="string(@codeSystem) = ('1.2.40.0.34.5.77') or not(@codeSystem)">(epims_entry_foreignCountry): Der Wert von codeSystem MUSS '1.2.40.0.34.5.77' sein. Gefunden: "<value-of select="@codeSystem"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="count(hl7:qualifier[hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]]) &gt;= 1">(epims_entry_foreignCountry): Element hl7:qualifier[hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="count(hl7:qualifier[hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]]) &lt;= 1">(epims_entry_foreignCountry): Element hl7:qualifier[hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.62
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')]/hl7:qualifier[hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]]
Item: (epims_entry_foreignCountry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')]/hl7:qualifier[hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]]"
         id="d291416e61-false-d291533e0">
      <extends rule="CR"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CR')">(epims_entry_foreignCountry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CR" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="count(hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]) &gt;= 1">(epims_entry_foreignCountry): Element hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="count(hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]) &lt;= 1">(epims_entry_foreignCountry): Element hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="count(hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.51-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &gt;= 1">(epims_entry_foreignCountry): Element hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.51-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="count(hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.51-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &lt;= 1">(epims_entry_foreignCountry): Element hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.51-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.62
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')]/hl7:qualifier[hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]
Item: (epims_entry_foreignCountry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')]/hl7:qualifier[hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]"
         id="d291416e63-false-d291565e0">
      <extends rule="CV"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CV')">(epims_entry_foreignCountry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CV" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="@nullFlavor or (@code='TRVCNTRY' and @codeSystem='1.2.40.0.34.5.101')">(epims_entry_foreignCountry): Der Elementinhalt MUSS einer von 'code 'TRVCNTRY' codeSystem '1.2.40.0.34.5.101'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.62
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')]/hl7:qualifier[hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.51-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]
Item: (epims_entry_foreignCountry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')]/hl7:qualifier[hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.51-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]"
         id="d291416e68-false-d291584e0">
      <extends rule="CD"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(epims_entry_foreignCountry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.6.0.10.51-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(epims_entry_foreignCountry): Der Elementinhalt MUSS einer von '1.2.40.0.34.6.0.10.51 EMS Reiseland VS (DYNAMIC)' sein.</assert>
   </rule>
</pattern>
