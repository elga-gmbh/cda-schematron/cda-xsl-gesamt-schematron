<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.83
Name: Device Regulation Status Information Observation
Description: Die Device Regulation Status Information Observation dokumentiert den Regulations-Status des verwendeten Gerätes. In PCHA konformen Geräten, kommen die Informationen aus dem RegCertDataList Attribut oder dem RegCertData Werten des Device Information Service eines Bluetooth Low Energy Health device. Ein Gerät ist entweder reguliert oder nicht reguliert.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402">
   <title>Device Regulation Status Information Observation</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.83
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceRegulationStatusInformationObservation)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.83
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]
Item: (atcdabbr_entry_DeviceRegulationStatusInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]"
         id="d41e54488-false-d3607708e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="string(@classCode) = ('OBS')">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Der Wert von classCode MUSS 'OBS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="string(@moodCode) = ('DEF')">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Der Wert von moodCode MUSS 'DEF' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83']) &gt;= 1">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83']) &lt;= 1">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']) &gt;= 1">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']) &lt;= 1">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="count(hl7:code[(@code = '532354' and @codeSystem = '2.16.840.1.113883.6.24')]) &gt;= 1">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Element hl7:code[(@code = '532354' and @codeSystem = '2.16.840.1.113883.6.24')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="count(hl7:code[(@code = '532354' and @codeSystem = '2.16.840.1.113883.6.24')]) &lt;= 1">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Element hl7:code[(@code = '532354' and @codeSystem = '2.16.840.1.113883.6.24')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="count(hl7:text[not(@nullFlavor)][hl7:reference]) &gt;= 1">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Element hl7:text[not(@nullFlavor)][hl7:reference] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="count(hl7:text[not(@nullFlavor)][hl7:reference]) &lt;= 1">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Element hl7:text[not(@nullFlavor)][hl7:reference] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="count(hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'BN')]) &gt;= 1">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Element hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'BN')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="count(hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'BN')]) &lt;= 1">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Element hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'BN')] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.83
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83']
Item: (atcdabbr_entry_DeviceRegulationStatusInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83']"
         id="d41e54494-false-d3607768e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.83')">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.83' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.83
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']
Item: (atcdabbr_entry_DeviceRegulationStatusInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']"
         id="d41e54503-false-d3607783e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.36.28')">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Der Wert von root MUSS '2.16.840.1.113883.10.20.36.28' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="string(@extension) = ('2015-08-17')">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Der Wert von extension MUSS '2015-08-17' sein. Gefunden: "<value-of select="@extension"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.83
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:code[(@code = '532354' and @codeSystem = '2.16.840.1.113883.6.24')]
Item: (atcdabbr_entry_DeviceRegulationStatusInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:code[(@code = '532354' and @codeSystem = '2.16.840.1.113883.6.24')]"
         id="d41e54514-false-d3607805e0">
      <extends rule="CD.IPS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="string(@code) = ('532354')">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Der Wert von code MUSS '532354' sein. Gefunden: "<value-of select="@code"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="string(@codeSystem) = ('2.16.840.1.113883.6.24')">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Der Wert von codeSystem MUSS '2.16.840.1.113883.6.24' sein. Gefunden: "<value-of select="@codeSystem"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="string(@codeSystemName) = ('MDC')">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Der Wert von codeSystemName MUSS 'MDC' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="string(@displayName) = ('MDC_REG_CERT_DATA_CONTINUA_REG_STATUS: Regulation status')">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Der Wert von displayName MUSS 'MDC_REG_CERT_DATA_CONTINUA_REG_STATUS: Regulation status' sein. Gefunden: "<value-of select="@displayName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.83
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:code[(@code = '532354' and @codeSystem = '2.16.840.1.113883.6.24')]/hl7:translation
Item: (atcdabbr_entry_DeviceRegulationStatusInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:code[(@code = '532354' and @codeSystem = '2.16.840.1.113883.6.24')]/hl7:translation"
         id="d41e54524-false-d3607843e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="@code">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="@codeSystem">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@codeSystem,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@codeSystem,'^[A-Za-z][A-Za-z\d\-]*$')">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Attribute @codeSystem MUSS vom Datentyp 'uid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.83
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:code[(@code = '532354' and @codeSystem = '2.16.840.1.113883.6.24')]/ips:designation
Item: (atcdabbr_entry_DeviceRegulationStatusInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:code[(@code = '532354' and @codeSystem = '2.16.840.1.113883.6.24')]/ips:designation"
         id="d41e54539-false-d3607870e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="@language">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Attribut @language MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="not(@language) or (string-length(@language) &gt; 0 and not(matches(@language,'\s')))">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Attribute @language MUSS vom Datentyp 'cs' sein  - '<value-of select="@language"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.83
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)][hl7:reference]
Item: (atcdabbr_entry_DeviceRegulationStatusInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)][hl7:reference]"
         id="d41e54549-false-d3607886e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.83
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)][hl7:reference]/hl7:reference[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceRegulationStatusInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)][hl7:reference]/hl7:reference[not(@nullFlavor)]"
         id="d41e54554-false-d3607902e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="@value">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="not(@value) or string-length(@value)&gt;0">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Attribute @value MUSS vom Datentyp 'st' sein  - '<value-of select="@value"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.83
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'BN')]
Item: (atcdabbr_entry_DeviceRegulationStatusInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.83'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.28'][@extension = '2015-08-17']]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'BN')]"
         id="d41e54560-false-d3607915e0">
      <extends rule="BN"/>
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.83-2020-02-19T163402.html"
              test="@nullFlavor or ($xsiLocalName='BN' and $xsiLocalNS='urn:hl7-org:v3')">(atcdabbr_entry_DeviceRegulationStatusInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:BN" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
