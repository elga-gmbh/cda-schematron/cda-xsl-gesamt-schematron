<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.9.43
Name: Device Product Instance Template
Description: Das "Device Product Instance Template" beschreibt die verpflichtenden Eigenschaften des verwendeten Gerätes. Mindestens eine eindeutige ID, die Klassifizierung und der Name (samt Hersteller) des Gerätes sind für jedes verwendetes Gerät anzugeben. Dieses Template definiert das participantRole-Element des Device Information Organizer. In vielen Fällen ist das Gerät
                ein IEEE 11073 kompatibles Gerät. In diesem Dokument vorhandene medizinische Beobachtungen besitzen ein author-Element, welches in den Daten dieses Templates vorhanden sein muss. Alle weiteren Informationen zum verwendeten Gerät können im darüberliegenden Device Information Organizer kodiert werden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534">
   <title>Device Product Instance Template</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]"
         id="d41e62798-false-d3617714e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="string(@classCode) = ('MANU')">(atcdabbr_entry_DeviceProductInstanceTemplate): Der Wert von classCode MUSS 'MANU' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43']) &gt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43']) &lt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37']) &gt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37']) &lt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']) &gt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']) &lt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:id[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:id[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]) &gt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]) &lt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:scopingEntitiy) = 0">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:scopingEntitiy DARF NICHT vorkommen.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43']
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43']"
         id="d41e62808-false-d3617777e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceProductInstanceTemplate): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.9.43')">(atcdabbr_entry_DeviceProductInstanceTemplate): Der Wert von root MUSS '1.2.40.0.34.6.0.11.9.43' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37']
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37']"
         id="d41e62816-false-d3617792e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceProductInstanceTemplate): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.22.4.37')">(atcdabbr_entry_DeviceProductInstanceTemplate): Der Wert von root MUSS '2.16.840.1.113883.10.20.22.4.37' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']"
         id="d41e62824-false-d3617807e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceProductInstanceTemplate): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.36.9')">(atcdabbr_entry_DeviceProductInstanceTemplate): Der Wert von root MUSS '2.16.840.1.113883.10.20.36.9' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="string(@extension) = ('2015-08-17')">(atcdabbr_entry_DeviceProductInstanceTemplate): Der Wert von extension MUSS '2015-08-17' sein. Gefunden: "<value-of select="@extension"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:id[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:id[not(@nullFlavor)]"
         id="d41e62835-false-d3617828e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="@root">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribut @root MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$')">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribute @root MUSS vom Datentyp 'oid' sein  - '<value-of select="@root"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="@extension">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribut @extension MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="@assigningAuthorityName">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribut @assigningAuthorityName MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="not(@assigningAuthorityName) or string-length(@assigningAuthorityName)&gt;0">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribute @assigningAuthorityName MUSS vom Datentyp 'st' sein  - '<value-of select="@assigningAuthorityName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]"
         id="d41e62853-false-d3617855e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:code[@codeSystem = '2.16.840.1.113883.6.24']) &gt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:code[@codeSystem = '2.16.840.1.113883.6.24'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:code[@codeSystem = '2.16.840.1.113883.6.24']) &lt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:code[@codeSystem = '2.16.840.1.113883.6.24'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:manufacturerModelName[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:manufacturerModelName[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:manufacturerModelName[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:manufacturerModelName[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:code[@codeSystem = '2.16.840.1.113883.6.24']
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:code[@codeSystem = '2.16.840.1.113883.6.24']"
         id="d41e62858-false-d3617887e0">
      <extends rule="CE.IPS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atcdabbr_entry_DeviceProductInstanceTemplate): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="@code">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="string(@codeSystem) = ('2.16.840.1.113883.6.24')">(atcdabbr_entry_DeviceProductInstanceTemplate): Der Wert von codeSystem MUSS '2.16.840.1.113883.6.24' sein. Gefunden: "<value-of select="@codeSystem"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="string(@codeSystemName) = ('MDC') or not(@codeSystemName)">(atcdabbr_entry_DeviceProductInstanceTemplate): Der Wert von codeSystemName MUSS 'MDC' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:originalText[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:originalText[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:originalText[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:originalText[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.2
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:code[@codeSystem = '2.16.840.1.113883.6.24']/hl7:originalText
Item: (atcdabbr_other_OriginalTextReference)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:code[@codeSystem = '2.16.840.1.113883.6.24']/hl7:originalText"
         id="d3617890e70-false-d3617931e0">
      <extends rule="ED"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(atcdabbr_other_OriginalTextReference): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_OriginalTextReference): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_OriginalTextReference): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.2
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:code[@codeSystem = '2.16.840.1.113883.6.24']/hl7:originalText/hl7:reference[not(@nullFlavor)]
Item: (atcdabbr_other_OriginalTextReference)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:code[@codeSystem = '2.16.840.1.113883.6.24']/hl7:originalText/hl7:reference[not(@nullFlavor)]"
         id="d3617890e76-false-d3617950e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabbr_other_OriginalTextReference): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="@value">(atcdabbr_other_OriginalTextReference): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="starts-with(@value,'#')">(atcdabbr_other_OriginalTextReference): The @value attribute content MUST conform to the format '#xxx', where xxx is the ID of the corresponding 'content'-element.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:code[@codeSystem = '2.16.840.1.113883.6.24']/hl7:translation
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:code[@codeSystem = '2.16.840.1.113883.6.24']/hl7:translation"
         id="d41e62874-false-d3617964e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceProductInstanceTemplate): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:manufacturerModelName[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:manufacturerModelName[not(@nullFlavor)]"
         id="d41e62881-false-d3617974e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atcdabbr_entry_DeviceProductInstanceTemplate): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:scopingEntitiy
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:scopingEntitiy"
         id="d41e62888-false-d3617984e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="count(hl7:desc) = 0">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:desc DARF NICHT vorkommen.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:scopingEntitiy/hl7:desc
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:scopingEntitiy/hl7:desc"
         id="d41e62893-false-d3617998e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-10-05T183534.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atcdabbr_entry_DeviceProductInstanceTemplate): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
