<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.2.2.18
Name: Anamnese (ärztlich)
Description: Anamnese des Patienten.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.2.2.18-2011-12-19T000000">
   <title>Anamnese (ärztlich)</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.18
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]]
Item: (AnamneseAerztlich)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.18
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]
Item: (AnamneseAerztlich)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]"
         id="d42e8946-false-d173357e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.2.2.18']) &gt;= 1">(AnamneseAerztlich): Element hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.2.2.18']) &lt;= 1">(AnamneseAerztlich): Element hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']) &gt;= 1">(AnamneseAerztlich): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4'] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']) &lt;= 1">(AnamneseAerztlich): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="count(hl7:code[(@code = '10164-2' and @codeSystem = '2.16.840.1.113883.6.1')]) &gt;= 1">(AnamneseAerztlich): Element hl7:code[(@code = '10164-2' and @codeSystem = '2.16.840.1.113883.6.1')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="count(hl7:code[(@code = '10164-2' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt;= 1">(AnamneseAerztlich): Element hl7:code[(@code = '10164-2' and @codeSystem = '2.16.840.1.113883.6.1')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(AnamneseAerztlich): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(AnamneseAerztlich): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(AnamneseAerztlich): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(AnamneseAerztlich): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.18
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]/hl7:templateId[@root = '1.2.40.0.34.11.2.2.18']
Item: (AnamneseAerztlich)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]/hl7:templateId[@root = '1.2.40.0.34.11.2.2.18']"
         id="d42e8950-false-d173409e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(AnamneseAerztlich): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="string(@root) = ('1.2.40.0.34.11.2.2.18')">(AnamneseAerztlich): Der Wert von root MUSS '1.2.40.0.34.11.2.2.18' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.18
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']
Item: (AnamneseAerztlich)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']"
         id="d42e8955-false-d173424e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(AnamneseAerztlich): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="string(@root) = ('1.3.6.1.4.1.19376.1.5.3.1.3.4')">(AnamneseAerztlich): Der Wert von root MUSS '1.3.6.1.4.1.19376.1.5.3.1.3.4' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.18
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]/hl7:code[(@code = '10164-2' and @codeSystem = '2.16.840.1.113883.6.1')]
Item: (AnamneseAerztlich)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]/hl7:code[(@code = '10164-2' and @codeSystem = '2.16.840.1.113883.6.1')]"
         id="d42e8962-false-d173439e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(AnamneseAerztlich): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="@nullFlavor or (@code='10164-2' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='History of present illness')">(AnamneseAerztlich): Der Elementinhalt MUSS einer von 'code '10164-2' codeSystem '2.16.840.1.113883.6.1' displayName='History of present illness'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.18
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]/hl7:title[not(@nullFlavor)]
Item: (AnamneseAerztlich)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]/hl7:title[not(@nullFlavor)]"
         id="d42e8973-false-d173455e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(AnamneseAerztlich): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="text()='Anamnese'">(AnamneseAerztlich): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Anamnese'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.18
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]/hl7:text[not(@nullFlavor)]
Item: (AnamneseAerztlich)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.18'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.4']]/hl7:text[not(@nullFlavor)]"
         id="d42e8981-false-d173469e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.18-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(AnamneseAerztlich): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
